<%-- 
    Document   : insertReceita
    Created on : 19/11
    Author     : Ivan
--%>


<%@ include file="header.jsp" %>
<%@ page import="java.text.SimpleDateFormat"%>
<%@ page import="java.text.DateFormat"%>
<%@ page import="java.util.Date"%>
<%@ page import="data.ConsultaDO" %>
<%@ page import="data.PacienteDO" %>
<%@ page import="data.MedicoDO" %>
<%@ page import="transacoes.Consulta" %>
<%@ page import="transacoes.Paciente" %>
<%@ page import="transacoes.Medico" %>
<%@ page import="java.util.Vector" %>
<%@ page import="java.util.*" %>

<div id="content">
    <h1>Inserir receita</h1>
    <hr>
    
<! ------------------------------------------------------------------------->
<!--   se for o request inicial, mostrar somente o formulario de escolha  -->

<%     
    String action = request.getParameter("action");
    if ( null == action ) {
       action = "showSearchForm";
%>

    <form action="./insertReceita.jsp" method="post">
<%
           
    // Puxar todas as consultas do usuario
    transacoes.Consulta tn = new transacoes.Consulta();
    //System.out.println(userid) ;
    Medico med = new Medico();
    MedicoDO medi = med.buscarPorUserID(Integer.parseInt(userid));
    int medid = medi.getIdMedico();
    //System.out.println(medid) ;
    Vector consultas = tn.pesquisar4(medid,2);
    System.out.println(consultas.size());
%>
        <table>
          <tr>
             <td>Selecione a consulta em que deseja anexar receitar</td>
             <td>
                 <select name="cid">
                     <%
                     int aux;
                     if (consultas.size()>10)  aux = 10;
                     else aux = consultas.size();
                     for (int i = aux; i >0; i--) {
                       
                        ConsultaDO consulta = (ConsultaDO)consultas.get(i-1);
                      
                        if (true) { // Medico
                           transacoes.Medico t = new transacoes.Medico();
                           MedicoDO medico = t.buscar(consulta.getMedicoId());
                           transacoes.Paciente p = new transacoes.Paciente();
                           PacienteDO paciente = p.buscar(consulta.getPacienteId());
                     %>
                            <option value="<%= consulta.getId()%>">Sr(a). <%= paciente.getNome() %> - <%= consulta.getData() %></option>
                     <% }
                     }
                     %>

                 </select>
             </td>
          </tr>
        </table>
        <input type="submit" name="enviar" value="Inserir receita" />
        <input type="hidden" name="action" value="showSearchResults" />
    </form>

<%  } 
%>
    
<! ------------------------------------------------------------------------->
<!--   Inserir a receita                                       -->
<%
    if (action.equals("showSearchResults")) {
        String cid = request.getParameter("cid");
        
       
%>
    <form action="./insertReceita.jsp" method="post">

        <table>
            <tr>
               <td>Entre com a receita (nome comercial, componente ativo, pre�o m�dio, posologia, data de emiss�o, observa��es)</td>
            </tr>
            <tr>
                <td><textarea name="Receita" rows="10" cols="100" maxlength="500" required></textarea></td>
            </tr>
        </table>
        <a href="index.jsp">Voltar</a>
        <input type="submit" name="enviar" value="Inserir receita" />
        <input type="hidden" name="cid" value="<%= cid %>" />
        <input type="hidden" name="action" value="updateValues" />
        
    </form>
<%  } 
%>
    
<! ------------------------------------------------------------------->
<!--   atualizar valores -->
<%     
    if (action.equals("updateValues")) {
        String receita = request.getParameter("Receita");
        
        int cid = Integer.parseInt( request.getParameter("cid") );

        transacoes.Consulta tn = new transacoes.Consulta();
        ConsultaDO consulta = tn.buscar(cid);
        
        consulta.setReceita(receita);
        
        
        boolean result = false;
        try {
           result = tn.atualizar(consulta);
        } catch (Exception e) {
%>           <%= e.toString() %>
<%
        }
        
  
        
        if ( result ) {
        // avisar usuario que transacao foi feita com sucesso
%>
        Receita inserida com sucesso
        <form action="./index.jsp" method="post">
           <input type="submit" name="voltar" value="Voltar" />
        </form>
<%      } else {
%>
        Erro 
        <form action="./cancelarConsulta.jsp" method="post">
           <input type="submit" name="retry" value="Repetir" />
        </form>
<%        }
    } // updateValues
%>

</div> <!-- Content div -->

<%@ include file="footer.jsp" %>

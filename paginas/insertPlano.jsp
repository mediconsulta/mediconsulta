<%-- 
    Document   : insertPlano
    Created on : Oct 25, 2014, 5:57:44 PM
    Author     : Edson
--%>

<%@ include file="header.jsp" %>

<div id="content">
    <h1>Inserir Plano de Sa&uacute;de</h1>
    <hr>
    
    <%
    if((Integer)session.getAttribute("user_type")!=0) {
    %>
    Voc&ecirc; n&atilde;o tem permiss&atilde;o para ver esta p&aacute;gina.
    <% }
    else {
        String action = request.getParameter("action");
        if ( null == action ) {
        action = "showSearchForm";
%>

        <form action="./insertPlano.jsp" method="post">
            <table>
              <tr>
                 <td>Nome do Plano de Sa&uacute;de que pretende cadastrar</td>
                 <td><input type="text" name="nome" required/>
              </tr>
            </table>
            <input type="submit" name="cadastrar" value="Cadastrar" />
            <input type="hidden" name="action" value="insertValues" />
            <a href="index.jsp">Cancelar</a>
        </form>

<%      } 
%>
    
<! ------------------------------------------------------------------->
<!--   atualizar valores -->
<%     
        if (action.equals("insertValues")) {
            String nome = request.getParameter("nome");

            transacoes.PlanoDeSaude p = new transacoes.PlanoDeSaude();
            data.PlanoDeSaudeDO plano = new data.PlanoDeSaudeDO();
            plano.setNome(nome);

            boolean result = false;
            try {
               result = p.incluir(plano);
            } catch (Exception e) {
%>           <%= e.toString() %>
<%
            }
            if ( result ) {
            // avisar usuario que transacao foi feita com sucesso
%>
            Transa��o realizada com sucesso!
            <form action="./index.jsp" method="post">
               <input type="submit" name="voltar" value="Voltar" />
            </form>
<%          } else {
%>
            Erro ao atualizar dados do contato
            <form action="./insertPlano.jsp" method="post">
               <input type="submit" name="retry" value="Repetir" />
            </form>
<%          }
        } // updateValues
%>  
    
<% }
%>
    
</div> <!-- Content div -->

<%@ include file="footer.jsp" %>
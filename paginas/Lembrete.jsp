<%@ include file="header.jsp" %>
<%@ page import="java.text.SimpleDateFormat"%>
<%@ page import="java.text.DateFormat"%>
<%@ page import="java.util.Date"%>
<%@ page import="data.ConsultaDO" %>
<%@ page import="data.PacienteDO" %>
<%@ page import="data.MedicoDO" %>
<%@ page import="transacoes.Consulta" %>
<%@ page import="transacoes.Paciente" %>
<%@ page import="transacoes.Medico" %>
<%@ page import="java.util.Vector" %>

<div id="content">
    <h1>Lembrete</h1>
    <hr>
    
<! ------------------------------------------------------------------------->
<!--   se for o request inicial, mostrar somente o formulario de escolha  -->

<%     
    String action = request.getParameter("action");
    if ( null == action ) {
       action = "showSearchForm";
%>

    <form action="./index.jsp" method="post">
<%
    if ( session.getAttribute("user_name") == null) {
       pageContext.forward("index.jsp");
    }
       
    // Puxar todas as consultas do usuario
    transacoes.Consulta tn = new transacoes.Consulta();
    Vector consultas = tn.pesquisar( Integer.parseInt(userid), (Integer)session.getAttribute("user_type"));
%>
        <table>
          <tr>
             <td>Proximas consultas:</td>
             
             <tr>
                <td>Medico</td>
                <td>Data</td>
             </tr>
             
                     <%
                     for (int i = 0; i < consultas.size (); i++) {
                        ConsultaDO consulta = (ConsultaDO)consultas.get(i);
                        
                        
                        if ((Integer)session.getAttribute("user_type") == 1) { // Paciente
                            transacoes.Medico t = new transacoes.Medico();
                            MedicoDO medico = t.buscar(consulta.getMedicoId());
                     %>
                     
                <tr>
                   <td><%= medico.getNome() %></td>
                   <td><%= consulta.getData() %></td>
                   
                                                         
                </tr>
                     <% }
                     }
                     %>

                 </select>
             </td>
          </tr>
        </table>
        <input type="submit" name="voltar" value="Voltar" />
        
    </form>

<%  } 
%>
    
<! ------------------------------------------------------------------->

</div> <!-- Content div -->

<%@ include file="footer.jsp" %>

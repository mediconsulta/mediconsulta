<%-- 
    Document   : logout
    Created on : 11/11/2014, 10:10:05
    Author     : Edson
--%>

<%@ include file="header.jsp" %>

<div id="content">
    <%
    session.removeAttribute("user_name");
    session.removeAttribute("user_id");
    session.removeAttribute("user_type");
    pageContext.forward("index.jsp");
    %>
    
</div> <!-- Content div -->

<%@ include file="footer.jsp" %>

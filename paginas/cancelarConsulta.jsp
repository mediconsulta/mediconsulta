<%-- 
    Document   : cancelarConsulta
    Created on : Nov 7, 2014, 5:13:17 PM
    Author     : Edson Nakada
--%>

<%@ include file="header.jsp" %>
<%@ page import="java.text.SimpleDateFormat"%>
<%@ page import="java.text.DateFormat"%>
<%@ page import="java.util.Date"%>
<%@ page import="data.ConsultaDO" %>
<%@ page import="data.PacienteDO" %>
<%@ page import="data.MedicoDO" %>
<%@ page import="transacoes.Consulta" %>
<%@ page import="transacoes.Paciente" %>
<%@ page import="transacoes.Medico" %>
<%@ page import="java.util.Vector" %>

<div id="content">
    <h1>Cancelar Consulta</h1>
    <hr>
    
<! ------------------------------------------------------------------------->
<!--   se for o request inicial, mostrar somente o formulario de escolha  -->

<%     
    String action = request.getParameter("action");
    if ( null == action ) {
       action = "showSearchForm";
%>

    <form action="./cancelarConsulta.jsp" method="post">
<%
    // VERIFICACAO MANUAL DO LOGIN - Tirar o comment quando tiver login funcionando
    //if ( session.getAttribute("user_name") == null) {
    //   pageContext.forward("index.jsp");
    //}

    //String nome1 = (String)session.getAttribute("user_name");
       
    // Puxar todas as consultas do usuario
    transacoes.Consulta tn = new transacoes.Consulta();
    Vector consultas = tn.pesquisar( Integer.parseInt(subuserid), (Integer)session.getAttribute("user_type"));
%>
        <table>
          <tr>
             <td>Selecione a consulta que deseja cancelar</td>
             <td>
                 <select name="cid">
                     <%
                     for (int i = 0; i < consultas.size (); i++) {
                        ConsultaDO consulta = (ConsultaDO)consultas.get(i);
                        
                        
                        if ((Integer)session.getAttribute("user_type") == 1) { // Paciente
                            transacoes.Medico t = new transacoes.Medico();
                            MedicoDO medico = t.buscar(consulta.getMedicoId());
                     %>
                            <option value="<%= consulta.getId()%>">Dr(a). <%= medico.getNome() %> - <%= consulta.getData() %></option>
                     <% }
                        else if ((Integer)session.getAttribute("user_type") == 2) { // Medico
                            transacoes.Paciente t = new transacoes.Paciente();
                            PacienteDO paciente = t.buscar(consulta.getPacienteId());
                     %>
                            <option value="<%= consulta.getId()%>">Sr(a). <%= paciente.getNome() %> - <%= consulta.getData() %></option>
                     <% }
                     }
                     %>

                 </select>
             </td>
          </tr>
        </table>
        <input type="submit" name="enviar" value="Cancelar Consulta" />
        <input type="hidden" name="action" value="showSearchResults" />
    </form>

<%  } 
%>
    
<! ------------------------------------------------------------------------->
<!--   Confirmacao do cancelamento                                        -->
<%
    if (action.equals("showSearchResults")) {
        String cid = request.getParameter("cid");
        
        // Perguntar o motivo do cancelamento
%>
    <form action="./cancelarConsulta.jsp" method="post">

        <table>
            <tr>
               <td>Motivo da Desist&ecirc;ncia</td>
            </tr>
            <tr>
                <td><textarea name="motivo" rows="15" cols="50" maxlength="255" required></textarea></td>
            </tr>
        </table>
        <a href="index.jsp">Voltar</a>
        <input type="submit" name="enviar" value="Cancelar Consulta" />
        <input type="hidden" name="cid" value="<%= cid %>" />
        <input type="hidden" name="action" value="updateValues" />
        
    </form>
<%  } 
%>
    
<! ------------------------------------------------------------------->
<!--   atualizar valores -->
<%     
    if (action.equals("updateValues")) {
        String motivo = request.getParameter("motivo");
        int cid = Integer.parseInt( request.getParameter("cid") );

        transacoes.Consulta tn = new transacoes.Consulta();
        ConsultaDO consulta = tn.buscar(cid);
        
        consulta.setMotivo(motivo);
        consulta.setEstado(3); // Cancelado
        
        boolean result = false;
        try {
           result = tn.atualizar(consulta);
        } catch (Exception e) {
%>           <%= e.toString() %>
<%
        }
        
        // Incluir funcao de envio de email de notificacao
        
        if ( result ) {
        // avisar usuario que transacao foi feita com sucesso
%>
        Transa��o realizada com sucesso!
        <form action="./index.jsp" method="post">
           <input type="submit" name="voltar" value="Voltar" />
        </form>
<%      } else {
%>
        Erro ao atualizar dados do contato
        <form action="./cancelarConsulta.jsp" method="post">
           <input type="submit" name="retry" value="Repetir" />
        </form>
<%        }
    } // updateValues
%>

</div> <!-- Content div -->

<%@ include file="footer.jsp" %>

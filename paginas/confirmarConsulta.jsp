<%@ include file="header.jsp" %>
<%@ page import="java.text.SimpleDateFormat"%>
<%@ page import="java.text.DateFormat"%>
<%@ page import="java.util.Date"%>
<%@ page import="data.ConsultaDO" %>
<%@ page import="data.PacienteDO" %>
<%@ page import="data.MedicoDO" %>
<%@ page import="transacoes.Consulta" %>
<%@ page import="transacoes.Paciente" %>
<%@ page import="transacoes.Medico" %>
<%@ page import="java.util.Vector" %>

<div id="content">
    <h1>Confirmar Consulta</h1>
    <hr>
    
<! ------------------------------------------------------------------------->
<!--   se for o request inicial, mostrar somente o formulario de escolha  -->

<%     
    String action = request.getParameter("action");
    if ( null == action ) {
       action = "showSearchForm";
%>

    <form action="./confirmarConsulta.jsp" method="post">
<%
    if ( session.getAttribute("user_name") == null) {
       pageContext.forward("index.jsp");
    }
       
    // Puxar todas as consultas do usuario
    transacoes.Consulta tn = new transacoes.Consulta();
    Vector consultas = tn.pesquisar( Integer.parseInt(userid), (Integer)session.getAttribute("user_type"));
%>
        <table>
          <tr>
             <td>Selecione a consulta que deseja confirmar</td>
             <td>
                 <select name="cid">
                     <%
                     for (int i = 0; i < consultas.size (); i++) {
                        ConsultaDO consulta = (ConsultaDO)consultas.get(i);
                        
                        
                        if ((Integer)session.getAttribute("user_type") == 1) { // Paciente
                            transacoes.Medico t = new transacoes.Medico();
                            MedicoDO medico = t.buscar(consulta.getMedicoId());
                     %>
                            <option value="<%= consulta.getId()%>">Dr(a). <%= medico.getNome() %> - <%= consulta.getData() %></option>
                     <% }
                     }
                     %>

                 </select>
             </td>
          </tr>
        </table>
        <input type="submit" name="enviar" value="Confirmar Consulta" />
        <input type="hidden" name="action" value="confirmAppointment" />
    </form>

<%  } 
%>
    
<! ------------------------------------------------------------------->
<!--   confirmacao de consulta -->
<%     
    if (action.equals("confirmAppointment")) {
        int cid = Integer.parseInt( request.getParameter("cid") );

        transacoes.Consulta tn = new transacoes.Consulta();
        ConsultaDO consulta = tn.buscar(cid);
        
        consulta.setEstado(2); // Confirmado
        
        boolean result = false;
        try {
           result = tn.atualizar(consulta);
        } catch (Exception e) {
%>           <%= e.toString() %>
<%
        }
        
        // Incluir funcao de envio de email de notificacao
        
        if ( result ) {
        // avisar usuario que transacao foi feita com sucesso
%>
        Consulta confirmada com sucesso!
        <form action="./index.jsp" method="post">
           <input type="submit" name="voltar" value="Voltar" />
        </form>
<%      } else {
%>
        Erro ao confirmar consulta. Tente novamente!
        <form action="./confirmarConsulta.jsp" method="post">
           <input type="submit" name="retry" value="Repetir" />
        </form>
<%        }
    } // confirmAppointment
%>

</div> <!-- Content div -->

<%@ include file="footer.jsp" %>

<%@ include file="header.jsp" %>
<%@ page import="java.util.Vector" %>
<%@ page import="transacoes.Hospital" %>
<%@ page import="transacoes.Medico" %>
<%@ page import="transacoes.Especialidade" %>
<%@ page import="data.HospitalDO" %>
<%@ page import="data.MedicoDO" %>
<%@ page import="data.EspecialidadeDO" %>

<div id="content">
    <h1>Pesquisar por Hospitais</h1>
    <hr>
    
<! ------------------------------------------------------------------->
<!--   sempre mostrar o formulario de busca, ateh acao ser "voltar" -->

<%     if ( null != request.getParameter("voltar")) {
%>        <jsp:forward page="./index.jsp?action=" />
<%        return;
       }
%>

         <form action="./searchHospital.jsp" method="post">
<%
    // VERIFICACAO MANUAL DO LOGIN
    if ( session.getAttribute("user_name") == null) {
       pageContext.forward("index.jsp");
    }

    String nome1 = (String)session.getAttribute("user_name");
%>
           <table>
             <tr>
               <td>Hospital para pesquisar: </td>
               <td>
                   <select name="idHospital" />
                   
                   <%      transacoes.Hospital tn = new transacoes.Hospital();
        Vector hospitais = tn.ListarTodos();
        for(int i = 0; i < hospitais.size(); i++) {
                data.HospitalDO hospital = (data.HospitalDO)hospitais.elementAt(i); 
%>
                <option value="<%= hospital.getIdHospital()%>"><%= hospital.getNome() %></option>
                <%
        }
        %>
        </select>
        </td>
             </tr>
           </table>
           <input type="submit" name="pesquisar" value="Pesquisar" />
           <input type="submit" name="voltar" value="Voltar" />
         </form>

<! ------------------------------------------------------------------->
<!--   se nao for o request inicial, acionar a transacao de negocio -->


        
<%   if ( null != request.getParameter("pesquisar")) {  
       String idHospital = request.getParameter("idHospital");
       transacoes.Medico tr = new transacoes.Medico();
       int idHosp = Integer.parseInt(idHospital);
       Vector Medicos = tr.pesquisarHospital(idHosp);
       if ( (Medicos == null) || (Medicos.size() == 0)) {
         // avisar usuario que nao ha' contatos com este nome
%>
          Nenhum m�dico foi encontrado!
          <form action="./index.jsp" method="post">
             <input type="submit" name="voltar" value="Voltar" />
          </form>
<%     } else {
%>
          <table class="results-table">
             <tr>
                <th>Nome</th>
                <th>CRM</th>
                <th>Endere�o</th>
                <th>N�mero</th>
                <th>Complemento</th>
                <th>CEP</th>
                <th>Bairro</th>
                <th>Cidade</th>
                <th>Estado</th>
                <th>Telefone</th>
                <th>Especialidade</th>
             </tr>
<%           for(int i = 0; i < Medicos.size(); i++) {
                MedicoDO Medico = (MedicoDO)Medicos.elementAt(i);
                int idMedico = Medico.getIdMedico();
                transacoes.Especialidade t = new transacoes.Especialidade();
                Vector especialidades = t.pesquisarPorMedico(idMedico);
%>              <tr>
                   <td><%= Medico.getNome() %></td>
                   <td><%= Medico.getCRM() %></td>
                   <td><%= Medico.getEndereco() %></td>
                   <td><%= Medico.getNumero() %></td>
                   <td><%= Medico.getComplemento() %></td>
                   <td><%= Medico.getCEP() %></td>
                   <td><%= Medico.getBairro()%></td>
                   <td><%= Medico.getCidade() %></td>
                   <td><%= Medico.getEstado() %></td>
                   <td><%= Medico.getTelefone() %></td>
                   <td>
                   <% for (int j=0;j<especialidades.size();j++) { 
                         EspecialidadeDO especialidade = (EspecialidadeDO)especialidades.elementAt(j);
                   %>
                       <%= especialidade.getNome()%><br>
                   <%
                   }
                   %>
                   </td>                
                </tr>        
<%           } // for i      
%>        </table>            
<%     } // contatos retornados
     } // pesquisar
%>
    <!-- Seu codigo aqui! -->
    
</div> <!-- Content div -->

<%@ include file="footer.jsp" %>

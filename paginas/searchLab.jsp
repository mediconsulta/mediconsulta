<%-- 
    Document   : searchLab
    Created on : 02/11
    Author     : Ivan
--%>

<%@ include file="header.jsp" %>
<%@ page import="java.util.Vector" %>
<%@ page import="transacoes.Laboratorio" %>
<%@ page import="data.LaboratorioDO" %>

<div id="content">
    <h1>Pesquisar por laborat&oacute;rio</h1>
    <hr>
    
<! ------------------------------------------------------------------->
<!--   sempre mostrar o formulario de busca, ateh acao ser "voltar" -->

<%     if ( null != request.getParameter("voltar")) {
%>        <jsp:forward page="./index.jsp" />
<%        return;
       }
%>

         <form action="./searchLab.jsp" method="post">
<%
    // VERIFICACAO MANUAL DO LOGIN
    if ( session.getAttribute("user_name") == null) {
       pageContext.forward("index.jsp");
    }

    String nome1 = (String)session.getAttribute("user_name");
%>
           <table>
             <tr>
               <td>Nome do laborat&oacute;rio: </td>
               <td><input type="text" name="nome" />
               <td>Rua: </td>
               <td><input type="text" name="rua" />
               <td>CEP: </td>
               <td><input type="text" name="CEP" />
               <td>Cidade: </td>
               <td><input type="text" name="cidade" />
               <td>Estado: </td>
               <td><input type="text" name="estado" />
             </tr>
            </table>
           <br>
            <p>Tipo de exame desejado</p>
            
            <select name="exames">
                <option value="0"> </option>
                <option value="1">Flu&iacute;dos</option>
                <option value="2">Ortop&eacute;dicos</option>
                <option value="3">Respirat&oacute;rios</option>
            </select>
            <br>
            <input type="submit" name="pesquisar" value="Pesquisar" />
            <input type="submit" name="voltar" value="Voltar" />
         </form>

<! ------------------------------------------------------------------->
<!--   se nao for o request inicial, acionar a transacao de negocio -->


<%   if ( null != request.getParameter("pesquisar")) {  
       String nome = request.getParameter("nome");
       String endereco = request.getParameter("rua");
       String cep = request.getParameter("CEP");
       String cidade = request.getParameter("cidade");
       String estado = request.getParameter("estado");
       String tipo = request.getParameter("exames");
       transacoes.Laboratorio tn = new transacoes.Laboratorio();
       Vector laboratorios = tn.pesquisar(nome, endereco, cep, cidade, estado, tipo);
       if ( (laboratorios == null) || (laboratorios.size() == 0)) {
         // avisar usuario que nao ha' contatos com este nome
%>
          Nenhum laborat&oacute;rio encontrado!
          <form action="./main.jsp" method="post">
             <input type="submit" name="voltar" value="Voltar" />
          </form>
<%     } else {
%>
        <table class="results-table">
             <tr>
                <th>Nome</th>
                <th>Telefone</th>
                <th>Endere&ccedil;o</th>
                <th>Website</th>
                <th>Email</th>
             </tr>
<%           for(int i = 0; i < laboratorios.size(); i++) {
                LaboratorioDO lab = (LaboratorioDO)laboratorios.elementAt(i);
%>              <tr>
                   <td><%= lab.getNome() %></td>
                   <td><%= lab.getTelefone() %></td>
                   <td><%= lab.getEndereco() %></td>
                   <td><%= lab.getWebsite() %></td>
                   <td><%= lab.getEmail() %></td>
                </tr>        
<%           } // for i      
%>        </table>            
<%     } // contatos retornados
     } // pesquisar
%>

    
</div> <!-- Content div -->
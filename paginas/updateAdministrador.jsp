<%-- 
    Document   : updatePacient
    Created on : 23/11/2014
    Author     : Leonardo Sommer
--%>

<%@ include file="header.jsp" %>
<%@ page import="java.text.SimpleDateFormat"%>
<%@ page import="java.text.DateFormat"%>
<%@ page import="java.util.Date"%>
<%@ page import="data.AdministradorDO" %>
<%@ page import="transacoes.Administrador" %>
<%@ page import="java.util.Vector" %>

<div id="content">
    <h1>Atualizar Perfil de Administrador</h1>
    <hr>
    
<! ------------------------------------------------------------------------->
<!--   se for o request inicial, mostrar somente o formulario de pesquisa -->

<%     
    String action = request.getParameter("action");
    if ( null == action ) {
       action = "showSearchForm";
       
        // VERIFICACAO MANUAL DO LOGIN - Tirar o comment quando tiver login funcionando
        if ( (Integer)session.getAttribute("user_type")==0 ) {
            response.sendRedirect("updateAdministrador.jsp?action=showEditForm&id="+subuserid);
        }
        else  {
            response.sendRedirect("index.jsp");
        }
%>

    <form id="update-p-search-form" action="./updateAdministrador.jsp" method="post">
        <table>
          <tr>
             <td>Nome para pesquisar</td>
             <td><input type="text" name="nome" />
          </tr>
        </table>
        <input type="submit" name="pesquisar" value="pesquisar" />
        <input type="hidden" name="action" value="showSearchResults" />
    </form>

<%  } 
%>

<! ------------------------------------------------------------------------->
<!--   faz a pesquisa e mostra os resultados                              -->
<%
    if (action.equals("showSearchResults")) {
        String nome = request.getParameter("nome");
        transacoes.Administrador tn = new transacoes.Administrador();
        Vector administradores = tn.pesquisar(nome);
        if ( (administradores == null) || (administradores.size() == 0)) {
        // avisar usuario que nao ha' contatos com este nome
%>
        Nenhum contato com este nome foi encontrado!
        <form action="./updateAdministrador.jsp" method="post">
            <%= nome %>
            <input type="submit" name="voltar" value="Voltar" />
        </form>
<%      } else {
%>
        <table id="update-p-show-results">
            <tr>
               <td>Nome</td>
               <td>CPF</td>
               <td>Endereco</td>
               <td>Numero</td>
               <td>Complemento</td>
               <td>Bairro</td>
               <td>Cidade</td>
               <td>Estado</td>
               <td>Nascimento</td>
               <td>Telefone</td>
            </tr>
<%          for(int i = 0; i < administradores.size(); i++) {
            AdministradorDO administrador = (AdministradorDO)administradores.elementAt(i);
%>          <tr>
               <td><%= administrador.getNome() %></td>
               <td><%= administrador.getCpf() %></td>
               <td><%= administrador.getEndereco() %></td>
               <td><%= administrador.getNumero() %></td>
               <td><%= administrador.getComplemento() %></td>
               <td><%= administrador.getBairro() %></td>
               <td><%= administrador.getCidade() %></td>
               <td><%= administrador.getEstado() %></td>
               <td><%= administrador.getNascimento() %></td>
               <td><%= administrador.getTelefone() %></td>
               <td><a href="updateAdministrador.jsp?action=showEditForm&id=<%= administrador.getId()%>">Editar</a>
            </tr>        
<%          } // for i      
%>      </table>            
<%      } // contatos retornados

    } // pesquisar
%>

<! ------------------------------------------------------------------->
<!--   mostra formulario para atualizacao                           -->
<%  
    if (action.equals("showEditForm")) {
    int id = Integer.parseInt(request.getParameter("id"));
    System.out.println(id);
    transacoes.Administrador p = new transacoes.Administrador();
    data.AdministradorDO administrador = p.buscar(id);

    DateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
    String date = formatter.format( administrador.getNascimento() );
%>        
         <form action="./updateAdministrador.jsp" method="post">
            <table>
                <tr>
                   <td>Nome</td>
                   <td><input type="text" name="nome" value="<%= administrador.getNome() %>" required /></td>
                </tr>
                <tr>
                   <td>CPF</td>
                   <td><input type="text" name="cpf" value="<%= administrador.getCpf() %>" required />
                </tr>
                <tr>
                   <td>Endereco</td>
                   <td><input type="text" name="endereco" value="<%= administrador.getEndereco() %>" required /></td>
                </tr>
                <tr>
                   <td>Numero</td>
                   <td><input type="text" name="numero" value="<%= administrador.getNumero() %>" required /></td>
                </tr>
                <tr>
                   <td>Complemento</td>
                   <td><input type="text" name="complemento" value="<%= administrador.getComplemento() %>"/></td>
                </tr>
                <tr>
                   <td>Bairro</td>
                   <td><input type="text" name="bairro" value="<%= administrador.getBairro() %>" required /></td>
                </tr>
                <tr>
                   <td>Cidade</td>
                   <td><input type="text" name="cidade" value="<%= administrador.getCidade() %>" required /></td>
                </tr>
                <tr>
                   <td>Estado</td>
                   <td><input type="text" name="estado" value="<%= administrador.getEstado() %>" required /></td>
                </tr>
                <tr>
                   <td>Data de Nascimento</td>
                   <td><input type="text" name="nascimento" value="<%= date %>" required />
                </tr>
                <tr>
                   <td>Telefone</td>
                   <td><input type="text" name="telefone" value="<%= administrador.getTelefone() %>" required />
                </tr>
            </table>

            <input type="submit" name="atualizar" value="Atualizar" />
            <a href="updateAdministrador.jsp">Voltar</a>
            <input type="hidden" name="id" value=<%= id %> />
            <input type="hidden" name="action" value="updateValues" />
        </form>
<%         
    } // showEditForm
%>

<! ------------------------------------------------------------------->
<!--   atualizar valores -->
<%     
    if (action.equals("updateValues")) {
        String nome = request.getParameter("nome");
        String cpf = request.getParameter("cpf");
        String endereco = request.getParameter("endereco");
        int numero = Integer.parseInt (request.getParameter("numero"));
        String complemento = request.getParameter("complemento");
        String bairro = request.getParameter("bairro");
        String cidade = request.getParameter("cidade");
        String estado = request.getParameter("estado");
        String nascimento = request.getParameter("nascimento");
        String telefone = request.getParameter("telefone");
        int id = Integer.parseInt(request.getParameter("id"));

        DateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
        Date date = formatter.parse(nascimento);

        transacoes.Administrador p = new transacoes.Administrador();
        data.AdministradorDO administrador = new data.AdministradorDO();
        administrador.setId(id);
        administrador.setNome(nome);
        administrador.setCpf(cpf);
        administrador.setEndereco(endereco);
        administrador.setNumero(numero);
        administrador.setComplemento(complemento);
        administrador.setBairro(bairro);
        administrador.setCidade(cidade);
        administrador.setEstado(estado);
        administrador.setCpf(cpf);
        administrador.setNascimento( date );
        administrador.setTelefone(telefone); 

        boolean result = false;
        try {
           result = p.atualizar(administrador);
        } catch (Exception e) {
%>           <%= e.toString() %>
<%
        }
        if ( result ) {
        // avisar usuario que transacao foi feita com sucesso
%>
        Transa��o realizada com sucesso!
        <form action="./index.jsp" method="post">
           <input type="submit" name="voltar" value="Voltar" />
        </form>
<%      } else {
%>
        Erro ao atualizar dados do contato
        <form action="./updateAdministrador.jsp" method="post">
           <input type="submit" name="retry" value="Repetir" />
        </form>
<%        }
    } // updateValues
%>

</div> <!-- Content div -->

<%@ include file="footer.jsp" %>

<%@ include file="header.jsp" %>
<%@ page import="java.text.SimpleDateFormat"%>
<%@ page import="java.text.DateFormat"%>
<%@ page import="java.util.Date"%>
<%@ page import="data.ConsultaDO" %>
<%@ page import="data.PacienteDO" %>
<%@ page import="data.MedicoDO" %>
<%@ page import="transacoes.Consulta" %>
<%@ page import="transacoes.Paciente" %>
<%@ page import="transacoes.Medico" %>
<%@ page import="java.util.Vector" %>

<div id="content">
    <h1>Registre o valor pago na sua consulta</h1>
    <hr>
    
<!--   se for o request inicial, mostrar somente o formulario de escolha  -->  
    <%     
    String action = request.getParameter("action");
    if ( null == action ) {
       action = "showSearchForm";
%>
    
    <form action="./precopago.jsp" method="post">
<%
    // VERIFICACAO MANUAL DO LOGIN 
    if ( session.getAttribute("user_name") == null) {
       pageContext.forward("index.jsp");
    }

    String nome1 = (String)session.getAttribute("user_name");
    
    // Puxar todas as consultas do usuario
    transacoes.Consulta tn = new transacoes.Consulta();
    Vector consultas = tn.pesquisar4( Integer.parseInt(userid), (Integer)session.getAttribute("user_type"));
%>

 <table>
          <tr>
             <td>Selecione a consulta para registrar um valor</td>
             <td>
                 <select name="cid">
                     <%
                     for (int i = 0; i < consultas.size (); i++) {
                        ConsultaDO consulta = (ConsultaDO)consultas.get(i);
                        
                        
                        if ((Integer)session.getAttribute("user_type") == 1) { // Paciente
                            transacoes.Medico t = new transacoes.Medico();
                            MedicoDO medico = t.buscar(consulta.getMedicoId());
                     %>
                            <option value="<%= consulta.getId()%>">Dr(a). <%= medico.getNome() %> - <%= consulta.getData() %></option>
                     <% }
                        else if ((Integer)session.getAttribute("user_type") == 2) { // Medico
                            transacoes.Paciente t = new transacoes.Paciente();
                            PacienteDO paciente = t.buscar(consulta.getPacienteId());
                     %>
                            <option value="<%= consulta.getId()%>">Sr(a). <%= paciente.getNome() %> - <%= consulta.getData() %></option>
                     <% }
                     }
                     %>

                 </select>
             </td>
          </tr>
        </table>
        <input type="submit" name="enviar" value="Registrar pre&ccedil;o da consulta" />
        <input type="hidden" name="action" value="showSearchResults" />
    </form>
<%  } 
%>

<%
    if (action.equals("showSearchResults")) {
        String cid = request.getParameter("cid");
        
        // Perguntar o motivo do cancelamento
%>
    <form action="./precopago.jsp" method="post">

        <table>
            <tr>
               <td>Entre com o valor pago na consulta </td>
            </tr>
            <tr>
                <td>R$ <input name="tipo" type="number" step="0.01" min="0"></td>
            </tr>
        </table>
        <a href="index.jsp">Voltar</a>
        <input type="submit" name="enviar" value="Enviar valor da consulta" />
        <input type="hidden" name="cid" value="<%= cid %>" />
        <input type="hidden" name="action" value="updateValues" />
        
    </form>
<%  } 
%>

<!--   atualizar valores -->

<%     
    if (action.equals("updateValues")) {
        String type = request.getParameter("tipo");
        float t = Float.parseFloat(type);
        int cid = Integer.parseInt( request.getParameter("cid") );

        transacoes.Consulta tn = new transacoes.Consulta();
        ConsultaDO consulta = tn.buscar(cid);
        
        consulta.setPreco(t);
        
        boolean result = false;
        try {
           result = tn.atualizar(consulta);
        } catch (Exception e) {
%>           <%= e.toString() %>
<%
        }
        
        if ( result ) {
        // avisar usuario que transacao foi feita com sucesso
%>
        Pre&ccedil;o enviado com sucesso!
        <form action="./index.jsp" method="post">
           <input type="submit" name="voltar" value="Voltar" />
        </form>
<%      } else {
%>
        Erro ao incluir o valor da consulta
        <form action="./precopago.jsp" method="post">
           <input type="submit" name="retry" value="Repetir" />
        </form>
<%        }
    } // updateValues
%>

    
</div> <!-- Content div -->

<%@ include file="footer.jsp" %>
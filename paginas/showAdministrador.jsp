<%-- 
    Document   : showAdministrador
    Created on : 23/11/2014, 23:56:29
    Author     : Leonardo Sommer
--%>

<%@ include file="header.jsp" %>
<%@ page import="java.text.SimpleDateFormat"%>
<%@ page import="java.text.DateFormat"%>
<%@ page import="java.util.Date"%>
<%@ page import="data.AdministradorDO" %>
<%@ page import="transacoes.Administrador" %>
<%@ page import="data.ConsultaDO" %>
<%@ page import="transacoes.Consulta" %>

<%
// Try checking if a parameter was posted and check if I have permission
int pid = 0;
if (request.getParameter("pid")!= null) {
    if((Integer)session.getAttribute("user_type")== 0 && Integer.parseInt(request.getParameter("pid"))==Integer.parseInt(subuserid)) {
        pid = Integer.parseInt(subuserid);
    }
    if((Integer)session.getAttribute("user_type")== 1 && Integer.parseInt(request.getParameter("pid"))==Integer.parseInt(subuserid)) {
        pid = Integer.parseInt(subuserid);
    }
    if((Integer)session.getAttribute("user_type")== 2) {
        transacoes.Consulta tn = new transacoes.Consulta();
        if (tn.checkPM(Integer.parseInt(subuserid),Integer.parseInt(request.getParameter("pid"))).size() > 0 )
            pid = Integer.parseInt( request.getParameter("pid") );
    }
}
//System.out.println(pid);

%>

<div id="content">
    <%
    // Posso mostrar um resultado
    if (pid !=0) {
        transacoes.Administrador tn = new transacoes.Administrador();
        AdministradorDO administrador = tn.buscar(pid);
        
        
        DateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
        String date = formatter.format( administrador.getNascimento() );
    %>
    <h1>Sr./Sra. <%= administrador.getNome() %></h1>
    <hr>
    
    <ul>
        <li>Nome Completo: <%= administrador.getNome() %></li>
        <li>CPF: <%= administrador.getCpf() %></li>
        <li>Endereco: <%= administrador.getEndereco() %></li>
        <li>Numero: <%= administrador.getNumero() %></li>
        <li>Complemento: <%= administrador.getComplemento() %></li>
        <li>Bairro: <%= administrador.getBairro() %></li>
        <li>Cidade: <%= administrador.getCidade() %></li>
        <li>Estado: <%= administrador.getEstado() %></li>
        <li>Data de Nascimento: <%= date %></li>
        <li>Telefone: <%= administrador.getTelefone() %></li>
    </ul>
    
    <% 
    } else {
    %>
    Voc&ecirc; n&atilde;o tem permiss&atilde;o para ver o perfil deste usu&aacute;rio.
    
    <% }
    %>
</div> <!-- Content div -->

<%@ include file="footer.jsp" %>
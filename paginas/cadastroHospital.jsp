<%@ include file="header.jsp" %>
<%@ page import="transacoes.Hospital" %>
<%@ page import="data.HospitalDO" %>

<div id="content">
    <h1>Cadastro de Hospital</h1>
    <hr>

<! ------------------------------------------------------------>
<!--   se for o request inicial, mostrar somente o formulario -->

<%     if ( null == request.getParameterValues("cadastrar") ) {
%>
       <form action="./cadastroHospital.jsp" method="post">
<%
    // VERIFICACAO MANUAL DO LOGIN
/*
    if ( session.getAttribute("user_name") == null) {
       pageContext.forward("index.jsp");
    }

    String nome = (String)session.getAttribute("user_name"); 
*/
    String[] estados = {"AC", "AL", "AP", "AM", "BA", "CE", "DF", "ES", "GO", 
                        "MA", "MT", "MS", "MG", "PA", "PB", "PR", "PE", "PI", 
                        "RJ", "RN", "RS", "RO", "RR", "SC", "SP", "SE", "TO"};
%>

    <table>
        <tr><td>Nome do hospital</td><td><input name="nome" type="text"></td></tr>
        <tr><td><b>Endere�o</b></td><td></td></tr>
        <tr><td>Rua</td><td><input name="endereco" type="text"></td></tr>
        <tr><td>N�mero</td><td><input name="numero" type="text"></td></tr>
        <tr><td>Complemento</td><td><input name="complemento" type="text"></td></tr>
        <tr><td>CEP</td><td><input name="cep" type="text"></td></tr>
        <tr><td>Bairro</td><td><input name="bairro" type="text"></td></tr>
        <tr><td>Cidade</td><td><input name="cidade" type="text"></td></tr>
        <tr><td>Estado</td>
            <td>
                <select name="estado">
                    <option value="">Selecione</option>
                    <% for (int i = 0; i < estados.length; i++) { %>
                    <option value="<%=estados[i]%>"><%=estados[i]%></option>
                    <% } %>
                </select>
            </td></tr>
        <tr><td><b>Pronto socorro</b></td><td></td></tr>
        <tr><td>Possui pronto socorro</td><td><input type="checkbox" name="ps" value="Pronto Socorro"></td></tr>
        <tr><td></td><td><input type="submit" name="cadastrar" value="Cadastrar"></td>
   </table>
   </form>

<%      } else { 
%>
<! ------------------------------------------------------------------->
<!--   se nao for o request inicial, acionar a transacao de negocio -->


<%     String nome = request.getParameter("nome");
       String endereco = request.getParameter("endereco");
       String numero = request.getParameter("numero");
       String complemento = request.getParameter("complemento");
       String cep = request.getParameter("cep");
       String bairro = request.getParameter("bairro");
       String cidade = request.getParameter("cidade");
       String estado = request.getParameter("estado");
       String ps = request.getParameter("ps");
       ps = (ps == null) ? "n" : "s";   
       
       boolean error = false;
       
       transacoes.Hospital tn = new transacoes.Hospital();
       data.HospitalDO hospital = new data.HospitalDO();

       hospital.setNome(nome);
       hospital.setEndereco(endereco);
       try {
         hospital.setNumero(Integer.parseInt(numero));
       } catch (Exception e) {
         e.printStackTrace();
         error = true;
       }
       hospital.setComplemento(complemento);
       hospital.setCep(cep);
       hospital.setBairro(bairro);
       hospital.setCidade(cidade);
       hospital.setEstado(estado);
       hospital.setProntoSocorro(ps);
       if (!error && tn.incluir(hospital)) {
         // avisar usuario que transacao foi feita com sucesso
%>
          Transa��o realizada com sucesso!
          <form action="./index.jsp" method="post">
             <input type="submit" name="voltar" value="Voltar" />
          </form>
          
<%     } else {
%>
          Erro ao incluir hospital!
          <form action="./cadastroHospital.jsp" method="post">
             <input type="submit" name="retry" value="Repetir" />
          </form>
<%    }
    }
%>

</div> <!-- Content div -->

<%@ include file="footer.jsp" %>
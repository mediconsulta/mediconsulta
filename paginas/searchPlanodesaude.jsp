<%@ include file="header.jsp" %>
<%@ page import="java.util.Vector" %>
<%@ page import="transacoes.PlanoDeSaude" %>
<%@ page import="transacoes.Medico" %>
<%@ page import="data.PlanoDeSaudeDO" %>
<%@ page import="data.MedicoDO" %>

<div id="content">
    <h1>Pesquisar Plano De Saude</h1>
    <hr>
    
<! ------------------------------------------------------------------->
<!--   sempre mostrar o formulario de busca, ateh acao ser "voltar" -->

<%     if ( null != request.getParameter("voltar")) {
%>        <jsp:forward page="./index.jsp" />
<%        return;
       }
%>

         <form action="./searchPlanodesaude.jsp" method="post">
<%
    // VERIFICACAO MANUAL DO LOGIN
    if ( session.getAttribute("user_name") == null) {
       pageContext.forward("index.jsp");
    }

    String nome1 = (String)session.getAttribute("user_name");
%>
           <table>
             <tr>
               <td>Plano de Saude para pesquisar: </td>
               <td>
                   <select name="idPlano" />
                   
                   <%      transacoes.PlanoDeSaude tn = new transacoes.PlanoDeSaude();
        Vector plano = tn.ListarTodos();
        for(int i = 0; i < plano.size(); i++) {
                PlanoDeSaudeDO Plano = (PlanoDeSaudeDO)plano.elementAt(i); 
%>
                <option value="<%= Plano.getId() %>"><%= Plano.getNome() %></option>
                <%
        }
        %>
        </select>
        </td>
             </tr>
           </table>
           <input type="submit" name="pesquisar" value="Pesquisar" />
           <input type="submit" name="voltar" value="Voltar" />
         </form>

<! ------------------------------------------------------------------->
<!--   se nao for o request inicial, acionar a transacao de negocio -->


        
<%   if ( null != request.getParameter("pesquisar")) {  
       String nome = request.getParameter("idPlano");
       transacoes.Medico tnm = new transacoes.Medico();
       int idnome = Integer.parseInt(nome);
       Vector Medicos = tnm.pesquisarP(idnome);
       if ( (Medicos == null) || (Medicos.size() == 0)) {
         // avisar usuario que nao ha' contatos com este nome
%>
          Nenhum Plano de Saude com este nome foi encontrado!
          <form action="./index.jsp" method="post">
             <input type="submit" name="voltar" value="Voltar" />
          </form>
<%     } else {
%>
          <table class="results-table">
             <tr>
                <th>Nome</th>
                <th>CRM</th>
                <th>Endere�o</th>
                <th>N�mero</th>
                <th>Complemento</th>
                <th>CEP</th>
                <th>Bairro</th>
                <th>Cidade</th>
                <th>Estado</th>
                <th>Telefone</th>
             </tr>
<%           for(int i = 0; i < Medicos.size(); i++) {
                MedicoDO Medico = (MedicoDO)Medicos.elementAt(i);
%>              <tr>
                   <td><%= Medico.getNome() %></td>
                   <td><%= Medico.getCRM() %></td>
                   <td><%= Medico.getEndereco() %></td>
                   <td><%= Medico.getNumero() %></td>
                   <td><%= Medico.getComplemento() %></td>
                   <td><%= Medico.getCEP() %></td>
                   <td><%= Medico.getBairro()%></td>
                   <td><%= Medico.getCidade() %></td>
                   <td><%= Medico.getEstado() %></td>
                   <td><%= Medico.getTelefone() %></td>
                                                         
                </tr>        
<%           } // for i      
%>        </table>            
<%     } // contatos retornados
     } // pesquisar
%>
    <!-- Seu codigo aqui! -->
    
</div> <!-- Content div -->

<%@ include file="footer.jsp" %>

<%@ include file="header.jsp" %>
<%@ page import="java.text.SimpleDateFormat"%>
<%@ page import="java.text.DateFormat"%>
<%@ page import="java.util.Date"%>
<%@ page import="data.MedicoDO" %>
<%@ page import="transacoes.Medico" %>
<%@ page import="java.util.Vector" %>
<%@ page import="data.PlanoDeSaudeDO" %>
<%@ page import="data.EspecialidadeDO" %>
<%@ page import="data.HospitalDO" %>
<%@ page import="transacoes.Usuario" %>
<%@ page import="data.UsuarioDO" %>

<%
// Try checking if a parameter was posted and check if I have permission
int pid = 0;
if (request.getParameter("pid")!= null) {
    if((Integer)session.getAttribute("user_type")== 0) {
        pid = Integer.parseInt( request.getParameter("pid") );
    }
    if((Integer)session.getAttribute("user_type")== 1 && Integer.parseInt(request.getParameter("pid"))==Integer.parseInt(subuserid)) {
        pid = Integer.parseInt(subuserid);
    }
    
    if((Integer)session.getAttribute("user_type")== 2 && Integer.parseInt(request.getParameter("pid"))==Integer.parseInt(subuserid)) {
            pid = Integer.parseInt(subuserid);
    }
}
//System.out.println(pid);

%>

<div id="content">
    <%
    // Posso mostrar um resultado
    if (pid !=0) {
        transacoes.Medico tn = new transacoes.Medico();
        MedicoDO Medico = tn.buscar(pid);
                
        DateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
        String date = formatter.format( Medico.getDataDeNascimento() );
    %>
    <h1>Dr./Dra. <%= Medico.getNome() %></h1>
    <hr>
    
    <ul>Dados Gerais
        <li>Nome Completo: <%= Medico.getNome() %></li>
        <li>CPF: <%= Medico.getCPF() %></li>
        <li>CRM: <%= Medico.getCRM() %></li>
        <li>Data de Nascimento: <%= date %></li>
        <li>Telefone: <%= Medico.getTelefone() %></li>
        <br>
        <li>Endere�o: <%= Medico.getEndereco()+", "+ Medico.getNumero() %></li>
        <%
        if( Medico.getComplemento() != null) {
        %>
            <li>Complemento: <%= Medico.getComplemento()%></li>
        <% }
        %>
        <li>CEP: <%= Medico.getCEP() %></li>
        <li>Bairro: <%= Medico.getBairro() %></li>
        <li>Cidade/Estado: <%= Medico.getCidade()+" - "+ Medico.getEstado() %></li>
    </ul>
    
    <% 
    transacoes.PlanoDeSaude tnp = new transacoes.PlanoDeSaude();
    Vector planos = tnp.pesquisarPorMedico( Integer.parseInt(subuserid) );
    %>
    <br>
    <ul>Plano(s) de sa�de
    
    <%
    for(int i = 0; i < planos.size(); i++) {
        PlanoDeSaudeDO ps = (PlanoDeSaudeDO)planos.elementAt(i);
    %>
            <li><%= ps.getNome() %>
    <% } // for i
    %>
    </ul>
    
<% 
    transacoes.Especialidade tne = new transacoes.Especialidade();
    Vector especialidades = tne.pesquisarPorMedico(Integer.parseInt(subuserid));             
%>
    <br>
    <ul>Especialidades
    
    <%
    for(int i = 0; i < especialidades.size(); i++) {
        EspecialidadeDO es = (EspecialidadeDO)especialidades.elementAt(i);
    %>
            <li><%= es.getNome() %>
    <% } // for i
    %>
    </ul>

<% 
    transacoes.Hospital tnh = new transacoes.Hospital();
    Vector hospitais = tnh.pesquisarPorMedico(Integer.parseInt(subuserid));             
%>
    <br>
    <ul>Hospitais de Atendimento
    
    <%
    for(int i = 0; i < hospitais.size(); i++) {
        HospitalDO ho = (HospitalDO)hospitais.elementAt(i);
    %>
            <li><%= ho.getNome() %>
    <% } // for i
    %>
    </ul>
    
    <% 
    } else {
    %>
    Voc&ecirc; n&atilde;o tem permiss&atilde;o para ver o perfil deste usu&aacute;rio.
    
    <% }
    %>
</div> <!-- Content div -->

<%@ include file="footer.jsp" %>
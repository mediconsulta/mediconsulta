<%-- 
    Document   : updateMedico
    Created on : 10/11/2014
    Author     : Bruno Nakagawa
--%>

<%@ include file="header.jsp" %>
<%@ page import="java.text.SimpleDateFormat"%>
<%@ page import="java.text.DateFormat"%>
<%@ page import="java.util.Date"%>
<%@ page import="data.MedicoDO" %>
<%@ page import="transacoes.Medico" %>
<%@ page import="java.util.Vector" %>
<%@ page import="data.PlanoDeSaudeDO" %>
<%@ page import="data.EspecialidadeDO" %>
<%@ page import="data.HospitalDO" %>
<%@ page import="transacoes.Usuario" %>
<%@ page import="data.UsuarioDO" %>

<div id="content">
    <h1>Atualizar Perfil de Medico</h1>
    <hr>
    
<! ------------------------------------------------------------------------->
<!--   se for o request inicial, mostrar somente o formulario de pesquisa -->

<%     
    String action = request.getParameter("action");
    if ( null == action ) {
       action = "showSearchForm";
       
       // VERIFICACAO MANUAL DO LOGIN - Tirar o comment quando tiver login funcionando
        if ( (Integer)session.getAttribute("user_type")==2 ) {
            response.sendRedirect("updateMedico.jsp?action=showEditForm&id="+subuserid);
        }
        else if( (Integer)session.getAttribute("user_type")==1 ) {
            response.sendRedirect("index.jsp");
        }
%>

    <form id="update-p-search-form" action="./updateMedico.jsp" method="post">
        <table>
          <tr>
             <td>Nome para pesquisar</td>
             <td><input type="text" name="nome" />
          </tr>
        </table>
        <input type="submit" name="pesquisar" value="pesquisar" />
        <input type="hidden" name="action" value="showSearchResults" />
    </form>

<%  } 
%>

<! ------------------------------------------------------------------------->
<!--   faz a pesquisa e mostra os resultados                              -->
<%
    if (action.equals("showSearchResults")) {
        String nome = request.getParameter("nome");
        transacoes.Medico tn = new transacoes.Medico();
        Vector medicos = tn.pesquisarN(nome);
        if ( (medicos == null) || (medicos.size() == 0)) {
        // avisar usuario que nao ha' contatos com este nome
%>
        Nenhum contato com este nome foi encontrado!
        <form action="./updateMedico.jsp" method="post">
            <%= nome %>
            <input type="submit" name="voltar" value="Voltar" />
        </form>
<%      } else {
%>
        <table id="update-p-show-results">
            <tr>
               <td>Nome</td>
               <td>CPF</td>
               <td>CRM</td>
               <td>Nascimento</td>
               <td>Telefone</td>
            </tr>
<%          for(int i = 0; i < medicos.size(); i++) {
            MedicoDO medico = (MedicoDO)medicos.elementAt(i);
%>          <tr>
               <td><%= medico.getNome() %></td>
               <td><%= medico.getCPF() %></td>
               <td><%= medico.getCRM() %></td>
               <td><%= medico.getDataDeNascimento() %></td>
               <td><%= medico.getTelefone() %></td>
               <td><a href="updateMedico.jsp?action=showEditForm&id=<%= medico.getIdMedico()%>">Editar</a>
            </tr>        
<%          } // for i      
%>      </table>            
<%      } // contatos retornados

    } // pesquisar
%>

<! ------------------------------------------------------------------->
<!--   mostra formulario para atualizacao                           -->
<%  
    if (action.equals("showEditForm")) {
    int id = Integer.parseInt(request.getParameter("id"));
    transacoes.Medico m = new transacoes.Medico();
    data.MedicoDO medico = m.buscar(id);
    System.out.println(medico.getNome());
    DateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
    String date = formatter.format( medico.getDataDeNascimento() );
    
    transacoes.PlanoDeSaude tnp1 = new transacoes.PlanoDeSaude();
    Vector planos1 = tnp1.pesquisarPorMedico( Integer.parseInt(subuserid) );
    
    transacoes.Especialidade tne1 = new transacoes.Especialidade();
    Vector especialidades1 = tne1.pesquisarPorMedico(Integer.parseInt(subuserid));
    
    transacoes.Hospital tnh1 = new transacoes.Hospital();
    Vector hospitais1 = tnh1.pesquisarPorMedico(Integer.parseInt(subuserid));
%>        
         <form action="./updateMedico.jsp" method="post">
            <table>
                <tr>
                   <td>Nome</td>
                   <td><input type="text" name="nome" value="<%= medico.getNome() %>" required /></td>
                </tr>
                <tr>
                   <td>CPF</td>
                   <td><input type="text" name="cpf" value="<%= medico.getCPF() %>" required />
                </tr>
                <tr>
                   <td>CRM</td>
                   <td><input type="text" name="crm" value="<%= medico.getCRM() %>" required />
                </tr>
                <tr>
                   <td>Data de Nascimento</td>
                   <td><input type="text" name="nascimento" value="<%= date %>" required />
                </tr>
                <tr>
                   <td>Telefone</td>
                   <td><input type="text" name="telefone" value="<%= medico.getTelefone() %>" required />
                </tr>
                <tr>
                   <td>Endere&ccedil;o</td>
                   <td><input type="text" name="endereco" value="<%= medico.getEndereco() %>" required />
                </tr>
                <tr>
                   <td>N&uacute;mero</td>
                   <td><input type="text" name="numero" value="<%= medico.getNumero() %>" required />
                </tr>
                <tr>
                   <td>Complemento</td>
                   <td><input type="text" name="complemento" value="<%= medico.getComplemento() %>" />
                </tr>
                <tr>
                   <td>CEP</td>
                   <td><input type="text" name="cep" value="<%= medico.getCEP() %>" required />
                </tr>
                <tr>
                   <td>Bairro</td>
                   <td><input type="text" name="bairro" value="<%= medico.getBairro() %>" required />
                </tr>
                <tr>
                   <td>Cidade</td>
                   <td><input type="text" name="cidade" value="<%= medico.getCidade() %>" required />
                </tr>
                <tr>
                   <td>Estado</td>
                   <td><input type="text" name="estado" value="<%= medico.getEstado() %>" required />
                </tr>
            </table>

<% 
    transacoes.PlanoDeSaude tnp = new transacoes.PlanoDeSaude();
    Vector planos = tnp.ListarTodos();
%>
    <br>
    <p>Plano de sa�de</p>

            <%
                for (int p = 1; p <= 4; p++ ){
            %>
                    <select name="planos <%=p%>">
                        <option value="-1"></option>
                    <%
                    for(int i = 0; i < planos.size(); i++) {
                        PlanoDeSaudeDO ps = (PlanoDeSaudeDO)planos.elementAt(i);
                        if (planos1.contains( ps )) {
                            System.out.println("sairei");
                            planos1.remove( ps );
                    %>
                        <option value="<%= ps.getId()%>" selected><%= ps.getNome() %></option>
                 <%     } else {
                 %>
                        <option value="<%= ps.getId()%>"><%= ps.getNome() %></option>
                 <%     }
                    } //for i
                %>
                    </select>
                <% } // for p
                %>
        
<% 
    transacoes.Especialidade tne = new transacoes.Especialidade();
    Vector especialidades = tne.ListarTodos();             
%>
    <p>Especialidade</p>
            <%
                for (int p = 1; p <= 4; p++ ){
            %>
                    <select name="especialidades <%=p%>">
                        <option value="-1"></option>
                    <%
                    for(int i = 0; i < especialidades.size(); i++) {
                        EspecialidadeDO esp = (EspecialidadeDO)especialidades.elementAt(i);
                    %>
                        <option value="<%= esp.getId()%>"><%= esp.getNome() %></option>
                 <% } //for i
                %>
                    </select>
                <% } // for p
                %>

<% 
    transacoes.Hospital tnh = new transacoes.Hospital();
    Vector hospitais = tnh.ListarTodos();             
%>
    <p>Hospital</p>
            <%
                for (int p = 1; p <= 4; p++ ){
            %>
                    <select name="hospitais <%=p%>">
                        <option value="-1"></option>
                    <%
                    for(int i = 0; i < hospitais.size(); i++) {
                        HospitalDO hosp = (HospitalDO)hospitais.elementAt(i);
                    %>
                        <option value="<%= hosp.getIdHospital()%>"><%= hosp.getNome() %></option>
                 <% } //for i
                %>
                    </select>
                <% } // for p
                %>    
                
            <input type="submit" name="atualizar" value="Atualizar" />
            <a href="updateMedico.jsp">Voltar</a>
            <input type="hidden" name="id" value=<%= id %> />
            <input type="hidden" name="action" value="updateValues" />
        </form>
<%         
    } // showEditForm
%>

<! ------------------------------------------------------------------->
<!--   atualizar valores -->
<%     
    if (action.equals("updateValues")) {
        String nome = request.getParameter("nome");
        String cpf = request.getParameter("cpf");
        String crm = request.getParameter("crm");
        String endereco = request.getParameter("endereco");
        String numero = request.getParameter("numero");
        String complemento = request.getParameter("complemento");
        String cep = request.getParameter("cep");
        String bairro = request.getParameter("bairro");
        String cidade = request.getParameter("cidade");
        String estado = request.getParameter("estado");
        String nascimento = request.getParameter("nascimento");
        String telefone = request.getParameter("telefone");
        int id = Integer.parseInt(request.getParameter("id"));

        DateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
        Date date = formatter.parse(nascimento);

        transacoes.Medico m = new transacoes.Medico();
        data.MedicoDO medico = new data.MedicoDO();
        medico.setIdMedico(id);
        medico.setNome(nome);
        medico.setCPF(cpf);
        medico.setCRM(crm);
        medico.setEndereco(endereco);
        medico.setNumero(Integer.parseInt(numero));
        medico.setComplemento(complemento);
        medico.setCEP(cep);
        medico.setBairro(bairro);
        medico.setCidade(cidade);
        medico.setEstado(estado);
        medico.setDataDeNascimento(date);
        medico.setTelefone(telefone); 

       transacoes.PlanoDeSaude tnp = new transacoes.PlanoDeSaude();
       transacoes.Especialidade tne = new transacoes.Especialidade();
       transacoes.Hospital tnh = new transacoes.Hospital();
        
       data.PlanoDeSaudeDO plano1 = new data.PlanoDeSaudeDO();
       data.PlanoDeSaudeDO plano2 = new data.PlanoDeSaudeDO();
       data.PlanoDeSaudeDO plano3 = new data.PlanoDeSaudeDO();
       data.PlanoDeSaudeDO plano4 = new data.PlanoDeSaudeDO();
       int idP = Integer.parseInt(request.getParameter("planos 1"));
       plano1 = tnp.buscar(idP);
       idP = Integer.parseInt(request.getParameter("planos 2"));
       plano2 = tnp.buscar(idP);
       idP = Integer.parseInt(request.getParameter("planos 3"));
       plano3 = tnp.buscar(idP);
       idP = Integer.parseInt(request.getParameter("planos 4"));
       plano4 = tnp.buscar(idP);
       data.EspecialidadeDO espec1 = new data.EspecialidadeDO();
       data.EspecialidadeDO espec2 = new data.EspecialidadeDO();
       data.EspecialidadeDO espec3 = new data.EspecialidadeDO();
       data.EspecialidadeDO espec4 = new data.EspecialidadeDO();
       int idE = Integer.parseInt(request.getParameter("especialidades 1"));
       espec1 = tne.buscar(idE);
       idE = Integer.parseInt(request.getParameter("especialidades 2"));
       espec2 = tne.buscar(idE);
       idE = Integer.parseInt(request.getParameter("especialidades 3"));
       espec3 = tne.buscar(idE);
       idE = Integer.parseInt(request.getParameter("especialidades 4"));
       espec4 = tne.buscar(idE);
       data.HospitalDO hospital1 = new data.HospitalDO();
       data.HospitalDO hospital2 = new data.HospitalDO();
       data.HospitalDO hospital3 = new data.HospitalDO();
       data.HospitalDO hospital4 = new data.HospitalDO();
       int idH = Integer.parseInt(request.getParameter("hospitais 1"));
       hospital1 = tnh.buscar(idH);
       idH = Integer.parseInt(request.getParameter("hospitais 2"));
       hospital2 = tnh.buscar(idH);
       idH = Integer.parseInt(request.getParameter("hospitais 3"));
       hospital3 = tnh.buscar(idH);
       idH = Integer.parseInt(request.getParameter("hospitais 4"));
       hospital4 = tnh.buscar(idH);
        
        boolean result = false;
        try {
           if (m.atualizar(medico)) {
            m.incluirE(medico, espec1);
            m.incluirE(medico, espec2);
            m.incluirE(medico, espec3);
            m.incluirE(medico, espec4);
            m.incluirH(medico, hospital1);
            m.incluirH(medico, hospital2);
            m.incluirH(medico, hospital3);
            m.incluirH(medico, hospital4);
            tnp.incluirM(medico, plano1);
            tnp.incluirM(medico, plano2);
            tnp.incluirM(medico, plano3);
            tnp.incluirM(medico, plano4);
            result = true;
           }
        } catch (Exception e) {
%>           <%= e.toString() %>
<%
        }
        if ( result ) {
        // avisar usuario que transacao foi feita com sucesso
%>
        Transa��o realizada com sucesso!
        <form action="./index.jsp" method="post">
           <input type="submit" name="voltar" value="Voltar" />
        </form>
<%      } else {
%>
        Erro ao atualizar dados do contato
        <form action="./updateMedico.jsp" method="post">
           <input type="submit" name="retry" value="Repetir" />
        </form>
<%        }
    } // updateValues
%>

</div> <!-- Content div -->

<%@ include file="footer.jsp" %>

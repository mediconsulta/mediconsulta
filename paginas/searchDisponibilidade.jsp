<%@ include file="header.jsp" %>
<%@ page import="java.util.Vector" %>
<%@ page import="transacoes.Medico" %>
<%@ page import="data.MedicoDO" %>
<%@ page import="data.ConsultaDO" %>
<%@ page import="transacoes.Consulta" %>
<%@ page import="java.text.SimpleDateFormat"%>
<%@ page import="java.text.DateFormat"%>
<%@ page import="java.util.Date"%>
<%@ page import="transacoes.Especialidade" %>
<%@ page import="data.EspecialidadeDO" %>

<div id="content">
    <h1>Pesquisar por disponibilidade hor�rios</h1>
    <hr>
    
<! ------------------------------------------------------------------->
<!--   sempre mostrar o formulario de busca, ateh acao ser "voltar" -->

<%     if ( null != request.getParameter("voltar")) {
%>        <jsp:forward page="./index.jsp" />
<%        return;
       }
%>

         <form action="./searchDisponibilidade.jsp" method="post">
<%
    // VERIFICACAO MANUAL DO LOGIN
    if ( session.getAttribute("user_name") == null) {
       pageContext.forward("index.jsp");
    }

    String nome1 = (String)session.getAttribute("user_name");
%>
           <table>
            <tr><td>Data e hor�rio desejados (dd/MM/yyyy HH:mm):</td><td><input name="dia" type="text"></td></tr>
            </table>
            <input type="submit" name="pesquisar" value="Pesquisar" />
            <input type="submit" name="voltar" value="Voltar" />
         </form>
               
               
               
<%   if ( null != request.getParameter("pesquisar")) {
       String idEspecialidade = request.getParameter("idEspecialidade");
       String dia = request.getParameter("dia");
       DateFormat formatter = new SimpleDateFormat("dd/MM/yyyy HH:mm");
       Date date = formatter.parse(dia);
       java.sql.Timestamp sqlTimestamp = new java.sql.Timestamp(date.getTime());
       transacoes.Medico tn = new transacoes.Medico();
       Vector medicos = tn.pesquisarDisponibilidade(sqlTimestamp);   
       if ( (medicos == null) || (medicos.size() == 0)) {
%>
              Nenhum m�dico com disponibilidade para esse hor�rio foi encontrado!
          <form action="./index.jsp" method="post">
             <input type="submit" name="voltar" value="Voltar" />
          </form>
<%     } else {
%>
            <table class="results-table">
             <tr>
                <th>Nome</th>
                <th>Endere�o</th>
                <th>N&uacute;mero</th>
                <th>Complemento</th>
                <th>CEP</th>
                <th>Bairro</th>
                <th>Cidade</th>
                <th>Estado</th>
                <th>Especialidades</th>
             </tr>
<%           for(int i = 0; i < medicos.size(); i++) {
                MedicoDO medico = (MedicoDO)medicos.elementAt(i);
                int idMedico = medico.getIdMedico();
                transacoes.Especialidade t = new transacoes.Especialidade();
                Vector especialidades = t.pesquisarPorMedico(idMedico);
%>              <tr>
                   <td><%= medico.getNome() %></td>
                   <td><%= medico.getEndereco() %></td>
                   <td><%= medico.getNumero() %></td>
                   <td><%= medico.getComplemento().equals("") ? "-" : medico.getComplemento() %></td>
                   <td><%= medico.getCEP() %></td>
                   <td><%= medico.getBairro()%></td>
                   <td><%= medico.getCidade() %></td>
                   <td><%= medico.getEstado() %></td>
                   <td>
                   <% for (int j=0;j<especialidades.size();j++) { 
                         EspecialidadeDO especialidade = (EspecialidadeDO)especialidades.elementAt(j);
                   %>
                        <%= especialidade.getNome() %><br>
                   <%
                   }
                   %>
                   </td>                                       
                </tr>        
<%           } // for i      
%>        </table>            
<%     } // medicos retornados
     } // pesquisar
%>



</div> <!-- Content div -->

<%@ include file="footer.jsp" %>


<%-- 
    Document   : Cadastro
    Created on : Oct 25, 2014, 5:57:44 PM
    Author     : Edson
--%>

<%@ include file="header.jsp" %>
<%@ page import="data.UsuarioDO" %>
<%@ page import="transacoes.Usuario" %>

<div id="content">
    <h1>Cadastro de Usu�rio</h1>
    <hr>
    
<! ------------------------------------------------------------------------->
<!--   se for o request inicial, mostrar somente o formulario de pesquisa -->

<%  
    if (userid != "#") {
%>
        <jsp:forward page="./index.jsp" />
<%
    }

    String action = request.getParameter("action");
    if ( null == action ) {
       action = "showSearchForm";
%>

    <form id="update-p-search-form" action="./cadastro.jsp" method="post">
<%
    // VERIFICACAO MANUAL DO LOGIN - Tirar o comment quando tiver login funcionando
    //if ( session.getAttribute("user_name") == null) {
    //   pageContext.forward("index.jsp");
    //}

    //String nome1 = (String)session.getAttribute("user_name");
%>
        <table>
          <tr>
             <td>Email</td>
             <td><input type="email" name="email" required />
          </tr>
          <tr>
             <td>Senha</td>
             <td><input type="password" name="pass" required />
          </tr>
          <tr>
             <td>Tipo de Cadastro:</td>
             <td><input type="radio" name="tipo" value="0">Administrador<br></td>
             <td><input type="radio" name="tipo" value="1">Paciente<br></td>
             <td><input type="radio" name="tipo" value="2">M�dico<br></td>
          </tr>
        </table>
        <input type="submit" name="pesquisar" value="Cadastrar" />
        <input type="hidden" name="action" value="register" />
    </form>

<%  } 
%>

<! ------------------------------------------------------------------------->
<!--   Cadastra o usuario e redireciona para pagina de cadastro                              -->
<%
    if (action.equals("register")) {
        String email = request.getParameter("email");
        String pass = request.getParameter("pass");
        String type = request.getParameter("tipo");
        
        Usuario tn = new Usuario();
        UsuarioDO user = new UsuarioDO();
        
        user.setEmail(email);
        user.setPassword(pass);
        user.setTipo( Integer.parseInt(type) );
        
        
        if (tn.pesquisarEmail(user.getEmail()).size()>0){
 %>         E-mail j� existente.
 <%         
        }else{
            boolean v = tn.incluir(user);
            
            if (v) {
    %>
            Usu&aacute;rio adicionado com sucesso!
    <%      
     
                int t = Integer.parseInt(type);
                
                if(t==0) { //Admin
                    action = null;
    %>
                <jsp:forward page="./cadastroAdministrador.jsp?email=<%=email%>" />
    <%
                } else if (t==1) { //Paciente
    %>
                <jsp:forward page="./cadastroPaciente.jsp?email=<%=email%>" />
    <%
                } else if (t==2) { //Medico
                    action = null;
    %>
                <jsp:forward page="./cadastroMedico.jsp?email=<%=email%>" />
    <%
                }
            } else {
    %>
            Ocorreu um erro com o cadastro.
    <%      }
        }
    } // pesquisar
%>
 
</div> <!-- Content div -->

<%@ include file="footer.jsp" %>
<%-- 
    Document   : sendPubEmail
    Created on : Nov 23, 2014, 6:16:43 PM
    Author     : brunotakashi
--%>

<%@page import="transacoes.Usuario"%>
<%@page import="java.util.Vector"%>
<%@page import="data.UsuarioData"%>
<%@page import="utils.Transacao"%>
<%@page import="utils.Email"%>
<%@ include file="header.jsp" %>

<div id="content">
    <h1>Enviar Email de Publicidade</h1>
    <hr>
    
    <%
    String action = request.getParameter("action");
    if ( null == action ) {
       action = "showSearchForm";
    }
    %>
    
    <%
    if (action.equals("showSearchForm")) {
    %>
    <form action="./sendPubEmail.jsp" method="post">
        
        <table>
            <tr>
                <div>Escolha destinatários<br>
                    <input type="checkbox" name="option1" value=true> Médicos<br>
                    <input type="checkbox" name="option2" value=true> Pacientes<br>
                    <br>
                </div>
            </tr>
          <tr>
             <td>Assunto</td>
             <td><input type="text" name="subject" /></td>
          </tr>
          <tr>
             <td>Texto</td>
             <td><input type="text" name="email_text" size=100/></td>
          </tr>
        </table>

        <input type="submit" name="enviar" value="Enviar" />
        <input type="hidden" name="action" value="enviar" />
    </form>
    <%
    }
    if (action.equals("enviar")) {
        String message = request.getParameter("subject");
        String text = request.getParameter("email_text");
        Email email = new Email();
        
        String medico = request.getParameter("option1");
        if (medico != null){
            Usuario user = new Usuario();
            Vector emailsMedicos = user.getEmails(2);
            for (int i = 0; i < emailsMedicos.size(); i++){
                if (email.sendMessage(emailsMedicos.get(i).toString(), message, text)){
                    System.out.println("Email sent to " + emailsMedicos.get(i).toString());
                }
                else {
                    System.out.println("Cannot send email to " + emailsMedicos.get(i).toString());
                }
            }
        }
        String paciente = request.getParameter("option2");
        if (paciente != null){
            Usuario user = new Usuario();
            Vector emailsPacientes = user.getEmails(1);
            for (int i = 0; i < emailsPacientes.size(); i++){
                if (email.sendMessage(emailsPacientes.get(i).toString(), message, text)){
                    System.out.println("Email sent to " + emailsPacientes.get(i).toString());
                }
                else {
                    System.out.println("Cannot send email to " + emailsPacientes.get(i).toString());
                }
            }
        }
        %>
        <div>E-mails enviados com sucesso</div>
        <form action="./sendPubEmail.jsp" method="post">
           <input type="submit" name="voltar" value="Voltar" />
           <input type="hidden" name="action" value="showSearchForm" />
        </form>
        <%
    }
    %>
    
</div> <!-- Content div -->



<%@ include file="footer.jsp" %>

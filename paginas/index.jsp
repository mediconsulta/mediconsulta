<%-- 
    Document   : index
    Created on : Oct 25, 2014, 12:54:01 PM
    Author     : Edson
--%>

<%@ include file="header.jsp" %>

<div id="content">
<%     
    String action = request.getParameter("action");
    if ( null == action) {
       action = "";
    }
%>

    <!-- Show homepage -->
<%
    if (action.equals("")) {
%>
        <img class="propaganda" src="img/mediconsulta.png" />
    
<%  } // End show homepage
%>
    <!-- Show Pesquisar -->
<%
    if (action.equals("search")) {
%>
        <h1>Pesquisar</h1>
        <hr>
        
        <p>Escolha o m&eacute;todo de busca desejado:</p>
        <br>
        <ul>
            <li><a href="searchNomeProfissional.jsp">Pesquisar por nome do profissional</a></li>
            <li><a href="searchHospital.jsp">Pesquisar m&eacute;dicos por hospitais</a></li>
            <li><a href="searchEnderecoProfissional.jsp">Pesquisar m&eacute;dicos por localidade</a></li>
            <li><a href="searchDisponibilidade.jsp">Pesquisar m&eacute;dicos por disponibilidade de hor&aacute;rios</a></li>
            <li><a href="searchPlanodesaude.jsp">Pesquisar m&eacute;dicos por plano de sa&uacute;de</a></li>
            <li><a href="searchEspecialidade.jsp">Pesquisar m&eacute;dicos por especialidade</a></li>
            <li><a href="searchLab.jsp">Buscar laborat&oacute;rio de exames</a></li>
            <li><a href="searchHospitais.jsp">Buscar hospitais ou centros de emerg&ecirc;ncia</a></li>
        </ul>
<%  } // End show pesquisa
%>    
    
    <!-- Show Painel de Controle do Paciente -->
<%
    if (action.equals("pcp")) {
%>
        <h1>Painel de Controle do Paciente</h1>
        <hr>
        
        <ul>
            <li><a href="updatePacient.jsp">Editar Perfil</a></li>
            <li><a href="alterarSenha.jsp">Alterar Senha</a></li>
        </ul>
<%  } // End show Pacient CP
%>     

    <!-- Show Painel de Controle do Medico -->
<%
    if (action.equals("dcp")) {
%>
        <h1>Painel de Controle do M&eacute;dico</h1>
        <hr>
        
        <ul>
            <li><a href="updateMedico.jsp">Editar Perfil</a></li>
            <li><a href="alterarSenha.jsp">Alterar Senha</a></li>
        </ul>
<%  } // End show Doctor CP
%>

    <!-- Show Painel de Controle do Administrador -->
<%
    if (action.equals("acp")) {
%>
        <h1>Painel de Controle do Administrador</h1>
        <hr>
        
        <ul>
            <li><a href="updateAdministrador.jsp">Editar Perfil</a></li>
            <li><a href="alterarSenha.jsp">Alterar Senha</a></li>
            <li><a href="removePaciente.jsp">Excluir Paciente</a></li>
            <li><a href="removeMedico.jsp">Excluir M&eacute;dico</a></li>
            <li><a href="insertLab.jsp">Cadastrar Laborat&oacute;rios</a></li>
            <li><a href="cadastroHospital.jsp">Cadastrar Hospitais</a></li>
            <li><a href="insertPlano.jsp">Inclus&atilde;o de Plano de Sa&uacute;de</a></li>
        </ul>
<%  } // End show Admin CP
%>

    <!-- Show Consultas (Paciente) -->
<%
    if (action.equals("pconsultas")) {
%>
        <h1>Consultas</h1>
        <hr>
        
        <ul>
            <li><a href="agendarConsulta.jsp">Agendar Nova Consulta</a></li>
            <li><a href="cancelarConsulta.jsp">Cancelar Consulta</a></li>
            <li><a href="confirmarConsulta.jsp">Confirmar Consulta</a></li>
            <li><a href="feedback.jsp">Deixe sua opini&atilde;o sobre uma consulta</a></li>
            <li><a href="notageral.jsp">Atribua uma nota &agrave; uma consulta</a></li>
            <li><a href="notapontualidade.jsp">Atribua uma nota &agrave; pontualidade do m&eacute;dico</a></li>
            <li><a href="precopago.jsp">Registre o pre&ccedil;o pago na consulta</a></li>
            <li><a href="editarConsulta.jsp">Editar Consulta</a></li>
            <li><a href="searchReceita.jsp">Consultar Receitas M&eacute;dicas</a></li>
            <li><a href="Lembrete.jsp">Lembrete</a></li>
        </ul>
<%  } // End show Consultas - paciente
%>    

    <!-- Show Consultas (Medico) -->
<%
    if (action.equals("dconsultas")) {
%>
        <h1>Consultas</h1>
        <hr>
        
        <ul>
            <li><a href="cancelarConsulta.jsp">Cancelar Consulta</a></li>
            <li><a href="insertReceita.jsp">Inserir Receitas</a></li>
        </ul>
<%  } // End show consultas - medico
%>

</div> <!-- Content div -->

<%@ include file="footer.jsp" %>

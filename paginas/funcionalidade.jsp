<%-- 
    Document   : funcionalidade
    Created on : 24/11/2014, 20:16:18
    Author     : babimello
--%>

<%@ include file="header.jsp" %>

<div id="content">
    <h1>Sobre o MediConsulta</h1>
    <hr>   

    N�s somos um site inovador de agendamento de consultas online. <br>
    <br>
    Agende e administre sua consulta a partir do que considera mais importante: <br>
    O profissional de sua confian�a;<br>
    A especialidade que voc� necessita;<br>
    O hospital de sua prefer�ncia;<br>
    A data que se encaixa na sua agenda;<br>
    O endere�o mais pr�ximo ao seu;<br>
    O plano de sa�de aceito.<br>
 <br>
    Al�m do conforto de administrar a sua agenda com um clique, voc� ainda conta com um feedback dos<br>
   usu�rios para saber sobre o desempenho do m�dico, pontualidade, pre�o da consulta e muito mais.<br>
   <br>
   Venha fazer parte da nossa equipe!<br>
   <br>
   <h1> MediConsulta, tornando a sua vida mais f�cil.</h1>
   
</div> <!-- Content div -->

<%@ include file="footer.jsp" %>

<%-- 
    Document   : remove Medico
    Created on : Nov 10, 2014
    Author     : Bruno Takashi
--%>

<%@page import="data.MedicoDO"%>
<%@page import="java.util.Vector"%>
<%@ include file="header.jsp" %>

<div id="content">
    <h1>Remover M�dico</h1>
    <hr>
    
<! ------------------------------------------------------------------------->
<!--   se for o request inicial, mostrar somente o formulario de pesquisa -->
<%     if ( null != request.getParameter("voltar")) {
%>        <jsp:forward page="./index.jsp" />
<%        return;
       }
%>

<%     
       String action = request.getParameter("action");
       if ( null == action ) {
          action = "showSearchForm";
%>

	  <form action="./removeMedico.jsp" method="post">
<%
       
    // VERIFICACAO MANUAL DO LOGIN
    if ( session.getAttribute("user_name") == null) {
       pageContext.forward("./index.jsp");
    }

    String nome1 = (String)session.getAttribute("user_name");
%>

            <table>
              <tr>
                 <td>Nome para pesquisar </td>
                 <td><input type="text" name="nome" />
              </tr>
            </table>
            <input type="submit" name="pesquisar" value="Pesquisar" />
	    <input type="hidden" name="action" value="showSearchResults" />
            <input type="submit" name="voltar" value="Voltar" />
          </form>

<%        
       } 

%>
<! ------------------------------------------------------------------------->
<!--   faz a pesquisa e mostra os resultados                              -->
<%
     if (action.equals("showSearchResults")) {
       String nome = request.getParameter("nome");
       transacoes.Medico tn = new transacoes.Medico();
       Vector medicos = tn.pesquisarN(nome);
       if ( (medicos == null) || (medicos.size() == 0)) {
         // avisar usuario que nao ha' contatos com este nome
%>
          Nenhum contato com este nome foi encontrado!
          <form action="./index.jsp" method="post">
             <input type="submit" name="voltar" value="Voltar" />
          </form>
<%     } else {
%>
          <table>
             <tr>
                <td>Nome</td>
                <td>CPF</td>
                <td>CRM</td>
                <td>Data de Nascimento</td>
                <td>Telefone</td>
             </tr>
<%           for(int i = 0; i < medicos.size(); i++) {
                MedicoDO medico = (MedicoDO)medicos.elementAt(i);
%>              <tr>
                   <td><%= medico.getNome() %></td>
                   <td><%= medico.getCPF()%></td>
                   <td><%= medico.getCRM()%></td>
                   <td><%= medico.getDataDeNascimento()%></td>
                   <td><%= medico.getTelefone() %></td>
		   <td><a href=removeMedico.jsp?action=remove&id=<%= medico.getIdMedico()%>>Remover</a>
                </tr>        
<%           } // for i      
%>
          </table>            
          <form action="./index.jsp" method="post">
             <input type="submit" name="voltar" value="Voltar" />
          </form>
<%     } // contatos retornados
       
     } // pesquisar

%>
<! ------------------------------------------------------------------->
<!--   mostra formulario para atualizacao                           -->
<%     if (action.equals("remove")) {
          int id = Integer.parseInt(request.getParameter("id"));
	  transacoes.Medico tn = new transacoes.Medico();
          data.MedicoDO contato = tn.buscar(id);
          boolean result = false;
          try {
            result = tn.remover(contato);
          } catch (Exception e) {
 %>           <%= e.toString() %>
 <%
          }      
       
       if ( result ) {
         // avisar usuario que transacao foi feita com sucesso
%>
          Transa��o realizada com sucesso!
          <form action="./index.jsp" method="post">
             <input type="submit" name="voltar" value="Voltar" />
          </form>
<%     } else {
%>
          Erro ao remover dados do medico
          <form action="./index.jsp" method="post">
             <input type="submit" name="retry" value="Repetir" />
          </form>
<%     }
     } // updateValues
%>
    
</div> <!-- Content div -->

<%@ include file="footer.jsp" %>

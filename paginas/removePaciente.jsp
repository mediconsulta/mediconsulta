<%@ include file="header.jsp" %>
<%@ page import="transacoes.Paciente" %>
<%@ page import="data.PacienteDO" %>
<%@ page import="transacoes.Usuario" %>
<%@ page import="data.UsuarioDO" %>
<%@ page import="java.util.Vector" %>

<div id="content">
    <h1>Remover Paciente</h1>
    <hr>


<! ------------------------------------------------------------------------->
<!--   se for o request inicial, mostrar somente o formulario de pesquisa -->
<%     if ( null != request.getParameter("voltar")) { 
%>       <jsp:forward page="./index.jsp?action=" />
<%        return;
       }
%>

<%     
       String action = request.getParameter("action");
       if ( null == action ) {
          action = "showSearchForm";
%>

	  <form action="./removePaciente.jsp" method="post">
<%
    // VERIFICACAO MANUAL DO LOGIN
    if ( session.getAttribute("user_name") == null) {
       pageContext.forward("./index.jsp");
    }

    String nome1 = (String)session.getAttribute("user_name");
%>

            <table>
              <tr>
                 <td>Nome para pesquisar </td>
                 <td><input type="text" name="nome" />
              </tr>
            </table>
            <input type="submit" name="pesquisar" value="Pesquisar" />
	    <input type="hidden" name="action" value="showSearchResults" />
            <input type="submit" name="voltar" value="Voltar" />
          </form>

<%        
       } 

%>
<! ------------------------------------------------------------------------->
<!--   faz a pesquisa e mostra os resultados                              -->
<%
     if (action.equals("showSearchResults")) {
       String nome = request.getParameter("nome");
       transacoes.Paciente tn = new transacoes.Paciente();
       Vector pacientes = tn.pesquisar(nome);
       if ( (pacientes == null) || (pacientes.size() == 0)) {
         // avisar usuario que nao ha' contatos com este nome
%>
          Nenhum contato com este nome foi encontrado!
          <form action="./index.jsp" method="post">
             <input type="submit" name="voltar" value="Voltar" />
          </form>
<%     } else {
%>
          <table>
             <tr>
                <td>Nome</td>
                <td>CPF</td>
                <td>Sexo</td>
                <td>Data de Nascimento</td>
                <td>Telefone</td>
             </tr>
<%           for(int i = 0; i < pacientes.size(); i++) {
                PacienteDO paciente = (PacienteDO)pacientes.elementAt(i);
%>              <tr>
                   <td><%= paciente.getNome() %></td>
                   <td><%= paciente.getCpf() %></td>
                   <td><%= paciente.getSexo() %></td>
                   <td><%= paciente.getNascimento() %></td>
                   <td><%= paciente.getTelefone() %></td>
		   <td><a href=removePaciente.jsp?action=remove&id=<%= paciente.getId()%>>Remover</a>
                </tr>        
<%           } // for i      
%>
          </table>            
          <form action="./index.jsp" method="post">
             <input type="submit" name="voltar" value="Voltar" />
          </form>
<%     } // contatos retornados
       
     } // pesquisar

%>
<! ------------------------------------------------------------------->
<!--   mostra formulario para atualizacao                           -->
<%     if (action.equals("remove")) {
          int id = Integer.parseInt(request.getParameter("id"));
	  transacoes.Paciente tn = new transacoes.Paciente();
          transacoes.Usuario tu = new transacoes.Usuario();
          data.PacienteDO contato = tn.buscar(id);
          data.UsuarioDO usuario = tu.buscarID(contato.getUserId());
          boolean result = false;
          try {
            result = (tn.remover(contato)&&tu.remover(usuario));
          } catch (Exception e) {
 %>           <%= e.toString() %>
 <%
          }      
       
       if ( result ) {
         // avisar usuario que transacao foi feita com sucesso
%>
          Transa��o realizada com sucesso!
          <form action="./index.jsp" method="post">
             <input type="submit" name="voltar" value="Voltar" />
          </form>
<%     } else {
%>
          Erro ao remover dados do paciente
          <form action="./removePaciente.jsp" method="post">
             <input type="submit" name="retry" value="Repetir" />
          </form>
<%     }
     } // updateValues
%>

</div> <!-- Content div -->

<%@ include file="footer.jsp" %>

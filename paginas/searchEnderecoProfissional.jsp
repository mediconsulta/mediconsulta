<%@ include file="header.jsp" %>
<%@ page import="java.util.Vector" %>
<%@ page import="transacoes.Medico" %>
<%@ page import="data.MedicoDO" %>
<%@ page import="transacoes.Especialidade" %>
<%@ page import="data.EspecialidadeDO" %>

<div id="content">
    <h1>Pesquisar por localidade</h1>
    <hr>

<! ------------------------------------------------------------------->
<!--   sempre mostrar o formulario de busca, ateh acao ser "voltar" -->

<%     if ( null != request.getParameter("voltar")) {
%>        <jsp:forward page="./index.jsp" />
<%        return;
       }
%>

         <form action="./searchEnderecoProfissional.jsp" method="post">
<%
    // VERIFICACAO MANUAL DO LOGIN
    if ( session.getAttribute("user_name") == null) {
       pageContext.forward("index.jsp");
    }

    String nome1 = (String)session.getAttribute("user_name");
%>
           <table>
             <tr>
               <td>Endereco de atendimento: </td>
               <td><input type="text" name="rua" />
               <td>CEP: </td>
               <td><input type="text" name="CEP" />
               <td>Cidade: </td>
               <td><input type="text" name="cidade" />
               <td>Estado: </td>
               <td><input type="text" name="estado" />
             </tr>
            </table>
           <input type="submit" name="pesquisar" value="Pesquisar" />
           <input type="submit" name="voltar" value="Voltar" />
         </form>

<! ------------------------------------------------------------------->
<!--   se nao for o request inicial, acionar a transacao de negocio -->


<%   if ( null != request.getParameter("pesquisar")) {  
       String rua = request.getParameter("rua");
       String CEP = request.getParameter("CEP");
       String Cidade = request.getParameter("cidade");
       String Estado = request.getParameter("estado");
       transacoes.Medico tn = new transacoes.Medico();
       if (rua.compareTo("")!=0){
        Vector medicos = tn.pesquisarR(rua);
        if ( (medicos == null) || (medicos.size() == 0)) {
         // avisar usuario que nao ha' contatos com este nome
%>
          Nenhum contato com este endereco foi encontrado!
          <form action="./index.jsp" method="post">
             <input type="submit" name="voltar" value="Voltar" />
          </form>
<%      } else {
%>
          <table class="results-table">
             <tr>
                <th>Nome</th>
                <th>Telefone</th>
                <th>Endereco</th>
                <th>Numero</th>
                <th>Bairro</th>
                <th>Cidade</th>
                <th>Especialidades</th>
             </tr>
<%           for(int i = 0; i < medicos.size(); i++) {
                MedicoDO medic = (MedicoDO)medicos.elementAt(i);
                int idMedico = medic.getIdMedico();
                transacoes.Especialidade t = new transacoes.Especialidade();
                Vector especialidades = t.pesquisarPorMedico(idMedico);
%>              <tr>
                   <td><%= medic.getNome() %></td>
                   <td><%= medic.getTelefone() %></td>
                   <td><%= medic.getEndereco() %></td>
                   <td><%= medic.getNumero() %></td>
                   <td><%= medic.getBairro() %></td>
                   <td><%= medic.getCidade() %></td>
                   <td>
                   <% for (int j=0;j<especialidades.size();j++) { 
                         EspecialidadeDO especialidade = (EspecialidadeDO)especialidades.elementAt(j);
                   %>
                        <%= especialidade.getNome() %><br>
                   <%
                   }
                   %>
                   </td>
                </tr>        
<%           } // for i      
%>        </table>            
<%      } // contatos retornados
       } // pesquisa por rua
       else if (CEP.compareTo("")!=0){
        Vector medicos = tn.pesquisarC(CEP);
        if ( (medicos == null) || (medicos.size() == 0)) {
         // avisar usuario que nao ha' contatos com este endereco
%>
          Nenhum contato com este endereco foi encontrado!
          <form action="./main.jsp" method="post">
             <input type="submit" name="voltar" value="Voltar" />
          </form>
<%      } else {
%>
          <table>
             <tr>
                <td>Nome</td>
                <td>Telefone</td>
                <td>Endereco</td>
                <td>Numero</td>
                <td>Bairro</td>
                <td>Cidade</td>
                <td>Especialidades</td>
             </tr>
<%           for(int i = 0; i < medicos.size(); i++) {
                MedicoDO medic = (MedicoDO)medicos.elementAt(i);
                int idMedico = medic.getIdMedico();
                transacoes.Especialidade t = new transacoes.Especialidade();
                Vector especialidades = t.pesquisarPorMedico(idMedico);
%>              <tr>
                   <td><%= medic.getNome() %></td>
                   <td><%= medic.getTelefone() %></td>
                   <td><%= medic.getEndereco() %></td>
                   <td><%= medic.getNumero() %></td>
                   <td><%= medic.getBairro() %></td>
                   <td><%= medic.getCidade() %></td>
                   <td>
                   <% for (int j=0;j<especialidades.size();j++) { 
                         EspecialidadeDO especialidade = (EspecialidadeDO)especialidades.elementAt(j);
                   %>
                        <%= especialidade.getNome() %><br>
                   <%
                   }
                   %>
                   </td>
                </tr>        
<%           } // for i      
%>        </table>            
<%      } // contatos retornados
       } // pesquisa por CEP
       else if (Cidade.compareTo("")!=0){
        Vector medicos = tn.pesquisarCi(Cidade);
        if ( (medicos == null) || (medicos.size() == 0)) {
         // avisar usuario que nao ha' contatos com este endereco
%>
          Nenhum contato com esta cidade foi encontrado!
          <form action="./main.jsp" method="post">
             <input type="submit" name="voltar" value="Voltar" />
          </form>
<%      } else {
%>
          <table>
             <tr>
                <td>Nome</td>
                <td>Telefone</td>
                <td>Endereco</td>
                <td>Numero</td>
                <td>Bairro</td>
                <td>Cidade</td>
                <td>Especialidades</td>
             </tr>
<%           for(int i = 0; i < medicos.size(); i++) {
                MedicoDO medic = (MedicoDO)medicos.elementAt(i);
                int idMedico = medic.getIdMedico();
                transacoes.Especialidade t = new transacoes.Especialidade();
                Vector especialidades = t.pesquisarPorMedico(idMedico);
%>              <tr>
                   <td><%= medic.getNome() %></td>
                   <td><%= medic.getTelefone() %></td>
                   <td><%= medic.getEndereco() %></td>
                   <td><%= medic.getNumero() %></td>
                   <td><%= medic.getBairro() %></td>
                   <td><%= medic.getCidade() %></td>
                   <td>
                   <% for (int j=0;j<especialidades.size();j++) { 
                         EspecialidadeDO especialidade = (EspecialidadeDO)especialidades.elementAt(j);
                   %>
                        <%= especialidade.getNome() %><br>
                   <%
                   }
                   %>
                   </td>
                </tr>        
<%           } // for i      
%>        </table>            
<%      } // contatos retornados
       } // pesquisa por Cidade
       else if (Estado.compareTo("")!=0){
        Vector medicos = tn.pesquisarE(Estado);
        if ( (medicos == null) || (medicos.size() == 0)) {
         // avisar usuario que nao ha' contatos com este endereco
%>
          Nenhum contato com este estado foi encontrado!
          <form action="./main.jsp" method="post">
             <input type="submit" name="voltar" value="Voltar" />
          </form>
<%      } else {
%>
          <table>
             <tr>
                <td>Nome</td>
                <td>Telefone</td>
                <td>Endereco</td>
                <td>Numero</td>
                <td>Bairro</td>
                <td>Cidade</td>
                <td>Especialidades</td>
             </tr>
<%           for(int i = 0; i < medicos.size(); i++) {
                MedicoDO medic = (MedicoDO)medicos.elementAt(i);
                int idMedico = medic.getIdMedico();
                transacoes.Especialidade t = new transacoes.Especialidade();
                Vector especialidades = t.pesquisarPorMedico(idMedico);
%>              <tr>
                   <td><%= medic.getNome() %></td>
                   <td><%= medic.getTelefone() %></td>
                   <td><%= medic.getEndereco() %></td>
                   <td><%= medic.getNumero() %></td>
                   <td><%= medic.getBairro() %></td>
                   <td><%= medic.getCidade() %></td>
                   <td>
                   <% for (int j=0;j<especialidades.size();j++) { 
                         EspecialidadeDO especialidade = (EspecialidadeDO)especialidades.elementAt(j);
                   %>
                        <%= especialidade.getNome() %><br>
                   <%
                   }
                   %>
                   </td>
                </tr>        
<%           } // for i      
%>        </table>            
<%      } // contatos retornados
       } // pesquisa por Estado
     } // pesquisar
%>
</div> <!-- Content div -->
<%@ include file="footer.jsp" %>
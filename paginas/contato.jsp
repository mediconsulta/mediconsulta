<%@ include file="header.jsp" %>
<%@ page import="utils.Email" %>
<%@ page import="transacoes.Administrador" %>
<%@ page import="data.AdministradorDO" %>
<%@ page import="transacoes.Paciente" %>
<%@ page import="data.PacienteDO" %>
<%@ page import="transacoes.Medico" %>
<%@ page import="data.MedicoDO" %>

<div id="content">
    
    <h1>Contato</h1>
    <hr>
    
<% 
    // se o usurio nao esta logado, mostra todos os campos
    if (session.getAttribute("user_type") == null) {
%>
    <form action="./contato.jsp" method="post">
        <table>
            <tr><td>Nome</td><td><input name="nome" type="text" /></td></tr>
            <tr><td>E-mail</td><td><input name="email" type="text" /></td></tr>
            <tr><td>Telefone</td><td><input name="telefone" type="text" /></td></tr>
            <tr><td>Assunto</td><td><input name="assunto" size="40" type="text" /></td></tr>
            <tr><td>Mensagem</td><td><textarea name="mensagem" cols="40" rows="10"></textarea></td></tr>
            <tr><td></td><td><input type="submit" name="enviar" value="Enviar" /></td>
       </table>
   </form>

<%  } else {
       // se o usuario esta logado, os campos nome, email e telefone sao
       // preenchidos automaticamente
        
       Integer userType = (Integer) session.getAttribute("user_type");
       Integer userID = (Integer) session.getAttribute("user_id");
       String nome = "", email = "", telefone = "";
       
       // email vem da variavel de sessao
       email = (String) session.getAttribute("user_name");
       
       if (userType == 0) { // admin
           transacoes.Administrador tn = new transacoes.Administrador();
           AdministradorDO admin = tn.buscarPorUserID(userID);
           if (admin != null) {
               nome = admin.getNome();
               telefone = admin.getTelefone();
           }           
       } else if (userType == 1) { // paciente
           transacoes.Paciente tn = new transacoes.Paciente();
           PacienteDO pac = tn.buscarPorUserID(userID);
           if (pac != null) {
               nome = pac.getNome();               
               telefone = pac.getTelefone();
           }
           
       } else if (userType == 2) { // medico
           transacoes.Medico tn = new transacoes.Medico();
           MedicoDO med = tn.buscarPorUserID(userID);
           if (med != null) {
               nome = med.getNome();
               telefone = med.getTelefone();
           }
       }
%> 

    <form action="./contato.jsp" method="post">
        <table>
            <tr><td>Nome</td><td><%= nome %><input type="hidden" name="nome" value="<%= nome %>" /></td></tr>
            <tr><td>E-mail</td><td><%= email %><input type="hidden" name="email" value="<%= email %>" /></td></tr>
            <tr><td>Telefone</td><td><%= telefone %><input type="hidden" name="telefone" value="<%= telefone %>" /></td></tr>
            <tr><td>Assunto</td><td><input name="assunto" size="40" type="text" /></td></tr>
            <tr><td>Mensagem</td><td><textarea name="mensagem" cols="40" rows="10"></textarea></td></tr>
            <tr><td></td><td><input type="submit" name="enviar" value="Enviar" /></td>
       </table>
   </form>

<% }
    if (request.getParameterValues("enviar") != null) {
        String nome = request.getParameter("nome");
        String email = request.getParameter("email");
        String telefone = request.getParameter("telefone");
        String assunto = request.getParameter("assunto");
        String mensagem = request.getParameter("mensagem");
        Email em = new Email();
       
        if (em.sendMessageFromVisitor(nome, email, telefone, assunto, mensagem)) {
%>

    <br /> Mensagem enviada com sucesso!
          
<%     } else {
%>
    <br /> Erro ao enviar mensagem.
<%     }
    }
%>

</div> <!-- Content div -->

<%@ include file="footer.jsp" %>
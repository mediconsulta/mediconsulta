<%-- 
    Document   : searchReceita
    Created on : 19/11
    Author     : Ivan
--%>


<%@ include file="header.jsp" %>
<%@ page import="java.text.SimpleDateFormat"%>
<%@ page import="java.text.DateFormat"%>
<%@ page import="java.util.Date"%>
<%@ page import="data.ConsultaDO" %>
<%@ page import="data.PacienteDO" %>
<%@ page import="data.MedicoDO" %>
<%@ page import="transacoes.Consulta" %>
<%@ page import="transacoes.Paciente" %>
<%@ page import="transacoes.Medico" %>
<%@ page import="java.util.Vector" %>
<%@ page import="java.util.*" %>

<div id="content">
    <h1>Consultar receita</h1>
    <hr>
    
<! ------------------------------------------------------------------------->
<!--   se for o request inicial, mostrar somente o formulario de escolha  -->

<%     
    String action = request.getParameter("action");
    if ( null == action ) {
       action = "showSearchForm";
%>

    <form action="./searchReceita.jsp" method="post">
<%
           
    // Puxar todas as consultas do usuario
    transacoes.Consulta tn = new transacoes.Consulta();
    //System.out.println(userid) ;
    Paciente pac = new Paciente();
    PacienteDO paci = pac.buscarPorUserID(Integer.parseInt(userid));
    int pacid = paci.getId();
    //System.out.println(medid) ;
    Vector consultas = tn.pesquisar4(pacid,1);
    System.out.println(consultas.size());
%>
        <table>
          <tr>
             <td>Selecione a consulta da qual deseja consultar receita</td>
             <td>
                 <select name="cid">
                     <%
                     int aux;
                     if (consultas.size()>10)  aux = 10;
                     else aux = consultas.size();
                     for (int i = aux; i >0; i--) {
                       
                        ConsultaDO consulta = (ConsultaDO)consultas.get(i-1);
                      
                        if (true) { // Medico
                           transacoes.Medico t = new transacoes.Medico();
                           MedicoDO medico = t.buscar(consulta.getMedicoId());
                           transacoes.Paciente p = new transacoes.Paciente();
                           PacienteDO paciente = p.buscar(consulta.getPacienteId());
                     %>
                            <option value="<%= consulta.getId()%>">Dr(a). <%= medico.getNome() %> - <%= consulta.getData() %></option>
                     <% }
                     }
                     %>

                 </select>
             </td>
          </tr>
        </table>
        <input type="submit" name="enviar" value="Consultar receita" />
        <input type="hidden" name="action" value="showSearchResults" />
    </form>

<%  } 
%>
    
<! ------------------------------------------------------------------------->
<!--   Ler a receita                                       -->
<%
    if (action.equals("showSearchResults")) {
        int cid = Integer.parseInt( request.getParameter("cid") );
        transacoes.Consulta tn = new transacoes.Consulta();
        ConsultaDO consulta = tn.buscar(cid);
       
%>
    <form action="./searchReceita.jsp" method="post">
   
        
 <table>
             <tr>Receita
             <br>
                 <td><%= consulta.getReceita() %></td>
             </tr>
    
        </table>            
       
    </form>
        <form action="./index.jsp" method="post">
           <input type="submit" name="voltar" value="Voltar" />
        </form>
<%  } 
%>
    
<! ------------------------------------------------------------------->
<!--   atualizar valores -->


</div> <!-- Content div -->

<%@ include file="footer.jsp" %>

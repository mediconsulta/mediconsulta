<%-- 
    Document   : updatePacient
    Created on : 21/10/2014, 09:02:14
    Author     : Edson Nakada
--%>

<%@ include file="header.jsp" %>
<%@ page import="java.text.SimpleDateFormat"%>
<%@ page import="java.text.DateFormat"%>
<%@ page import="java.util.Date"%>
<%@ page import="data.PacienteDO" %>
<%@ page import="transacoes.Paciente" %>
<%@ page import="java.util.Vector" %>

<div id="content">
    <h1>Atualizar Perfil de Paciente</h1>
    <hr>
    
<! ------------------------------------------------------------------------->
<!--   se for o request inicial, mostrar somente o formulario de pesquisa -->

<%     
    String action = request.getParameter("action");
    if ( null == action ) {
       action = "showSearchForm";
       
        // VERIFICACAO MANUAL DO LOGIN - Tirar o comment quando tiver login funcionando
        if ( (Integer)session.getAttribute("user_type")==1 ) {
            response.sendRedirect("updatePacient.jsp?action=showEditForm&id="+subuserid);
        }
        else if( (Integer)session.getAttribute("user_type")==2 ) {
            response.sendRedirect("index.jsp");
        }
%>

    <form id="update-p-search-form" action="./updatePacient.jsp" method="post">
        <table>
          <tr>
             <td>Nome para pesquisar</td>
             <td><input type="text" name="nome" />
          </tr>
        </table>
        <input type="submit" name="pesquisar" value="pesquisar" />
        <input type="hidden" name="action" value="showSearchResults" />
    </form>

<%  } 
%>

<! ------------------------------------------------------------------------->
<!--   faz a pesquisa e mostra os resultados                              -->
<%
    if (action.equals("showSearchResults")) {
        String nome = request.getParameter("nome");
        transacoes.Paciente tn = new transacoes.Paciente();
        Vector pacientes = tn.pesquisar(nome);
        if ( (pacientes == null) || (pacientes.size() == 0)) {
        // avisar usuario que nao ha' contatos com este nome
%>
        Nenhum contato com este nome foi encontrado!
        <form action="./updatePacient.jsp" method="post">
            <%= nome %>
            <input type="submit" name="voltar" value="Voltar" />
        </form>
<%      } else {
%>
        <table id="update-p-show-results">
            <tr>
               <td>Nome</td>
               <td>CPF</td>
               <td>Sexo</td>
               <td>Nascimento</td>
               <td>Telefone</td>
            </tr>
<%          for(int i = 0; i < pacientes.size(); i++) {
            PacienteDO paciente = (PacienteDO)pacientes.elementAt(i);
%>          <tr>
               <td><%= paciente.getNome() %></td>
               <td><%= paciente.getCpf() %></td>
               <td><%= paciente.getSexo() %></td>
               <td><%= paciente.getNascimento() %></td>
               <td><%= paciente.getTelefone() %></td>
               <td><a href="updatePacient.jsp?action=showEditForm&id=<%= paciente.getId()%>">Editar</a>
            </tr>        
<%          } // for i      
%>      </table>            
<%      } // contatos retornados

    } // pesquisar
%>

<! ------------------------------------------------------------------->
<!--   mostra formulario para atualizacao                           -->
<%  
    if (action.equals("showEditForm")) {
    int id = Integer.parseInt(request.getParameter("id"));
    System.out.println(id);
    transacoes.Paciente p = new transacoes.Paciente();
    data.PacienteDO paciente = p.buscar(id);

    DateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
    String date = formatter.format( paciente.getNascimento() );
%>        
         <form action="./updatePacient.jsp" method="post">
            <table>
                <tr>
                   <td>Nome</td>
                   <td><input type="text" name="nome" value="<%= paciente.getNome() %>" required /></td>
                </tr>
                <tr>
                   <td>CPF</td>
                   <td><input type="text" name="cpf" value="<%= paciente.getCpf() %>" required />
                </tr>
                <tr>
                   <td>Sexo</td>
                   <td><input type="text" name="sexo" value="<%= paciente.getSexo() %>" required />
                </tr>
                <tr>
                   <td>Data de Nascimento</td>
                   <td><input type="text" name="nascimento" value="<%= date %>" required />
                </tr>
                <tr>
                   <td>Telefone</td>
                   <td><input type="text" name="telefone" value="<%= paciente.getTelefone() %>" required />
                </tr>
            </table>

            <input type="submit" name="atualizar" value="Atualizar" />
            <a href="updatePacient.jsp">Voltar</a>
            <input type="hidden" name="id" value=<%= id %> />
            <input type="hidden" name="action" value="updateValues" />
        </form>
<%         
    } // showEditForm
%>

<! ------------------------------------------------------------------->
<!--   atualizar valores -->
<%     
    if (action.equals("updateValues")) {
        String nome = request.getParameter("nome");
        String cpf = request.getParameter("cpf");
        String sexo = request.getParameter("sexo");
        String nascimento = request.getParameter("nascimento");
        String telefone = request.getParameter("telefone");
        int id = Integer.parseInt(request.getParameter("id"));

        DateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
        Date date = formatter.parse(nascimento);

        transacoes.Paciente p = new transacoes.Paciente();
        data.PacienteDO paciente = new data.PacienteDO();
        paciente.setId(id);
        paciente.setNome(nome);
        paciente.setCpf(cpf);
        paciente.setSexo(sexo);
        paciente.setNascimento( date );
        paciente.setTelefone(telefone); 

        boolean result = false;
        try {
           result = p.atualizar(paciente);
        } catch (Exception e) {
%>           <%= e.toString() %>
<%
        }
        if ( result ) {
        // avisar usuario que transacao foi feita com sucesso
%>
        Transa��o realizada com sucesso!
        <form action="./index.jsp" method="post">
           <input type="submit" name="voltar" value="Voltar" />
        </form>
<%      } else {
%>
        Erro ao atualizar dados do contato
        <form action="./updatePacient.jsp" method="post">
           <input type="submit" name="retry" value="Repetir" />
        </form>
<%        }
    } // updateValues
%>

</div> <!-- Content div -->

<%@ include file="footer.jsp" %>

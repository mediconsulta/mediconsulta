<%-- 
    Document   : header
    Created on : Oct 25, 2014, 12:56:59 PM
    Author     : Edson
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>MediConsulta</title>
        
        <!-- FavIcon
            <meta content="/images/google_favicon_128.png" itemprop="image">
        -->
        <link rel="stylesheet" type="text/css" href="style.css">
        <script src="//code.jquery.com/jquery-1.11.0.min.js"></script>
        <script src="//code.jquery.com/jquery-migrate-1.2.1.min.js"></script>
        
    </head>
    <body>
        <!-- Debug section -->
        <%
        //session.setAttribute("user_name", "Edson");
        //session.setAttribute("user_id", 1);
        //session.setAttribute("user_type", 0);
        //session.removeAttribute("user_name");
        //session.removeAttribute("user_id");
        //session.removeAttribute("user_type");
        %>
        
        <!-- Handling user section -->
        <%
        String username = "Visitante";
        String userid = "#";
        String subuserid = "#";
        String link = "#";
        //String userType = "";
        if ( session.getAttribute("user_name") != null) {
           userid = ""+(Integer)session.getAttribute("user_id");
           username = (String)session.getAttribute("user_name2");
           subuserid = ""+(Integer)session.getAttribute("subuser_id");
           //userType = (String)session.getAttribute("user_type");
           if ((Integer)session.getAttribute("user_type")==0) {
                link = "showAdministrador.jsp?pid="+subuserid;
           }
           if ((Integer)session.getAttribute("user_type")==1) {
                link = "showPacient.jsp?pid="+subuserid;
           }
           else if ((Integer)session.getAttribute("user_type")==2) {
               // This is a doctor
               username = "Dr. " + username;
               link = "showMedico.jsp?pid="+subuserid;
           }
           else {
               
           }
        }
        %>
        
        
        <div id="wrapper">
            <div id="header">
                <!-- Banner Div -->
                <div id="banner">
                    <a href="index.jsp"><img src="img/banner.png" border="0"></a> 
                </div>
                
                <!-- Welcome Bar Div -->
                <div id="welcome-bar">
                    <p>Bem Vindo <a href="<%= link %>"><%= username %></a>.</p>
                    <div id="login-box">
                        <%
                        //System.out.println(userid);
                        if (userid == "#") {
                        %>
                        <a href="usuario.jsp">Login</a>
                        <a href="cadastro.jsp">Cadastrar</a>
                        <% } else {
                        %>
                        <a href="logout.jsp">Logout</a>
                        <% }
                        %>
                    </div>
                </div>
                <!-- Menu Bar -->
                <div id="nav-bar">
                    <ul id="menu">

                    <%
                    // If user type is Pacient
                    if ( session.getAttribute("user_type") != null &&
                         (Integer)session.getAttribute("user_type") == 1 ) {
                    %>
                        <li><a href="index.jsp?action=pcp">Painel de Controle</a>
                            <ul>
                                <li><a href="updatePacient.jsp">Editar Perfil</a></li>
                                <li><a href="alterarSenha.jsp">Alterar Senha</a></li>
                            </ul>
                        </li>
                        <li class="last-menu"><a href="index.jsp?action=pconsultas">Consultas</a>
                            <ul>
                                <li><a href="agendarConsulta.jsp">Agendar Nova Consulta</a></li>
                                <li><a href="cancelarConsulta.jsp">Cancelar Consulta</a></li>
                                <li><a href="confirmarConsulta.jsp">Confirmar Consulta</a></li>
                                <li><a href="avaliarConsulta.jsp">Avaliar Consulta</a></li>
                                <li><a href="editarConsulta.jsp">Editar Consulta</a></li>
                                <li><a href="searchReceita.jsp">Consultar Receitas Médicas</a></li>
                                <li><a href="Lembrete.jsp">Lembrete</a></li>
                            </ul>
                        </li>
                    <% }
                    
                    
                    // If user type is Doctor
                    else if (session.getAttribute("user_type") != null &&
                             (Integer)session.getAttribute("user_type") == 2) {
                    %>
                        <li><a href="index.jsp?action=dcp">Painel de Controle</a>
                            <ul>
                                <li><a href="updateMedico.jsp">Editar Perfil</a></li>
                                <li><a href="alterarSenha.jsp">Alterar Senha</a></li>
                            </ul>
                        </li>
                        <li class="last-menu"><a href="index.jsp?action=dconsultas">Consultas</a>
                            <ul>
                                <li><a href="cancelarConsulta.jsp">Cancelar Consulta</a></li>
                                <li><a href="insertReceita.jsp">Inserir Receitas Médicas</a></li>
                            </ul>
                        </li>
                    <% }

                    
                    // If user type is Administrator
                    else if (session.getAttribute("user_type") != null &&
                             (Integer)session.getAttribute("user_type") == 0) {
                    %>
                        <li class="last-menu"><a href="index.jsp?action=acp">Painel de Controle</a>
                            <ul>
                                <li><a href="updateAdministrador.jsp">Editar Perfil</a></li>
                                <li><a href="alterarSenha.jsp">Alterar Senha</a></li>
                                <li><a href="removePaciente.jsp">Excluir Paciente</a></li>
                                <li><a href="removeMedico.jsp">Excluir Médico</a></li>
                                <li><a href="insertLab.jsp">Cadastrar Laboratórios</a></li>
                                <li><a href="cadastroHospital.jsp">Cadastrar Hospitais</a></li>
                                <li><a href="insertPlano.jsp">Inclusão de Plano de Saúde</a></li>
                            </ul>
                        </li>
                    <% }
                       if (session.getAttribute("user_name")!=null) {
                    %>
                        <li class="right-menu"><a href="index.jsp?action=search">Pesquisar</a></li>
                    <% }
                    %>
                        <li class="right-menu"><a href="contato.jsp">Contato</a></li>
                        <li class="right-menu"><a href="mapadosite.jsp">Mapa do Site</a></li>  
                        <li class="right-menu"><a href="funcionalidade.jsp">Sobre Nós</a></li>
                    </ul>
                </div>
                
            </div> <!-- header div -->
            

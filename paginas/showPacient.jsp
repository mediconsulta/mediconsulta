<%-- 
    Document   : showPacient
    Created on : Oct 25, 2014, 5:57:44 PM
    Author     : Edson
--%>

<%@ include file="header.jsp" %>
<%@ page import="java.text.SimpleDateFormat"%>
<%@ page import="java.text.DateFormat"%>
<%@ page import="java.util.Date"%>
<%@ page import="data.ConsultaDO" %>
<%@ page import="data.PacienteDO" %>
<%@ page import="transacoes.Consulta" %>
<%@ page import="transacoes.Paciente" %>

<%
// Try checking if a parameter was posted and check if I have permission
int pid = 0;
if (request.getParameter("pid")!= null) {
    if((Integer)session.getAttribute("user_type")== 0) {
        pid = Integer.parseInt( request.getParameter("pid") );
    }
    if((Integer)session.getAttribute("user_type")== 1 && Integer.parseInt(request.getParameter("pid"))==Integer.parseInt(subuserid)) {
        pid = Integer.parseInt(subuserid);
    }
    if((Integer)session.getAttribute("user_type")== 2) {
        transacoes.Consulta tn = new transacoes.Consulta();
        if (tn.checkPM(Integer.parseInt(subuserid),Integer.parseInt(request.getParameter("pid"))).size() > 0 )
            pid = Integer.parseInt( request.getParameter("pid") );
    }
}
//System.out.println(pid);

%>

<div id="content">
    <%
    // Posso mostrar um resultado
    if (pid !=0) {
        transacoes.Paciente tn = new transacoes.Paciente();
        PacienteDO paciente = tn.buscar(pid);
        
        String sexo = "Masculino";
        if( paciente.getSexo().equals("F") )
            sexo = "Feminino";
        
        DateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
        String date = formatter.format( paciente.getNascimento() );
    %>
    <h1>Sr./Sra. <%= paciente.getNome() %></h1>
    <hr>
    
    <ul>
        <li>Nome Completo: <%= paciente.getNome() %></li>
        <li>CPF: <%= paciente.getCpf() %></li>
        <li>Sexo: <%= sexo %></li>
        <li>Data de Nascimento: <%= date %></li>
        <li>Telefone: <%= paciente.getTelefone() %></li>
    </ul>
    
    <% 
    } else {
    %>
    Voc&ecirc; n&atilde;o tem permiss&atilde;o para ver o perfil deste usu&aacute;rio.
    
    <% }
    %>
</div> <!-- Content div -->

<%@ include file="footer.jsp" %>
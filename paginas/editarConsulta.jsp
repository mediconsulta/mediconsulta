<%-- 
    Document   : editarConsulta
    Created on : 11/11/2014, 09:05:02
    Author     : Gustavo
--%>

<%@ include file="header.jsp" %>
<%@ page import="java.text.SimpleDateFormat"%>
<%@ page import="java.text.DateFormat"%>
<%@ page import="java.util.Date"%>
<%@ page import="data.ConsultaDO" %>
<%@ page import="data.PacienteDO" %>
<%@ page import="data.MedicoDO" %>
<%@ page import="transacoes.Consulta" %>
<%@ page import="transacoes.Paciente" %>
<%@ page import="transacoes.Medico" %>
<%@ page import="java.util.Vector" %>
<%@ page import="java.util.*" %>

<div id="content">
    <h1>Editar Consulta</h1>
    <hr>
    
<! ------------------------------------------------------------------------->
<!--   se for o request inicial, mostrar somente o formulario de escolha  -->

<%     
    String action = request.getParameter("action");
    if ( null == action ) {
       action = "showSearchForm";
%>

    <form action="./editarConsulta.jsp" method="post">
<%
           
    // Puxar todas as consultas do usuario
    transacoes.Consulta tn = new transacoes.Consulta();
    Vector consultas = tn.pesquisar( Integer.parseInt(userid), (Integer)session.getAttribute("user_type"));
%>
        <table>
          <tr>
             <td>Selecione a consulta que deseja editar</td>
             <td>
                 <select name="cid">
                     <%
                     for (int i = 0; i < consultas.size (); i++) {
                        ConsultaDO consulta = (ConsultaDO)consultas.get(i);
                        
                        
                        if ((Integer)session.getAttribute("user_type") == 1) { // Paciente
                            transacoes.Medico t = new transacoes.Medico();
                            MedicoDO medico = t.buscar(consulta.getMedicoId());
                     %>
                            <option value="<%= consulta.getId()%>">Dr(a). <%= medico.getNome() %> - <%= consulta.getData() %></option>
                     <% }
                        else if ((Integer)session.getAttribute("user_type") == 2) { // Medico
                            transacoes.Paciente t = new transacoes.Paciente();
                            PacienteDO paciente = t.buscar(consulta.getPacienteId());
                     %>
                            <option value="<%= consulta.getId()%>">Sr(a). <%= paciente.getNome() %> - <%= consulta.getData() %></option>
                     <% }
                     }
                     %>

                 </select>
             </td>
          </tr>
        </table>
        <input type="submit" name="enviar" value="Editar Consulta" />
        <input type="hidden" name="action" value="showSearchResults" />
    </form>

<%  } 
%>
    
<! ------------------------------------------------------------------------->
<!--   Confirmacao do cancelamento                                        -->
<%
    if (action.equals("showSearchResults")) {
        String cid = request.getParameter("cid");
        
        // Perguntar nova data e horário
%>
    <form action="./editarConsulta.jsp" method="post">

        <table>
            <tr>
               <td>Nova data (dd/MM/yyyy HH:mm):</td>
            </tr>
            <tr>
                <td><input type="text" name="novadata" maxlength="16" required></textarea></td>
            </tr>
        </table>
        <a href="index.jsp">Voltar</a>
        <input type="submit" name="enviar" value="Editar Consulta" />
        <input type="hidden" name="cid" value="<%= cid %>" />
        <input type="hidden" name="action" value="updateValues" />
        
    </form>
<%  } 
%>
    
<! ------------------------------------------------------------------->
<!--   atualizar valores -->
<%     
    if (action.equals("updateValues")) {
        String novadata = request.getParameter("novadata");
        
        SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy HH:mm");
	String dateInString = novadata;
 
	Date data = formatter.parse(dateInString);
        
        String novohorario = request.getParameter("novohorario");

        int cid = Integer.parseInt( request.getParameter("cid") );

        transacoes.Consulta tn = new transacoes.Consulta();
        ConsultaDO consulta = tn.buscar(cid);
        
        consulta.setData(data);
        
        
        boolean result = false;
        try {
           result = tn.atualizar(consulta);
        } catch (Exception e) {
%>           <%= e.toString() %>
<%
        }
        
        // Incluir funcao de envio de email de notificacao
        
        if ( result ) {
        // avisar usuario que transacao foi feita com sucesso
%>
        Transa&ccedil;&atilde;o realizada com sucesso!
        <form action="./index.jsp" method="post">
           <input type="submit" name="voltar" value="Voltar" />
        </form>
<%      } else {
%>
        Erro ao atualizar dados do contato
        <form action="./cancelarConsulta.jsp" method="post">
           <input type="submit" name="retry" value="Repetir" />
        </form>
<%        }
    } // updateValues
%>

</div> <!-- Content div -->

<%@ include file="footer.jsp" %>

<%@ include file="header.jsp" %>
<%@ page import="java.util.Vector" %>
<%@ page import="transacoes.Medico" %>
<%@ page import="data.MedicoDO" %>
<%@ page import="transacoes.Especialidade" %>
<%@ page import="data.EspecialidadeDO" %>

<div id="content">
    <h1>Pesquisar por nome do profissional</h1>
    <hr>
    
<! ------------------------------------------------------------------->
<!--   sempre mostrar o formulario de busca, ateh acao ser "voltar" -->

<%     if ( null != request.getParameter("voltar")) {
%>        <jsp:forward page="./index.jsp" />
<%        return;
       }
%>

         <form action="./searchNomeProfissional.jsp" method="post">
<%
    // VERIFICACAO MANUAL DO LOGIN
    if ( session.getAttribute("user_name") == null) {
       pageContext.forward("index.jsp");
    }

    String nome1 = (String)session.getAttribute("user_name");
%>
           <table>
             <tr>
               <td>Nome para pesquisar: </td>
               <td><input type="text" name="nome" />
             </tr>
           </table>
           <input type="submit" name="pesquisar" value="Pesquisar" />
           <input type="submit" name="voltar" value="Voltar" />
         </form>

<! ------------------------------------------------------------------->
<!--   se nao for o request inicial, acionar a transacao de negocio -->


<%   if ( null != request.getParameter("pesquisar")) {  
       String nome = request.getParameter("nome");
       transacoes.Medico tn = new transacoes.Medico();
       Vector medicos = tn.pesquisarN(nome);
       if ( (medicos == null) || (medicos.size() == 0)) {
         // avisar usuario que nao ha' contatos com este nome
%>
          Nenhum contato com este nome foi encontrado!
          <form action="./index.jsp" method="post">
             <input type="submit" name="voltar" value="Voltar" />
          </form>
<%     } else {
%>
          <table class="results-table">
             <tr>
                <th>Nome</th>
                <th>CRM</th>
                <th>Telefone</th>
                <th>Endere�o</th>
                <th>N�mero</th>
                <th>Complemento</th>
                <th>Bairro</th>
                <th>Cidade</th>
                <th>Estado</th>
                <th>Especialidades</th>
             </tr>
<%           for(int i = 0; i < medicos.size(); i++) {
                MedicoDO medico = (MedicoDO)medicos.elementAt(i);
                int idMedico = medico.getIdMedico();
                transacoes.Especialidade t = new transacoes.Especialidade();
                Vector especialidades = t.pesquisarPorMedico(idMedico);
%>              <tr>
                   <td><%= medico.getNome() %></td>
                   <td><%= medico.getCRM() %></td>
                   <td><%= medico.getTelefone() %></td>
                   <td><%= medico.getEndereco()%></td>
                   <td><%= medico.getNumero()%></td>
                   <td><%= medico.getComplemento()%></td>
                   <td><%= medico.getBairro()%></td>
                   <td><%= medico.getCidade()%></td>
                   <td><%= medico.getEstado()%></td>
                   <td>
                   <% for (int j=0;j<especialidades.size();j++) { 
                         EspecialidadeDO especialidade = (EspecialidadeDO)especialidades.elementAt(j);
                   %>
                        <%= especialidade.getNome() %><br>
                   <%
                   }
                   %>
                   </td>
                </tr>
<%           } // for i      
%>        </table>            
<%     } // contatos retornados
     } // pesquisar
%>
    <!-- Seu codigo aqui! -->
    
</div> <!-- Content div -->

<%@ include file="footer.jsp" %>
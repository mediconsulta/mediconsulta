<%@ include file="header.jsp" %>
<%@ page import="java.text.SimpleDateFormat"%>
<%@ page import="java.text.DateFormat"%>
<%@ page import="java.util.Date"%>
<%@ page import="data.ConsultaDO" %>
<%@ page import="data.PacienteDO" %>
<%@ page import="data.MedicoDO" %>
<%@ page import="transacoes.Consulta" %>
<%@ page import="transacoes.Paciente" %>
<%@ page import="transacoes.Medico" %>
<%@ page import="java.util.Vector" %>

<div id="content">
    <h1>D&ecirc; uma nota para a pontualidade do m&eacute;dico</h1>
    <hr>
    
<!--   se for o request inicial, mostrar somente o formulario de escolha  -->  
    <%     
    String action = request.getParameter("action");
    if ( null == action ) {
       action = "showSearchForm";
%>
    
    <form action="./notapontualidade.jsp" method="post">
<%
    // VERIFICACAO MANUAL DO LOGIN 
    if ( session.getAttribute("user_name") == null) {
       pageContext.forward("index.jsp");
    }

    String nome1 = (String)session.getAttribute("user_name");
    
    // Puxar todas as consultas do usuario
    transacoes.Consulta tn = new transacoes.Consulta();
    Vector consultas = tn.pesquisar4( Integer.parseInt(userid), (Integer)session.getAttribute("user_type"));
%>

 <table>
          <tr>
             <td>Selecione a consulta que vai receber a nota</td>
             <td>
                 <select name="cid">
                     <%
                     for (int i = 0; i < consultas.size (); i++) {
                        ConsultaDO consulta = (ConsultaDO)consultas.get(i);
                        
                        
                        if ((Integer)session.getAttribute("user_type") == 1) { // Paciente
                            transacoes.Medico t = new transacoes.Medico();
                            MedicoDO medico = t.buscar(consulta.getMedicoId());
                     %>
                            <option value="<%= consulta.getId()%>">Dr(a). <%= medico.getNome() %> - <%= consulta.getData() %></option>
                     <% }
                        else if ((Integer)session.getAttribute("user_type") == 2) { // Medico
                            transacoes.Paciente t = new transacoes.Paciente();
                            PacienteDO paciente = t.buscar(consulta.getPacienteId());
                     %>
                            <option value="<%= consulta.getId()%>">Sr(a). <%= paciente.getNome() %> - <%= consulta.getData() %></option>
                     <% }
                     }
                     %>

                 </select>
             </td>
          </tr>
        </table>
        <input type="submit" name="enviar" value="Avaliar Consulta" />
        <input type="hidden" name="action" value="showSearchResults" />
    </form>
<%  } 
%>

<%
    if (action.equals("showSearchResults")) {
        String cid = request.getParameter("cid");
        
        // Perguntar o motivo do cancelamento
%>
    <form action="./notapontualidade.jsp" method="post">

        <table>
            <tr>
               <td>Escolha uma nota para a pontualidade do m&eacute;dico </td>
            </tr>
            <tr>
             <td>Nota pela Pontualidade:</td>
             <td><input type="radio" name="tipo" value="0">0<br></td>
             <td><input type="radio" name="tipo" value="1">1<br></td>
             <td><input type="radio" name="tipo" value="2">2<br></td>
             <td><input type="radio" name="tipo" value="3">3<br></td>
             <td><input type="radio" name="tipo" value="4">4<br></td>
             <td><input type="radio" name="tipo" value="5">5<br></td>
             <td><input type="radio" name="tipo" value="6">6<br></td>
             <td><input type="radio" name="tipo" value="7">7<br></td>
             <td><input type="radio" name="tipo" value="8">8<br></td>
             <td><input type="radio" name="tipo" value="9">9<br></td>
             <td><input type="radio" name="tipo" value="10">10<br></td>
          </tr>
        </table>
        <a href="index.jsp">Voltar</a>
        <input type="submit" name="enviar" value="Enviar Avalia&ccedil;&atilde;o" />
        <input type="hidden" name="cid" value="<%= cid %>" />
        <input type="hidden" name="action" value="updateValues" />
        
    </form>
<%  } 
%>

<!--   atualizar valores -->

<%     
    if (action.equals("updateValues")) {
        String type = request.getParameter("tipo");
        int t = Integer.parseInt(type);
        int cid = Integer.parseInt( request.getParameter("cid") );

        transacoes.Consulta tn = new transacoes.Consulta();
        ConsultaDO consulta = tn.buscar(cid);
        
        consulta.setNotaPontualidade(t);
        
        boolean result = false;
        try {
           result = tn.atualizar(consulta);
        } catch (Exception e) {
%>           <%= e.toString() %>
<%
        }
        
        if ( result ) {
        // avisar usuario que transacao foi feita com sucesso
%>
        Nota enviada com sucesso! Obrigado pela sua opini&atilde;o!
        <form action="./index.jsp" method="post">
           <input type="submit" name="voltar" value="Voltar" />
        </form>
<%      } else {
%>
        Erro ao incluir sua nota!
        <form action="./notapontualidade.jsp" method="post">
           <input type="submit" name="retry" value="Repetir" />
        </form>
<%        }
    } // updateValues
%>

    
</div> <!-- Content div -->

<%@ include file="footer.jsp" %>
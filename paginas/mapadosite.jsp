<%-- 
    Document   : mapadosite
    Created on : 11/11/2014, 10:24:50
    Author     : Keiti

--%>
<%@ include file="header.jsp" %>
     
<div id="content">
    <h1>Mapa do Site</h1>
    <hr>
    <ul>

    <%
    // If user type is Pacient
    if ( session.getAttribute("user_type") != null &&
         (Integer)session.getAttribute("user_type") == 1 ) {
    %>
        <li><a href="index.jsp?action=pcp">Painel de Controle</a>
            <ul>
                <li><a href="updatePacient.jsp">Editar Perfil</a></li>
                <li><a href="alterarSenha.jsp">Alterar Senha</a></li>
            </ul>
        </li>
        <li class="last-menu"><a href="index.jsp?action=pconsultas">Consultas</a>
            <ul>
                <li><a href="agendarConsulta.jsp">Agendar Nova Consulta</a></li>
                <li><a href="cancelarConsulta.jsp">Cancelar Consulta</a></li>
                <li><a href="confirmarConsulta.jsp">Confirmar Consulta</a></li>
                <li><a href="feedback.jsp">Deixe sua opini&atilde;o sobre uma consulta</a></li>
                <li><a href="notageral.jsp">Atribua uma nota &agrave; uma consulta</a></li>
                <li><a href="notapontualidade.jsp">Atribua uma nota &agrave; pontualidade do m&eacute;dico</a></li>
                <li><a href="precopago.jsp">Registre o pre&ccedil;o pago na consulta</a></li>
                <li><a href="editarConsulta.jsp">Editar Consulta</a></li>
                <li><a href="searchReceita.jsp">Consultar Receitas M&eacute;dicas</a></li>
                <li><a href="Lembrete.jsp">Lembrete</a></li>
            </ul>
        </li>
        <li class="right-menu"><a href="index.jsp?action=search">Pesquisar</a></li>
    <% }


    // If user type is Doctor
    else if (session.getAttribute("user_type") != null &&
             (Integer)session.getAttribute("user_type") == 2) {
    %>
        <li><a href="index.jsp?action=dcp">Painel de Controle</a>
            <ul>
                <li><a href="updateMedico.jsp">Editar Perfil</a></li>
                <li><a href="alterarSenha.jsp">Alterar Senha</a></li>
            </ul>
        </li>
        <li class="last-menu"><a href="index.jsp?action=dconsultas">Consultas</a>
            <ul>
                <li><a href="cancelarConsulta.jsp">Cancelar Consulta</a></li>
                <li><a href="insertReceita.jsp">Inserir Receitas M&eacute;dicas</a></li>
            </ul>
        </li>
        <li class="right-menu"><a href="index.jsp?action=search">Pesquisar</a></li>
    <% }


    // If user type is Administrator
    else if (session.getAttribute("user_type") != null &&
             (Integer)session.getAttribute("user_type") == 0) {
    %>
        <li class="last-menu"><a href="index.jsp?action=acp">Painel de Controle</a>
            <ul>
                <li><a href="removePaciente.jsp">Excluir Paciente</a></li>
                <li><a href="removeMedico.jsp">Excluir M&eacute;dico</a></li>
                <li><a href="insertLab.jsp">Cadastrar Laborat&oacute;rios</a></li>
                <li><a href="cadastroHospital.jsp">Cadastrar Hospitais</a></li>
                <li><a href="insertPlano.jsp">Inclus&atilde;o de Plano de Sa&uacute;de</a></li>
            </ul>
        </li>
        <li class="right-menu"><a href="index.jsp?action=search">Pesquisar</a></li>
    <% }
    %>
        <li class="right-menu"><a href="funcionalidade.jsp">Sobre N�s</a></li>
        <li class="right-menu"><a href="contato.jsp">Contato</a></li>
        <li class="right-menu"><a href="mapadosite.jsp">Mapa do Site</a></li>  
    </ul>
</div>


<%@ include file="header.jsp" %>
<%@ page import="java.text.SimpleDateFormat"%>
<%@ page import="java.text.DateFormat"%>
<%@ page import="java.util.Date"%>
<%@ page import="data.ConsultaDO" %>
<%@ page import="data.PacienteDO" %>
<%@ page import="data.MedicoDO" %>
<%@ page import="transacoes.Consulta" %>
<%@ page import="transacoes.Paciente" %>
<%@ page import="transacoes.Medico" %>
<%@ page import="java.util.Vector" %>
<%@ page import="transacoes.Especialidade" %>
<%@ page import="data.EspecialidadeDO" %>

<div id="content">
    <h1>Agendar Consulta</h1>
    <hr>
    
<! ------------------------------------------------------------------------->
<!--   se for o request inicial, mostrar somente o formulario de escolha  -->

<%     
        if ( null != request.getParameter("voltar") ) {
%>          <jsp:forward page="./index.jsp?action=" />
<%        return;
       }
%>

<%     
    String action = request.getParameter("action");
    if ( null == action ) {
       action = "showSearchForm";
%>

    <form action="./agendarConsulta.jsp" method="post">
<%
    // VERIFICACAO MANUAL DO LOGIN 
    if ( session.getAttribute("user_name") == null) {
       pageContext.forward("index.jsp");
    }

    String nome1 = (String)session.getAttribute("user_name"); 
%>
        <table>
             <tr>
               <td>Nome do profissional: </td>
               <td><input type="text" name="nome" />
             </tr>
           </table>
           <input type="submit" name="pesquisar" value="Pesquisar" />
           <input type="hidden" name="action" value="showSearchResults" />
           <input type="submit" name="voltar" value="Voltar" />
         </form>
<%        
       } 

%>
<! ------------------------------------------------------------------------->
<!--   faz a pesquisa e mostra os resultados                              -->
<%   if (action.equals("showSearchResults")) {  
       String nome = request.getParameter("nome");
       transacoes.Medico tn = new transacoes.Medico();
       Vector medicos = tn.pesquisarN(nome);
       if ( (medicos == null) || (medicos.size() == 0)) {
         // avisar usuario que nao ha' contatos com este nome
%>
          Nenhum profissional com este nome foi encontrado!
          <form action="./index.jsp" method="post">
             <input type="submit" name="voltar" value="Voltar" />
          </form>
<%     } else {
%>
          <table>
             <tr>
                <td>Nome</td>
                <td>CRM</td>
                <td>Telefone</td>
                <td>Cidade</td>
                <td>Endere�o</td>
                <td>Especialidades</td>
             </tr>
             
<%           for(int i = 0; i < medicos.size(); i++) {
                MedicoDO medico = (MedicoDO)medicos.elementAt(i);
                int idMedico = medico.getIdMedico();
                transacoes.Especialidade t = new transacoes.Especialidade();
                Vector especialidades = t.pesquisarPorMedico(idMedico);
%>              <tr>
                   <td><%= medico.getNome() %></td>
                   <td><%= medico.getCRM() %></td>
                   <td><%= medico.getTelefone() %></td>
                   <td><%= medico.getCidade() %></td>
                   <td><%= medico.getEndereco() %></td>
                   <td>
                   <% for (int j=0;j<especialidades.size();j++) { 
                         EspecialidadeDO especialidade = (EspecialidadeDO)especialidades.elementAt(j);
                   %>
                        <%= especialidade.getNome() %><br>
                   <%
                   }
                   %>
                   </td>
                   <td><a href=agendarConsulta.jsp?action=agendar&id=<%= medico.getIdMedico()%>>Agendar</a>
                </tr>
                
<%           } // for i      
%>        </table>            
          <form action="./index.jsp" method="post">
             <input type="submit" name="voltar" value="Voltar" />
          </form> 
<%     } // contatos retornados
     } // pesquisar
%>
<! ------------------------------------------------------------------->
<!--   mostra formulario para atualizacao                           -->
<%
    if ( action.equals("agendar")) {
        int idM = Integer.parseInt(request.getParameter("id"));
%>
        <form action="./agendarConsulta.jsp" method="post">
            <table>
            <tr><td>Data da consulta (dd-MM-yyyy HH:mm)</td><td><input name="dia" type="text"></td></tr>
            </table>
            <input type="submit" name="confirmar" value="Confirmar" />
            <input type="hidden" name="action" value="Confirmar" />
            <input type="hidden" name="id" value="<%= idM%>" />
            <input type="hidden" name="dia" value="<%= request.getParameter("dia")%>" />
            <input type="submit" name="voltar" value="Voltar" />
        </form>
<%  } 
%>
<! ------------------------------------------------------------------->
<!--   atualizar valores -->
<% 
       if (action.equals("Confirmar")) {
           int idM = Integer.parseInt(request.getParameter("id"));
           String dia = request.getParameter("dia");
           DateFormat formatter = new SimpleDateFormat("dd-MM-yyyy HH:mm");
           String dateInString = dia;
           Date date = formatter.parse(dateInString);
       
           transacoes.Consulta tn = new transacoes.Consulta();
           Vector consultas = tn.pesquisar2(idM,2);
           int flag = 0;
           for (int i = 0; i<consultas.size(); i++){
               ConsultaDO consultaI = new ConsultaDO();
               consultaI = (ConsultaDO)consultas.elementAt(i);
               if (date.compareTo(consultaI.getData()) == 0){
                   flag = 1;
               }
           }
       // verificar se m�dico est� dispon�vel
           if (flag == 0){
               int idP = Integer.parseInt(userid);
               data.ConsultaDO consulta = new data.ConsultaDO();
               consulta.setPacienteId(idP);
               consulta.setMedicoId(idM);
               consulta.setData(date);
               consulta.setEstado(1);
               System.out.println("inicio inclusao1");
               boolean result = false;
               try {
                   result = tn.incluir(consulta);
               } catch (Exception e) {
%>              <%= e.toString() %>
<%             }
         
               if (result) {
                // avisar usuario que consulta foi marcada com sucesso
%>
            Consulta agendada com sucesso!
            <form action="./index.jsp" method="post">
                <input type="submit" name="voltar" value="Voltar" />
             </form>
          
<%             } else {
%>
                    Erro ao agendar consulta! 
                    <form action="./agendarConsulta.jsp" method="post">
                        <input type="submit" name="retry" value="Repetir" />
                    </form>
<%             }
            }
            else {
%>
                Profissional j� tem uma consulta agendada para este hor�rio!
                <form action="./agendarConsulta.jsp" method="post">
                 <input type="submit" name="retry" value="Repetir" />
                </form>
<%          }   
        }
    
%>
</div> <!-- Content div -->
<%@ include file="footer.jsp" %>

<%-- 
    Document   : footer
    Created on : Oct 25, 2014, 1:01:09 PM
    Author     : Edson
--%>
        </div> <!-- Wrapper Div -->
        
        <!-- JavaScript handlers -->
        <script type="text/javascript" src="js/menu.js"></script>
        
        <div id="footer">
            Escola Polit�cnica da Universidade de S�o Paulo<br>
            PMR2490 - Sistemas de Informa��o - Turma 01 - 2014<br><br>
            <table>
                <tr>
                    <td>Barbara Sarkis Solon de Mello</td>
                    <td class="division">7206259</td>
                    <td>Bruno Alan Miyamoto</td>
                    <td class="division">7626755</td>
                    <td>Bruno Takashi Nakagawa</td>
                    <td>7506516</td>
                </tr>
                <tr>
                    <td>Edson Rosa Nakada</td>
                    <td class="division">7246546</td>
                    <td>Fabricio Muche Schiavo</td>
                    <td class="division">7521539</td>
                    <td>Gustavo Ryu Shimada</td>
                    <td>7666812</td>
                </tr>
                <tr>
                    <td>Ivan Yukio Honda Tokutake</td>
                    <td class="division">5126980</td>
                    <td>Leonardo Fischi Sommer</td>
                    <td class="division">7626672</td>
                    <td>Leonardo Segura</td>
                    <td>7700719</td> 
                </tr>
                <tr>
                    <td>Lucas Gomes Aveiro</td>
                    <td class="division">7133683</td>
                    <td>Rafael Mikio Nakanishi</td>
                    <td class="division">7626564</td>
                    <td>Renato Keiti Umakoshi</td>
                    <td>7114305</td> 
                </tr>
            </table>
            <br>
        </div>
    </body>
</html>

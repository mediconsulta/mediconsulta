<%@ include file="header.jsp" %>
<%@ page import="java.util.Vector" %>
<%@ page import="transacoes.Especialidade" %>
<%@ page import="data.EspecialidadeDO" %>
<%@ page import="transacoes.Medico" %>
<%@ page import="data.MedicoDO" %>

<div id="content">
    <h1>Pesquisar por especialidade</h1>
    <hr>
    
<! ------------------------------------------------------------------->
<!--   sempre mostrar o formulario de busca, ateh acao ser "voltar" -->

<%     if ( null != request.getParameter("voltar")) {
%>        <jsp:forward page="./index.jsp" />
<%        return;
       }
%>

         <form action="./searchEspecialidade.jsp" method="post">
<%
    // VERIFICACAO MANUAL DO LOGIN
    if ( session.getAttribute("user_name") == null) {
       pageContext.forward("index.jsp");
    }

    String nome1 = (String)session.getAttribute("user_name");
%>
           <table>
             <tr>
               <td>Especialidade: </td>
               <td>
               <select name="idEspecialidade">
    <%
    
       transacoes.Especialidade tn = new transacoes.Especialidade();
       Vector especialidades = tn.ListarTodos();
       for(int i = 0; i < especialidades.size(); i++) {
           EspecialidadeDO especialidade = (EspecialidadeDO)especialidades.elementAt(i);
           String nomeEsp = especialidade.getNome();
           nomeEsp = nomeEsp.substring(0, 1).toUpperCase() + nomeEsp.substring(1).toLowerCase();
    %>
                   <option value="<%= especialidade.getId() %>"><%= nomeEsp %></option>
    <%
       }
    %>
               </select>                  
               </td>
             </tr>
           </table>
           <input type="submit" name="pesquisar" value="Pesquisar" />
         </form>
               
               
               
<%   if ( null != request.getParameter("pesquisar")) {
       String idEspecialidade = request.getParameter("idEspecialidade");
       
       transacoes.Medico tn_med = new transacoes.Medico();
       int idEsp = Integer.parseInt(idEspecialidade);
       Vector medicos = tn_med.pesquisarEspecialidade(idEsp);
       
       if ( (medicos == null) || (medicos.size() == 0)) {
%>
              Nenhum m�dico com essa especialidade foi encontrado!
          <form action="./index.jsp" method="post">
             <input type="submit" name="voltar" value="Voltar" />
          </form>
<%     } else {
%>
            <table class="results-table">
             <tr>
                <th>Nome</th>
                <th>Endere�o</th>
                <th>N&uacute;mero</th>
                <th>Complemento</th>
                <th>CEP</th>
                <th>Bairro</th>
                <th>Cidade</th>
                <th>Estado</th>
             </tr>
<%           for(int i = 0; i < medicos.size(); i++) {
                MedicoDO medico = (MedicoDO)medicos.elementAt(i);
%>              <tr>
                   <td><%= medico.getNome() %></td>
                   <td><%= medico.getEndereco() %></td>
                   <td><%= medico.getNumero() %></td>
                   <td><%= medico.getComplemento().equals("") ? "-" : medico.getComplemento() %></td>
                   <td><%= medico.getCEP() %></td>
                   <td><%= medico.getBairro()%></td>
                   <td><%= medico.getCidade() %></td>
                   <td><%= medico.getEstado() %></td>
                                                         
                </tr>        
<%           } // for i      
%>        </table>            
<%     } // medicos retornados
     } // pesquisar
%>



</div> <!-- Content div -->

<%@ include file="footer.jsp" %>

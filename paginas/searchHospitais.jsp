<%@ include file = "header.jsp" %>
<%@ page import = "transacoes.Hospital" %>
<%@ page import = "data.HospitalDO" %>
<%@ page import = "java.util.Vector" %>


<div id="content">
    <h1>Pesquisar Hospitais</h1>
    <hr>
    
<! ------------------------------------------------------------------->
<%
    // VERIFICACAO DO LOGIN
    if ( session.getAttribute("user_name") == null) {
       pageContext.forward("index.jsp");
    }

    String[] estados = {"AC", "AL", "AP", "AM", "BA", "CE", "DF", "ES", "GO", 
                        "MA", "MT", "MS", "MG", "PA", "PB", "PR", "PE", "PI", 
                        "RJ", "RN", "RS", "RO", "RR", "SC", "SP", "SE", "TO"};
%>
    Preencha pelo menos um dos campos abaixo:   
    <br />
    <form action="./searchHospitais.jsp" method="post">
    <table>
        <tr><td>Nome do hospital</td><td><input name="nome" type="text"></td></tr>
        <tr><td><b>Endere�o</b></td><td></td></tr>
        <tr><td>Rua</td><td><input name="endereco" type="text"></td></tr>
        <tr><td>CEP</td><td><input name="cep" type="text"></td></tr>
        <tr><td>Cidade</td><td><input name="cidade" type="text"></td></tr>
        <tr><td>Estado</td>
            <td>
                <select name="estado">
                    <option value="">Selecione</option>
                    <% for (int i = 0; i < estados.length; i++) { %>
                    <option value="<%=estados[i]%>"><%=estados[i]%></option>
                    <% } %>
                </select>
            </td></tr>
        <tr><td><b>Pronto socorro</b></td><td></td></tr>
        <tr><td>Escolha op&ccedil;&atilde;o</td>
            <td>
                <input type="radio" name="ps" value="all" checked>Todos<br />
                <input type="radio" name="ps" value="only">Com pronto-socorro
            </td>
        </tr>
        <tr><td></td><td><input type="submit" name="search" value="Pesquisar"></td>
   </table>
   </form>
   <br />
<! ------------------------------------------------------------------->
<!--   se nao for o request inicial, acionar a transacao de negocio -->


<%   if (request.getParameter("search") != null) {  
       String nome = request.getParameter("nome");
       String endereco = request.getParameter("endereco");
       String cep = request.getParameter("cep");
       String cidade = request.getParameter("cidade");
       String estado = request.getParameter("estado");
       String ps = request.getParameter("ps");
       
       transacoes.Hospital tn = new transacoes.Hospital();
       Vector hospitais = tn.pesquisar(nome, endereco, cep, cidade, estado, ps);
       
       if ( (hospitais == null) || (hospitais.size() == 0)) {
%>
          Nenhum hospital encontrado!
<%     } else {
%>
        <table class="results-table">
             <tr>
                <th>Nome</th>
                <th>Endere�o</th>
                <th>N�mero</th>
                <th>Complemento</th>
                <th>CEP</th>
                <th>Bairro</th>
                <th>Cidade</th>
                <th>Estado</th>
                <th>Possui pronto-socorro</th>
             </tr>
<%           for(int i = 0; i < hospitais.size(); i++) {
                HospitalDO hospital = (HospitalDO) hospitais.elementAt(i);
%>              <tr>
                   <td><%= hospital.getNome() %></td>
                   <td><%= hospital.getEndereco() %></td>
                   <td><%= hospital.getNumero() %></td>
                   <td><%= hospital.getComplemento().equals("") ? "-" : hospital.getComplemento() %></td>
                   <td><%= hospital.getCep() %></td>
                   <td><%= hospital.getBairro()%></td>
                   <td><%= hospital.getCidade() %></td>
                   <td><%= hospital.getEstado() %></td>
                   <td><%= hospital.getProntoSocorro().equals("s") ? "Sim" : "N&atilde;o" %></td>                                      
                </tr>
<%           } // for i      
%>        </table>     
<%     }
}
%>

    
</div> <!-- Content div -->

<%@ include file="footer.jsp" %>
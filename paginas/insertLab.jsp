<%-- 
    Document   : insertLab
    Created on : 02/11
    Author     : Ivan
--%>
<html>
<header>
  <title>Cadastrar laboratório de exames </title>
</header>

<body bgcolor="white">
<%@ include file="header.jsp" %>
<%@ page import="java.util.Vector" %>
<%@ page import="transacoes.Laboratorio" %>
<%@ page import="data.LaboratorioDO" %>

<div id="content">
    <h1>Cadastrar Laborat�rio</h1>
    <hr>

<! ------------------------------------------------------------>
<!--   se for o request inicial, mostrar somente o formulario -->

<%     if ( null == request.getParameterValues("incluir") ) {
%>
       <form action="./insertLab.jsp" method="post">
<%
    // VERIFICACAO MANUAL DO LOGIN
    if ( session.getAttribute("user_name") == null) {
       pageContext.forward("index.jsp");
    }

    String nome = (String)session.getAttribute("user_name");
%>

           <table>
            <tr>
               <td>Nome do Laboratorio</td>
               <td><input type="text" name="nome" />
            </tr>
            <p>Tipo de exame realizado</p>
            <form action="">
            <select name="exames">
                <option value="0"> </option>
                <option value="1">Fluidos</option>
                <option value="2">Ortopedicos</option>
                <option value="3">Respiratorios</option>
            </select>
                </table>
                <table>
            <p>Endere�o</p>
               <td>Rua: </td>
               <td><input type="text" name="rua" />
               <td>N�mero </td>
               <td><input type="int" name="numero" /> 
               <td>Complemento </td>
               <td><input type="text" name="complemento" />
               <td>CEP: </td>
               <td><input type="text" name="CEP" />
                </table>
                   <table>
               <td>Bairro: </td>
               <td><input type="text" name="bairro" />
               <td>Cidade: </td>
               <td><input type="text" name="cidade" />
               <td>Estado: </td>
               <td><input type="text" name="estado" />
               <td>Pa�s </td>
               <td><input type="text" name="pais" />
                </table>
            <table>
             <p>Contato</p>
               <td>Telefone</td>
               <td><input type="text" name="telefone" />
               <td>E-Mail</td>
               <td><input type="text" name="email" />
               <td>Website</td>
               <td><input type="text" name="website" />
      
          </table>
          <input type="submit" name="Incluir" value="Incluir" />
         </form>
         <form action="./index.jsp" method="post">
             <input type="submit" name="voltar" value="Voltar" />
          </form>
<%      } else { 
%>
<! ------------------------------------------------------------------->
<!--   se nao for o request inicial, acionar a transacao de negocio -->


<%     String nome = request.getParameter("nome");
       String rua = request.getParameter("rua");
       String numero = request.getParameter("numero");
       String complemento = request.getParameter("complemento");
       String bairro = request.getParameter("bairro");
       String telefone = request.getParameter("telefone");
       String email = request.getParameter("email");
       String website = request.getParameter("website");
       String cep = request.getParameter("CEP");
       String cidade = request.getParameter("cidade");
       String estado = request.getParameter("estado");
       String pais = request.getParameter("pais");
       String tipo = request.getParameter("exames");
       transacoes.Laboratorio tn = new transacoes.Laboratorio();
       data.LaboratorioDO laboratorio = new data.LaboratorioDO();
       laboratorio.setNome(nome);
       laboratorio.setTelefone(telefone); 
       laboratorio.setTipo_de_exame(Integer.parseInt(tipo)); 
       laboratorio.setCep(cep); 
       laboratorio.setBairro(bairro); 
       laboratorio.setCidade(cidade); 
       laboratorio.setEstado(estado); 
       laboratorio.setEndereco(rua);
       laboratorio.setNumero(Integer.parseInt(numero));
       laboratorio.setComplemento(complemento);
       laboratorio.setTelefone(telefone);
       laboratorio.setEmail(email);
       laboratorio.setWebsite(website);
       if ( tn.incluir(laboratorio)) {
         // avisar usuario que transacao foi feita com sucesso
%>
          Transa��o realizada com sucesso!
          <form action="./index.jsp" method="post">
             <input type="submit" name="voltar" value="Voltar" />
          </form>
<%     } else {
%>
          Erro ao incluir usu�rio            
          <form action="./insertLab.jsp" method="post">
             <input type="submit" name="retry" value="Repetir" />
          </form>
<%     }
       }
%>

</div> <!-- Content div -->

<%@ include file="footer.jsp" %>
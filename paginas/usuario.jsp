<%-- 
    Document   : MODELO
    Created on : Nov 11, 2014, 08:13 am
    Author     : Barbara
--%>
<%@ include file="header.jsp" %>
<%@ page import="data.UsuarioDO" %>
<%@ page import="transacoes.Usuario" %>
<%@ page import="java.util.Vector" %>
<%@ page import="data.AdministradorDO"%>
<%@ page import="transacoes.Administrador"%>
<%@ page import="data.PacienteDO"%>
<%@ page import="transacoes.Paciente"%>
<%@ page import="data.MedicoDO"%>
<%@ page import="transacoes.Medico"%>

<div id="content">
    <h1>Login</h1>
    <hr>
    
<%
    if( username != "Visitante" ) {
%>
    <jsp:forward page="./index.jsp" />
<%    
    }

    if ( request.getParameter("campo_controle") != null ) {
        // processa login
        String email = request.getParameter("username");
        String passwd = request.getParameter("pass");
        Usuario usuario = new Usuario();
        boolean v =  usuario.checkLogin(email, passwd);
        
        if (v) {
           UsuarioDO user = usuario.pesquisar(email, passwd);
           session.setAttribute("user_name", user.getEmail());
           session.setAttribute("user_id", user.getIduser() );
           session.setAttribute("user_type", user.getTipo());
     
           if ((Integer)session.getAttribute("user_type")==0) {
               Administrador tn = new Administrador();
               AdministradorDO admin = new AdministradorDO();
               
               admin = tn.buscarPorUserID( user.getIduser() );
               session.setAttribute( "subuser_id", admin.getId() );
               session.setAttribute("user_name2", admin.getNome());
           }
           else if ((Integer)session.getAttribute("user_type")==1) {
               Paciente tn = new Paciente();
               PacienteDO paciente = new PacienteDO();
               
               paciente = tn.buscarPorUserID( user.getIduser() );
               session.setAttribute( "subuser_id", paciente.getId() );
               session.setAttribute("user_name2", paciente.getNome());
           }
           else if ((Integer)session.getAttribute("user_type")==2) {
               Medico tn = new Medico();
               MedicoDO medico = new MedicoDO();
               
               medico = tn.buscarPorUserID( user.getIduser() );
               session.setAttribute( "subuser_id", medico.getIdMedico() );
               session.setAttribute("user_name2", medico.getNome());
           }
   %>
           <jsp:forward page="./index.jsp" />
           Login efetuado!
   <%
        } else {
   %>
           Usuario ou Senha invalidos!
           <a href="recuperarSenha.jsp">
               Gerar nova senha?</a>
   <%
        }
    }
    // show login form
%>
    <form method="post" action=usuario.jsp>
       Email <input type="email" name="username" required />
       Senha <input type="password" name="pass" required />
       <input type="submit" name="enviar" value="Enviar" />
       <input type="hidden" name="campo_controle" />
    </form>
    </hr>
</div> <!-- Content div -->

<%@ include file="footer.jsp" %>

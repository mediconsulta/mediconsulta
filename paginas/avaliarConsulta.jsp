<%-- 
    Document   : avaliarConsulta
    Created on : Oct 25, 2014, 5:57:44 PM
    Author     : Edson
--%>

<%@ include file="header.jsp" %>

<div id="content">
    <h1>Avalia��o de Consulta</h1>
    <hr>

    <ul>
        <li><a href="feedback.jsp">Deixe sua opini&atilde;o sobre uma consulta</a></li>
        <li><a href="notageral.jsp">Atribua uma nota &agrave; uma consulta</a></li>
        <li><a href="notapontualidade.jsp">Atribua uma nota &agrave; pontualidade do m&eacute;dico</a></li>
        <li><a href="precopago.jsp">Registre o pre&ccedil;o pago na consulta</a></li>
    </ul>
    
</div> <!-- Content div -->

<%@ include file="footer.jsp" %>
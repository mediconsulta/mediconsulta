package data;

import java.util.*;

public class AdministradorDO {
  private int _id, _userId, _numero;
  private String _cpf, _telefone;
  private String _nome, _endereco, _complemento;
  private String _bairro, _cidade, _estado;
  private Date _nascimento;
 

  public int getId() {
     return _id;
  } // getId

  public void setId(int id) {
    _id = id;
  } // setId
  
   public int getUserId() {
     return _userId;
  } // getUserId

  public void setUserId(int userId) {
    _userId = userId;
  } // setUserId

  public String getNome() {
    return _nome;
  } // getNome

  public void setNome(String nome) {
    _nome = nome;
  } // setNome

   public String getCpf() {
     return _cpf;
  } // getCpf

  public void setCpf(String cpf) {
    _cpf = cpf;
  } // setCpf
  
  public String getEndereco() {
    return _endereco;
  } // getEndereco

  public void setEndereco(String endereco) {
    _endereco = endereco;
  } // setEndereco
  
  public int getNumero() {
     return _numero;
  } // getNumero

  public void setNumero(int numero) {
    _numero = numero;
  } // setNumero

  public String getComplemento() {
    return _complemento;
  } // getComplemento

  public void setComplemento(String complemento) {
    _complemento = complemento;
  } // setComplemento  
  
  public String getBairro() {
    return _bairro;
  } // getBairro

  public void setBairro(String bairro) {
    _bairro = bairro;
  } // setBairro  
  
  public String getCidade() {
    return _cidade;
  } // getCidade

  public void setCidade(String cidade) {
    _cidade = cidade;
  } // setCidade  
  
  public String getEstado() {
    return _estado;
  } // getEstado

  public void setEstado(String estado) {
    _estado = estado;
  } // setEstado  
  
  public Date getNascimento() {
      return _nascimento;
  } // getNascimento
  
  public void setNascimento(Date nascimento) {
      _nascimento = nascimento;
  }
  
  public String getTelefone() {
    return _telefone;
  } // getTelefone

  public void setTelefone(String tel) {
    _telefone = tel;
  } // setTelefone


} // AdministradorDO
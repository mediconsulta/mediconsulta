package data;

public class EspecialidadeDO {
  private int _id;
  private String _nome;
 

  public int getId() {
     return _id;
  } // getId

  public void setId(int id) {
    _id = id;
  } // setId
  
  public String getNome() {
    return _nome;
  } // getNome

  public void setNome(String nome) {
    _nome = nome;
  } // setNome
} // EspecialidadeDO
package data;

import java.sql.*;
import java.util.*;
import utils.Transacao;

public class MedicoData {

  public void incluir(MedicoDO medic, Transacao tr) throws Exception {
     Connection con = tr.obterConexao();
     java.sql.Date sqlDate = new java.sql.Date(medic.getDataDeNascimento().getTime());
     
     String sql = "insert into mediconsulta.Medico (Usuario_iduser, Nome, CPF, CRM, Endereco, Numero, CEP, Complemento, Bairro, Cidade, Estado, DataDeNascimento, Telefone) values (?,?,?,?,?,?,?,?,?,?,?,?,?)";
     PreparedStatement ps = con.prepareStatement(sql);
     ps.setInt(1, medic.getUsuarioIdUser());
     ps.setString(2, medic.getNome());
     ps.setString(3, medic.getCPF());
     ps.setString(4, medic.getCRM());
     ps.setString(5, medic.getEndereco());
     ps.setInt(6, medic.getNumero());
     ps.setString(7, medic.getCEP());
     ps.setString(8, medic.getComplemento());
     ps.setString(9, medic.getBairro());
     ps.setString(10, medic.getCidade());
     ps.setString(11, medic.getEstado());
     ps.setDate(12, sqlDate);
     ps.setString(13, medic.getTelefone());
     
     int result = ps.executeUpdate();
  }

  public void excluir(MedicoDO medic, Transacao tr) throws Exception {
     excluir(medic.getIdMedico(), tr);
  } // excluir

  public void excluir (int idobj, Transacao tr) throws Exception {
     Connection con = tr.obterConexao();
      
     String sql = "delete from mediconsulta.Medico where idMedico=?";
     PreparedStatement ps = con.prepareStatement(sql);
     ps.setInt(1, idobj);
     
     int result = ps.executeUpdate();
  } // excluir 

  public void atualizar(MedicoDO medic, Transacao tr) throws Exception {
     Connection con = tr.obterConexao();
     java.sql.Date sqlDate = new java.sql.Date(medic.getDataDeNascimento().getTime());
     
     String sql = "update mediconsulta.Medico set Nome=?, CPF=?, CRM=?, Endereco=?, Numero=?, CEP=?, Complemento=?, Bairro=?, Cidade=?, Estado=?, DataDeNascimento=?, Telefone=? where idMedico=?";
     PreparedStatement ps = con.prepareStatement(sql);
     ps.setString(1, medic.getNome());
     ps.setString(2, medic.getCPF());
     ps.setString(3, medic.getCRM());
     ps.setString(4, medic.getEndereco());
     ps.setInt(5, medic.getNumero());
     ps.setString(6, medic.getCEP());
     ps.setString(7, medic.getComplemento());
     ps.setString(8, medic.getBairro());
     ps.setString(9, medic.getCidade());
     ps.setString(10, medic.getEstado());
     ps.setDate(11, sqlDate);
     ps.setString(12, medic.getTelefone());
     ps.setInt(13, medic.getIdMedico());
     
     int result = ps.executeUpdate();
  } // atualizar

  public MedicoDO buscar(int idobj, Transacao tr) throws Exception {
     Connection con = tr.obterConexao();
     
     String sql = "select * from mediconsulta.Medico where  idMedico=?";
     PreparedStatement ps = con.prepareStatement(sql);
     ps.setInt(1, idobj);
     
     ResultSet rs = ps.executeQuery();
     rs.next();
     MedicoDO medic = new MedicoDO();
     medic.setIdMedico (rs.getInt("idMedico"));
     medic.setUsuarioIdUser2 (rs.getInt("Usuario_iduser"));
     medic.setNome (rs.getString("Nome"));
     medic.setCPF (rs.getString("CPF"));
     medic.setCRM (rs.getString("CRM"));
     medic.setEndereco (rs.getString("Endereco"));
     medic.setNumero (rs.getInt("Numero"));
     medic.setCEP (rs.getString("CEP"));
     medic.setComplemento (rs.getString("Complemento"));
     medic.setBairro (rs.getString("Bairro"));
     medic.setCidade (rs.getString("Cidade"));
     medic.setEstado (rs.getString("Estado"));
     medic.setDataDeNascimento (rs.getDate("DataDeNascimento"));
     medic.setTelefone(rs.getString("Telefone"));
     
     //System.out.println(medic.getNome());
     return medic;
  } // buscar
  
  public MedicoDO buscarPorUserID(int iduser, Transacao tr) throws Exception {
     Connection con = tr.obterConexao();
     
     String sql = "select * from mediconsulta.Medico where Usuario_iduser=?";
     PreparedStatement ps = con.prepareStatement(sql);
     ps.setInt(1, iduser);
     
     ResultSet rs = ps.executeQuery();
     rs.next();
     MedicoDO medic = new MedicoDO();
     medic.setIdMedico (rs.getInt("idMedico"));
     medic.setUsuarioIdUser2 (rs.getInt("Usuario_iduser"));
     medic.setNome (rs.getString("Nome"));
     medic.setCPF (rs.getString("CPF"));
     medic.setCRM (rs.getString("CRM"));
     medic.setEndereco (rs.getString("Endereco"));
     medic.setNumero (rs.getInt("Numero"));
     medic.setCEP (rs.getString("CEP"));
     medic.setComplemento (rs.getString("Complemento"));
     medic.setBairro (rs.getString("Bairro"));
     medic.setCidade (rs.getString("Cidade"));
     medic.setEstado (rs.getString("Estado"));
     medic.setDataDeNascimento (rs.getDate("DataDeNascimento"));
     medic.setTelefone(rs.getString("Telefone"));
     
     //System.out.println(medic.getNome());
     return medic;
  } // buscar

  public Vector pesquisarPorNome(String nome, Transacao tr) throws Exception {
     Connection con = tr.obterConexao();
     
     String sql = "select * from mediconsulta.Medico where Nome like ?";
     PreparedStatement ps = con.prepareStatement(sql);
     ps.setString(1, "%" + nome + "%");
     
     ResultSet rs = ps.executeQuery();
     System.out.println("Query executada");
     Vector medics = new Vector();
     while (rs.next()) {
        MedicoDO medic = new MedicoDO();
        medic.setIdMedico (rs.getInt("idMedico"));
        medic.setUsuarioIdUser2 (rs.getInt("Usuario_iduser"));
        medic.setNome (rs.getString("Nome"));
        medic.setCPF (rs.getString("CPF"));
        medic.setCRM (rs.getString("CRM"));
        medic.setEndereco (rs.getString("Endereco"));
        medic.setNumero (rs.getInt("Numero"));
        medic.setCEP (rs.getString("CEP"));
        medic.setComplemento (rs.getString("Complemento"));
        if (medic.getComplemento()==null) medic.setComplemento("");
        medic.setBairro (rs.getString("Bairro"));
        if (medic.getBairro()==null) medic.setBairro("");
        medic.setCidade (rs.getString("Cidade"));
        medic.setEstado (rs.getString("Estado"));
        medic.setDataDeNascimento (rs.getDate("DataDeNascimento"));
        medic.setTelefone(rs.getString("Telefone"));
       
        System.out.println(" got " + medic.getNome());
        medics.add(medic);
     }
     return medics;
  } // pesquisarPorNome
  
  public Vector pesquisarPorRua(String rua, Transacao tr) throws Exception {
     Connection con = tr.obterConexao();
     
     String sql = "select * from mediconsulta.Medico where Endereco like ?";
     PreparedStatement ps = con.prepareStatement(sql);
     ps.setString(1, rua);
     
     ResultSet rs = ps.executeQuery();
     System.out.println("Query executada");
     Vector medics = new Vector();
     while (rs.next()) {
        MedicoDO medic = new MedicoDO();
        medic.setIdMedico (rs.getInt("idMedico"));
        medic.setUsuarioIdUser2 (rs.getInt("Usuario_iduser"));
        medic.setNome (rs.getString("Nome"));
        medic.setCPF (rs.getString("CPF"));
        medic.setCRM (rs.getString("CRM"));
        medic.setEndereco (rs.getString("Endereco"));
        medic.setNumero (rs.getInt("Numero"));
        medic.setCEP (rs.getString("CEP"));
        medic.setComplemento (rs.getString("Complemento"));
        if (medic.getComplemento()==null) medic.setComplemento("");
        medic.setBairro (rs.getString("Bairro"));
        if (medic.getBairro()==null) medic.setBairro("");
        medic.setCidade (rs.getString("Cidade"));
        medic.setEstado (rs.getString("Estado"));
        medic.setDataDeNascimento (rs.getDate("DataDeNascimento"));
        medic.setTelefone(rs.getString("Telefone"));
       
        System.out.println(" got " + medic.getNome());
        medics.add(medic);
     }
     return medics;
  } // pesquisarPorRua

  public Vector pesquisarPorCEP(String CEP, Transacao tr) throws Exception {
     Connection con = tr.obterConexao();
     
     String sql = "select * from mediconsulta.Medico where CEP like ?";
     PreparedStatement ps = con.prepareStatement(sql);
     ps.setString(1, CEP);
     
     ResultSet rs = ps.executeQuery();
     System.out.println("Query executada");
     Vector medics = new Vector();
     while (rs.next()) {
        MedicoDO medic = new MedicoDO();
        medic.setIdMedico (rs.getInt("idMedico"));
        medic.setUsuarioIdUser2 (rs.getInt("Usuario_iduser"));
        medic.setNome (rs.getString("Nome"));
        medic.setCPF (rs.getString("CPF"));
        medic.setCRM (rs.getString("CRM"));
        medic.setEndereco (rs.getString("Endereco"));
        medic.setNumero (rs.getInt("Numero"));
        medic.setCEP (rs.getString("CEP"));
        medic.setComplemento (rs.getString("Complemento"));
        if (medic.getComplemento()==null) medic.setComplemento("");
        medic.setBairro (rs.getString("Bairro"));
        if (medic.getBairro()==null) medic.setBairro("");
        medic.setCidade (rs.getString("Cidade"));
        medic.setEstado (rs.getString("Estado"));
        medic.setDataDeNascimento (rs.getDate("DataDeNascimento"));
        medic.setTelefone(rs.getString("Telefone"));
       
        System.out.println(" got " + medic.getNome());
        medics.add(medic);
     }
     return medics;
  } // pesquisarPorCEP
  
  public Vector pesquisarPorCidade(String cidade, Transacao tr) throws Exception {
     Connection con = tr.obterConexao();
     
     String sql = "select * from mediconsulta.Medico where Cidade like ?";
     PreparedStatement ps = con.prepareStatement(sql);
     ps.setString(1, cidade);
     
     ResultSet rs = ps.executeQuery();
     System.out.println("Query executada");
     Vector medics = new Vector();
     while (rs.next()) {
        MedicoDO medic = new MedicoDO();
        medic.setIdMedico (rs.getInt("idMedico"));
        medic.setUsuarioIdUser2 (rs.getInt("Usuario_iduser"));
        medic.setNome (rs.getString("Nome"));
        medic.setCPF (rs.getString("CPF"));
        medic.setCRM (rs.getString("CRM"));
        medic.setEndereco (rs.getString("Endereco"));
        medic.setNumero (rs.getInt("Numero"));
        medic.setCEP (rs.getString("CEP"));
        medic.setComplemento (rs.getString("Complemento"));
        if (medic.getComplemento()==null) medic.setComplemento("");
        medic.setBairro (rs.getString("Bairro"));
        if (medic.getBairro()==null) medic.setBairro("");
        medic.setCidade (rs.getString("Cidade"));
        medic.setEstado (rs.getString("Estado"));
        medic.setDataDeNascimento (rs.getDate("DataDeNascimento"));
        medic.setTelefone(rs.getString("Telefone"));
       
        System.out.println(" got " + medic.getNome());
        medics.add(medic);
     }
     return medics;
  } // pesquisarPorCidade
  
  public Vector pesquisarPorEstado(String estado, Transacao tr) throws Exception {
     Connection con = tr.obterConexao();
     
     String sql = "select * from mediconsulta.Medico where Estado like ?";
     PreparedStatement ps = con.prepareStatement(sql);
     ps.setString(1, estado);
     
     ResultSet rs = ps.executeQuery();
     System.out.println("Query executada");
     Vector medics = new Vector();
     while (rs.next()) {
        MedicoDO medic = new MedicoDO();
        medic.setIdMedico (rs.getInt("idMedico"));
        medic.setUsuarioIdUser2 (rs.getInt("Usuario_iduser"));
        medic.setNome (rs.getString("Nome"));
        medic.setCPF (rs.getString("CPF"));
        medic.setCRM (rs.getString("CRM"));
        medic.setEndereco (rs.getString("Endereco"));
        medic.setNumero (rs.getInt("Numero"));
        medic.setCEP (rs.getString("CEP"));
        medic.setComplemento (rs.getString("Complemento"));
        if (medic.getComplemento()==null) medic.setComplemento("");
        medic.setBairro (rs.getString("Bairro"));
        if (medic.getBairro()==null) medic.setBairro("");
        medic.setCidade (rs.getString("Cidade"));
        medic.setEstado (rs.getString("Estado"));
        medic.setDataDeNascimento (rs.getDate("DataDeNascimento"));
        medic.setTelefone(rs.getString("Telefone"));
       
        System.out.println(" got " + medic.getNome());
        medics.add(medic);
     }
     return medics;
  } // pesquisarPorEstado
  
  public Vector pesquisarPorCRM(String CRM, Transacao tr) throws Exception {
     Connection con = tr.obterConexao();
     
     String sql = "select * from mediconsulta.Medico where CRM like ?";
     PreparedStatement ps = con.prepareStatement(sql);
     ps.setString(1, CRM);
     
     ResultSet rs = ps.executeQuery();
     System.out.println("Query executada");
     Vector medics = new Vector();
     while (rs.next()) {
        MedicoDO medic = new MedicoDO();
        medic.setIdMedico (rs.getInt("idMedico"));
        medic.setUsuarioIdUser2 (rs.getInt("Usuario_iduser"));
        medic.setNome (rs.getString("Nome"));
        medic.setCPF (rs.getString("CPF"));
        medic.setCRM (rs.getString("CRM"));
        medic.setEndereco (rs.getString("Endereco"));
        medic.setNumero (rs.getInt("Numero"));
        medic.setCEP (rs.getString("CEP"));
        medic.setComplemento (rs.getString("Complemento"));
        if (medic.getComplemento()==null) medic.setComplemento("");
        medic.setBairro (rs.getString("Bairro"));
        if (medic.getBairro()==null) medic.setBairro("");
        medic.setCidade (rs.getString("Cidade"));
        medic.setEstado (rs.getString("Estado"));
        medic.setDataDeNascimento (rs.getDate("DataDeNascimento"));
        medic.setTelefone(rs.getString("Telefone"));
       
        System.out.println(" got " + medic.getNome());
        medics.add(medic);
     }
     return medics;
  } // pesquisarPorCRM
  
  public Vector pesquisarPorCPF(String CPF, Transacao tr) throws Exception {
     Connection con = tr.obterConexao();
     
     String sql = "select * from mediconsulta.Medico where CPF like ?";
     PreparedStatement ps = con.prepareStatement(sql);
     ps.setString(1, CPF);
     
     ResultSet rs = ps.executeQuery();
     System.out.println("Query executada");
     Vector medics = new Vector();
     while (rs.next()) {
        MedicoDO medic = new MedicoDO();
        medic.setIdMedico (rs.getInt("idMedico"));
        medic.setUsuarioIdUser2 (rs.getInt("Usuario_iduser"));
        medic.setNome (rs.getString("Nome"));
        medic.setCPF (rs.getString("CPF"));
        medic.setCRM (rs.getString("CRM"));
        medic.setEndereco (rs.getString("Endereco"));
        medic.setNumero (rs.getInt("Numero"));
        medic.setCEP (rs.getString("CEP"));
        medic.setComplemento (rs.getString("Complemento"));
        if (medic.getComplemento()==null) medic.setComplemento("");
        medic.setBairro (rs.getString("Bairro"));
        if (medic.getBairro()==null) medic.setBairro("");
        medic.setCidade (rs.getString("Cidade"));
        medic.setEstado (rs.getString("Estado"));
        medic.setDataDeNascimento (rs.getDate("DataDeNascimento"));
        medic.setTelefone(rs.getString("Telefone"));
       
        System.out.println(" got " + medic.getNome());
        medics.add(medic);
     }
     return medics;
  } // pesquisarPorCPF
  
  public Vector pesquisarPorPlano(int planoId, Transacao tr) throws Exception {
     Connection con = tr.obterConexao();
     
     String sql = "select * from mediconsulta.Medico where idMedico in "+
                  "(select Medico_idMedico from mediconsulta.Plano_de_saude_has_Medico where "+
                  "Plano_de_saude_idPlano_de_saude=?)";
     PreparedStatement ps = con.prepareStatement(sql);
     ps.setInt(1, planoId);
     
     ResultSet rs = ps.executeQuery();
     System.out.println("Query executada");
     Vector medicos = new Vector();
     while (rs.next()) {
        MedicoDO medic = new MedicoDO();
        medic.setIdMedico (rs.getInt("idMedico"));
        medic.setUsuarioIdUser2 (rs.getInt("Usuario_iduser"));
        medic.setNome (rs.getString("Nome"));
        medic.setCPF (rs.getString("CPF"));
        medic.setCRM (rs.getString("CRM"));
        medic.setEndereco (rs.getString("Endereco"));
        medic.setNumero (rs.getInt("Numero"));
        medic.setCEP (rs.getString("CEP"));
        medic.setComplemento (rs.getString("Complemento"));
        if (medic.getComplemento()==null) medic.setComplemento("");
        medic.setBairro (rs.getString("Bairro"));
        if (medic.getBairro()==null) medic.setBairro("");
        medic.setCidade (rs.getString("Cidade"));
        medic.setEstado (rs.getString("Estado"));
        medic.setDataDeNascimento (rs.getDate("DataDeNascimento"));
        medic.setTelefone(rs.getString("Telefone"));

        medicos.add(medic);
     }
     return medicos;
  } // pesquisarPorPlano

  public Vector pesquisarPorHospital(int hospitalId, Transacao tr) throws Exception {
     Connection con = tr.obterConexao();
     
     String sql = "select * from mediconsulta.Medico where idMedico in "+
                  "(select Medico_idMedico from mediconsulta.Medico_has_Hospital where "+
                  "Hospital_idHospital=?)";
     PreparedStatement ps = con.prepareStatement(sql);
     ps.setInt(1, hospitalId);
     
     ResultSet rs = ps.executeQuery();
     System.out.println("Query executada");
     Vector medicos = new Vector();
     while (rs.next()) {
        MedicoDO medic = new MedicoDO();
        medic.setIdMedico (rs.getInt("idMedico"));
        medic.setUsuarioIdUser2 (rs.getInt("Usuario_iduser"));
        medic.setNome (rs.getString("Nome"));
        medic.setCPF (rs.getString("CPF"));
        medic.setCRM (rs.getString("CRM"));
        medic.setEndereco (rs.getString("Endereco"));
        medic.setNumero (rs.getInt("Numero"));
        medic.setCEP (rs.getString("CEP"));
        medic.setComplemento (rs.getString("Complemento"));
        if (medic.getComplemento()==null) medic.setComplemento("");
        medic.setBairro (rs.getString("Bairro"));
        if (medic.getBairro()==null) medic.setBairro("");
        medic.setCidade (rs.getString("Cidade"));
        medic.setEstado (rs.getString("Estado"));
        medic.setDataDeNascimento (rs.getDate("DataDeNascimento"));
        medic.setTelefone(rs.getString("Telefone"));

        medicos.add(medic);
     }
     return medicos;
  } // pesquisarPorHospital
  
  public void incluirEspec(MedicoDO medic, EspecialidadeDO espec, Transacao tr) throws Exception {
     Connection con = tr.obterConexao();
     String sql = "insert into mediconsulta.Medico_has_Especialidade (Medico_idMedico, Especialidade_idEspecialidade) values (?,?)";
     PreparedStatement ps = con.prepareStatement(sql);
     ps.setInt(1, medic.getIdMedico());
     ps.setInt(2, espec.getId());
     ps.executeUpdate();
  }
  
  public void incluirHospital(MedicoDO medic, HospitalDO hospital, Transacao tr) throws Exception {
     Connection con = tr.obterConexao();
     String sql = "insert into mediconsulta.Medico_has_Hospital (Medico_idMedico, Hospital_idHospital) values (?,?)";
     PreparedStatement ps = con.prepareStatement(sql);
     ps.setInt(1, medic.getIdMedico());
     ps.setInt(2, hospital.getIdHospital());
     ps.executeUpdate();
  }

  public Vector pesquisarPorEspecialidade(int especialidadeId, Transacao tr) throws Exception {
     Connection con = tr.obterConexao();
     
     String sql = "select * from mediconsulta.Medico where idMedico in "+
                  "(select Medico_idMedico from mediconsulta.Medico_has_Especialidade where "+
                  "Especialidade_idEspecialidade=?)";
     PreparedStatement ps = con.prepareStatement(sql);
     ps.setInt(1, especialidadeId);
     
     ResultSet rs = ps.executeQuery();
     System.out.println("Query executada");
     Vector medicos = new Vector();
     while (rs.next()) {
        MedicoDO medic = new MedicoDO();
        medic.setIdMedico (rs.getInt("idMedico"));
        medic.setUsuarioIdUser2 (rs.getInt("Usuario_iduser"));
        medic.setNome (rs.getString("Nome"));
        medic.setCPF (rs.getString("CPF"));
        medic.setCRM (rs.getString("CRM"));
        medic.setEndereco (rs.getString("Endereco"));
        medic.setNumero (rs.getInt("Numero"));
        medic.setCEP (rs.getString("CEP"));
        medic.setComplemento (rs.getString("Complemento"));
        if (medic.getComplemento()==null) medic.setComplemento("");
        medic.setBairro (rs.getString("Bairro"));
        if (medic.getBairro()==null) medic.setBairro("");
        medic.setCidade (rs.getString("Cidade"));
        medic.setEstado (rs.getString("Estado"));
        medic.setDataDeNascimento (rs.getDate("DataDeNascimento"));
        medic.setTelefone(rs.getString("Telefone"));

        medicos.add(medic);
     }
     return medicos;
  } // pesquisarPorEspecialidade
  
  public Vector pesquisarEspecialidades(int MedicoId, Transacao tr) throws Exception {
     Connection con = tr.obterConexao();
     
     String sql = "select * from mediconsulta.Especialidade where Id in "+
                  "(select Especialidade_idEspecialidade from mediconsulta.Medico_has_Especialidade where "+
                  "Medico_idMedico=?)";
     PreparedStatement ps = con.prepareStatement(sql);
     ps.setInt(1, MedicoId);
     
     ResultSet rs = ps.executeQuery();
     System.out.println("Query executada");
     Vector especialidades = new Vector();
     while (rs.next()) {
        EspecialidadeDO espec = new EspecialidadeDO();
        espec.setId(rs.getInt("id"));
        espec.setNome(rs.getString("nome"));

        especialidades.add(espec);
     }
     return especialidades;
  } // pesquisarEspecialidades
  
  public Vector pesquisarPorDisponibilidade(Timestamp data, Transacao tr) throws Exception {
     Connection con = tr.obterConexao();
     
     String sql = "select * from mediconsulta.Medico where idMedico not in "+
                  "(select Medico_idMedico from mediconsulta.Consulta where "+
                  "DataDaConsulta=?)";
     PreparedStatement ps = con.prepareStatement(sql);
     ps.setTimestamp(1, data);
     
     ResultSet rs = ps.executeQuery();
     System.out.println("Query executada");
     Vector medicos = new Vector();
     while (rs.next()) {
        MedicoDO medic = new MedicoDO();
        medic.setIdMedico (rs.getInt("idMedico"));
        medic.setUsuarioIdUser2 (rs.getInt("Usuario_iduser"));
        medic.setNome (rs.getString("Nome"));
        medic.setCPF (rs.getString("CPF"));
        medic.setCRM (rs.getString("CRM"));
        medic.setEndereco (rs.getString("Endereco"));
        medic.setNumero (rs.getInt("Numero"));
        medic.setCEP (rs.getString("CEP"));
        medic.setComplemento (rs.getString("Complemento"));
        if (medic.getComplemento()==null) medic.setComplemento("");
        medic.setBairro (rs.getString("Bairro"));
        if (medic.getBairro()==null) medic.setBairro("");
        medic.setCidade (rs.getString("Cidade"));
        medic.setEstado (rs.getString("Estado"));
        medic.setDataDeNascimento (rs.getDate("DataDeNascimento"));
        medic.setTelefone(rs.getString("Telefone"));

        medicos.add(medic);
     }
     return medicos;
  } // pesquisarPorDisponibilidade
  
} // MedicoData

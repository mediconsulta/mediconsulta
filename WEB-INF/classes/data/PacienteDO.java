package data;

import java.util.*;

public class PacienteDO {
  private int _id, _userId;
  private String _cpf, _telefone;
  private String _nome;
  private String _sexo;
  private Date _nascimento;
 

  public int getId() {
     return _id;
  } // getId

  public void setId(int id) {
    _id = id;
  } // setId
  
   public int getUserId() {
     return _userId;
  } // getUserId

  public void setUserId(int userId) {
    _userId = userId;
  } // setUserId

  public String getNome() {
    return _nome;
  } // getNome

  public void setNome(String nome) {
    _nome = nome;
  } // setNome

   public String getCpf() {
     return _cpf;
  } // getCpf

  public void setCpf(String cpf) {
    _cpf = cpf;
  } // setCpf

  public String getSexo() {
     return _sexo;
  } // getSexo

  public void setSexo(String sexo) {
    _sexo = sexo;
  } // setSexo
  
  public Date getNascimento() {
      return _nascimento;
  } // getNascimento
  
  public void setNascimento(Date nascimento) {
      _nascimento = nascimento;
  }
  
  public String getTelefone() {
    return _telefone;
  } // getTelefone

  public void setTelefone(String tel) {
    _telefone = tel;
  } // setTelefone


} // PacienteDO
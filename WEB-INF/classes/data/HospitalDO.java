package data;

public class HospitalDO {
  private int _id;
  private String _nome;
  private String _endereco;
  private int _numero;
  private String _complemento;
  private String _cep;
  private String _bairro;
  private String _cidade;
  private String _estado;
  private String _telefone;
  private String _email;
  private String _website;
  private String _ps;

  public int getIdHospital() {
     return _id;
  } // getId

  public void setIdHospital(int id) {
    _id = id;
  } // setId
  
  public String getNome() {
    return _nome;
  } // obterNome

  public void setNome(String nome) {
    _nome = nome;
  } // setNome
  
 public String getEndereco() {
    return _endereco;
  } // getEndereco

  public void setEndereco(String enderecp) {
    _endereco = enderecp;
  } // setEndereco

  public int getNumero() {
     return _numero;
  } // getNumero

  public void setNumero(int numero) {
    _numero = numero;
  } // setNumero

  public String getComplemento() {
    return _complemento;
  } // getComplemento

  public void setComplemento(String complemento) {
    _complemento = complemento;
  } // setComplemento
  
  public String getCep() {
     return _cep;
  } // getCep

  public void setCep(String cep) {
    _cep = cep;
  } // setCep
  
  public String getBairro() {
    return _bairro;
  } // getBairro

  public void setBairro(String bairro) {
    _bairro = bairro;
  } // setBairro
  
  public String getCidade() {
    return _cidade;
  } // getCidade

  public void setCidade(String cidade) {
    _cidade = cidade;
  } // setCidade
  
  public String getEstado() {
    return _estado;
  } // getEstado

  public void setEstado(String estado) {
    _estado = estado;
  } // setEstado
  
  public String getProntoSocorro() {
    return _ps;
  } // getProntoSocorro

  public void setProntoSocorro(String ps) {
    _ps = ps;
  } // setProntoSocorro
} // HospitalDO 
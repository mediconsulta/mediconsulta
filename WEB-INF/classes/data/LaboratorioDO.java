/* 
    Document   : LaboratorioDO
    Created on : 02/11
    Author     : Ivan
*/
package data;

public class LaboratorioDO {
  private int _id;
  private int _tipo_de_exame_idTipo_de_exame;
  private String _nome;
  private String _endereco;
  private int _numero;
  private String _complemento;
  private String _cep;
  private String _bairro;
  private String _cidade;
  private String _estado;
  private String _telefone;
  private String _email;
  private String _website;

  public int getId() {
     return _id;
  } // getId

  public void setId(int id) {
    _id = id;
  } // setId
  
  public int getTipo_de_exame() {
     return _tipo_de_exame_idTipo_de_exame;
  } // getTipo_de_exame

  public void setTipo_de_exame(int tipo) {
    _tipo_de_exame_idTipo_de_exame = tipo;
  } // setTipo_de_exame

  public String getNome() {
    return _nome;
  } // obterNome

  public void setNome(String nome) {
    _nome = nome;
  } // setNome
  
 public String getEndereco() {
    return _endereco;
  } // getEndereco

  public void setEndereco(String enderecp) {
    _endereco = enderecp;
  } // setEndereco

  public int getNumero() {
     return _numero;
  } // getNumero

  public void setNumero(int numero) {
    _numero = numero;
  } // setNumero

  public String getComplemento() {
    return _complemento;
  } // getComplemento

  public void setComplemento(String complemento) {
    _complemento = complemento;
  } // setComplemento
  
  public String getCep() {
     return _cep;
  } // getCep

  public void setCep(String cep) {
    _cep = cep;
  } // setCep
  
  public String getBairro() {
    return _bairro;
  } // getBairro

  public void setBairro(String bairro) {
    _bairro = bairro;
  } // setBairro
  
  public String getCidade() {
    return _cidade;
  } // getCidade

  public void setCidade(String cidade) {
    _cidade = cidade;
  } // setCidade
  
  public String getEstado() {
    return _estado;
  } // getEstado

  public void setEstado(String estado) {
    _estado = estado;
  } // setEstado
  
  public String getTelefone() {
    return _telefone;
  } // getTelefone

  public void setTelefone(String tel) {
    _telefone = tel;
  } // setTelefone

  public String getEmail() {
    return _email;
  } // getEmail

  public void setEmail(String email) {
    _email = email;
  } // setEmail
  
  public String getWebsite() {
    return _website;
  } // getWebsite

  public void setWebsite(String website) {
    _website = website;
  } // setWebsite

} // LaboratorioDO 
package data;

import java.util.*;

public class UsuarioDO {
  private int _iduser, _tipo;
  private String _email, _password;

  
  
  public int getIduser() {
     return _iduser;
  } // getIdUser

  public void setIduser(int iduser_2) {
    _iduser = iduser_2;
  } // setIdUser
  

  public String getEmail() {
    return _email;
  } // getEmail

  public void setEmail(String email) {
    _email = email;
  } // setEmail

   public int getTipo() {
     return _tipo;
  } // getTipo

  public void setTipo(int tipo) {
    _tipo = tipo;
  } // setTipo
  
  public String getPassword() {
    return _password;
  } // gePassword

  public void setPassword(String password_2) {
    _password = password_2;
  } // setPassword
  

} // UsuarioDO



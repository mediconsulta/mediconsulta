package data;

import java.sql.*;
import java.util.*;
import utils.Transacao;

public class PlanoDeSaudeData {

  public void incluir(PlanoDeSaudeDO plano, Transacao tr) throws Exception {
     Connection con = tr.obterConexao();
     
     String sql = "insert into mediconsulta.Plano_de_saude (Nome) values (?)";
     PreparedStatement ps = con.prepareStatement(sql);
     ps.setString(1, plano.getNome());
     
     int result = ps.executeUpdate();
  }

  public void excluir(PlanoDeSaudeDO plano, Transacao tr) throws Exception {
     excluir(plano.getId(), tr);
  } // excluir

  public void excluir (int idobj, Transacao tr) throws Exception {
     Connection con = tr.obterConexao();
      
     String sql = "delete * from mediconsulta.Plano_de_saude where idPlano_de_saude=?";
     PreparedStatement ps = con.prepareStatement(sql);
     ps.setInt(1, idobj);
     
     int result = ps.executeUpdate();
  } // excluir 

  public void atualizar(PlanoDeSaudeDO plano, Transacao tr) throws Exception {
     Connection con = tr.obterConexao();
     
     String sql = "update mediconsulta.Plano_de_saude set Nome=? where idPlano_de_saude=?";
     PreparedStatement ps = con.prepareStatement(sql);
     ps.setString(1, plano.getNome());
     ps.setInt(2, plano.getId());
     
     int result = ps.executeUpdate();
  } // atualizar

  public PlanoDeSaudeDO buscar(int idobj, Transacao tr) throws Exception {
     Connection con = tr.obterConexao();
     
     String sql = "select * from mediconsulta.Plano_de_saude where  idPlano_de_saude=?";
     PreparedStatement ps = con.prepareStatement(sql);
     ps.setInt(1, idobj);
     
     ResultSet rs = ps.executeQuery();
     rs.next();
     PlanoDeSaudeDO plano = new PlanoDeSaudeDO();
     plano.setId (rs.getInt("idPlano_de_saude"));
     plano.setNome (rs.getString("Nome"));

     return plano;
  } // buscar

  public Vector pesquisarPorNome(String nome, Transacao tr) throws Exception {
     Connection con = tr.obterConexao();
     
     String sql = "select * from mediconsulta.Plano_de_saude where Nome like ?";
     PreparedStatement ps = con.prepareStatement(sql);
     ps.setString(1, nome);
     
     ResultSet rs = ps.executeQuery();
     System.out.println("Query executada");
     Vector planos = new Vector();
     while (rs.next()) {
        PlanoDeSaudeDO plano = new PlanoDeSaudeDO();
        plano.setId (rs.getInt("idPlano_de_saude"));
        plano.setNome (rs.getString("Nome"));
       
        System.out.println(" got " + plano.getNome());
        planos.add(plano);
     }
     return planos;
  } // pesquisarPorNome
  
  public Vector pesquisarPorMedico(int medicoId, Transacao tr) throws Exception {
     Connection con = tr.obterConexao();
     
     String sql = "select * from mediconsulta.Plano_de_saude where idPlano_de_saude in "+
                  "(select Plano_de_saude_idPlano_de_saude from mediconsulta.Plano_de_saude_has_Medico where "+
                  "Medico_idMedico=?)";
     PreparedStatement ps = con.prepareStatement(sql);
     ps.setInt(1, medicoId);
     
     ResultSet rs = ps.executeQuery();
     System.out.println("Query executada");
     Vector planos = new Vector();
     while (rs.next()) {
        PlanoDeSaudeDO plano = new PlanoDeSaudeDO();
        plano.setId (rs.getInt("idPlano_de_saude"));
        plano.setNome (rs.getString("Nome"));

        //System.out.println(" got " + plano.getNome());
        planos.add(plano);
     }
     return planos;
  } // pesquisarPorMedico
  
  public Vector ListarTodos(Transacao tr) throws Exception {
     Connection con = tr.obterConexao();
     
     String sql = "select * from mediconsulta.Plano_de_saude";
     PreparedStatement ps = con.prepareStatement(sql);
     ResultSet rs = ps.executeQuery();
     System.out.println("Query executada");
     Vector planos = new Vector();
     while (rs.next()) {
        PlanoDeSaudeDO plano = new PlanoDeSaudeDO();
        plano.setId (rs.getInt("idPlano_de_saude"));
        plano.setNome (rs.getString("Nome"));
       
        System.out.println(" got " + plano.getNome());
        planos.add(plano);
     }
     return planos;
  } // ListarTodos
  
  public void incluirMedico(MedicoDO medic, PlanoDeSaudeDO plano, Transacao tr) throws Exception {
     Connection con = tr.obterConexao();
     String sql = "insert into mediconsulta.Plano_de_saude_has_Medico (Plano_de_saude_idPlano_de_saude, Medico_idMedico) values (?,?)";
     PreparedStatement ps = con.prepareStatement(sql);
     ps.setInt(1, plano.getId());
     ps.setInt(2, medic.getIdMedico());
     ps.executeUpdate();
  }

} // PlanoDeSaudeData
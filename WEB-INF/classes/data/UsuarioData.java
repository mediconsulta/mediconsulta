package data;

import java.sql.*;
import java.util.Vector;
import utils.Transacao;


public class UsuarioData {

  public void incluir(UsuarioDO usuario, Transacao tr) throws Exception {
     Connection con = tr.obterConexao();
     
     String sql = "insert into mediconsulta.Usuario (Email, Password_2, Tipo ) values (?,?,?)";
     PreparedStatement ps = con.prepareStatement(sql);
     ps.setString(1, usuario.getEmail());
     ps.setString(2, usuario.getPassword());
     ps.setInt(3, usuario.getTipo());
     
     int result = ps.executeUpdate();
  }

  public void excluir(UsuarioDO usuario, Transacao tr) throws Exception {
     excluir(usuario.getIduser(), tr);
  } // excluir

  public void excluir (int idobj, Transacao tr) throws Exception {
     Connection con = tr.obterConexao();
      
     String sql = "delete from mediconsulta.Usuario where iduser=?";
     PreparedStatement ps = con.prepareStatement(sql);
     ps.setInt(1, idobj);
     
     int result = ps.executeUpdate();
  } // excluir 

  public void atualizar(UsuarioDO usuario, Transacao tr) throws Exception {
     Connection con = tr.obterConexao();
     
     String sql = "update mediconsulta.Usuario set Email=?, Password_2=?, Tipo=? where iduser=?";
     PreparedStatement ps = con.prepareStatement(sql);
     ps.setString(1, usuario.getEmail());
     ps.setString(2, usuario.getPassword());
     ps.setInt(3, usuario.getTipo());
     ps.setInt(4, usuario.getIduser());
     
     int result = ps.executeUpdate();
  } // atualizar

  public UsuarioDO buscar(int idobj, Transacao tr) throws Exception {
     Connection con = tr.obterConexao();
     
     String sql = "select * from mediconsulta.Usuario where  iduser=?";
     PreparedStatement ps = con.prepareStatement(sql);
     ps.setInt(1, idobj);
     
     ResultSet rs = ps.executeQuery();
     rs.next();
     UsuarioDO usuario = new UsuarioDO();
     usuario.setIduser (rs.getInt("iduser"));
     usuario.setTipo (rs.getInt("Tipo"));
     usuario.setEmail (rs.getString("Email"));
     usuario.setPassword (rs.getString("Password_2"));

     
     return usuario;
  } // buscar
  
  public UsuarioDO buscarUser(String username, String pass, Transacao tr) throws Exception {
     Connection con = tr.obterConexao();
     
     String sql = "select * from mediconsulta.Usuario where Email=? and Password_2=?";
     PreparedStatement ps = con.prepareStatement(sql);
     ps.setString(1, username);
     ps.setString(2, pass);
     
     ResultSet rs = ps.executeQuery();
     rs.next();
     UsuarioDO usuario = new UsuarioDO();
     usuario.setIduser (rs.getInt("iduser"));
     usuario.setTipo (rs.getInt("Tipo"));
     usuario.setEmail (rs.getString("Email"));
     usuario.setPassword (rs.getString("Password_2"));

     
     return usuario;
  } // buscarUser
  
  public boolean checkLogin(String username, String pass, Transacao tr) throws Exception {
     Connection con = tr.obterConexao();
     
     String sql = "select * from mediconsulta.Usuario where Email=? and Password_2=?";
     PreparedStatement ps = con.prepareStatement(sql);
     ps.setString(1, username);
     ps.setString(2, pass);
     
     ResultSet rs = ps.executeQuery();
     if (rs.next())
         return true;
     
     return false;
  } // checkLogin
  
  public UsuarioDO buscarEmail(String email, Transacao tr) throws Exception {
     Connection con = tr.obterConexao();
     String sql = "select * from mediconsulta.Usuario where Email=?";
     PreparedStatement ps = con.prepareStatement(sql);
     ps.setString(1,email);
     
     ResultSet rs = ps.executeQuery();
     rs.next();
     UsuarioDO usuario = new UsuarioDO();
     usuario.setIduser (rs.getInt("iduser"));
     usuario.setTipo (rs.getInt("Tipo"));
     usuario.setEmail (rs.getString("Email"));
     usuario.setPassword (rs.getString("Password_2"));

     return usuario;
  } // buscarEmail
  
  public Vector pesquisarPorEmail(String email, Transacao tr) throws Exception {
     Connection con = tr.obterConexao();
     
     String sql = "select * from mediconsulta.Usuario where Email like ?";
     PreparedStatement ps = con.prepareStatement(sql);
     ps.setString(1, email);
     
     ResultSet rs = ps.executeQuery();
     System.out.println("Query executada");
     Vector usuarios = new Vector();
     while (rs.next()) {
        UsuarioDO usuario = new UsuarioDO();
        usuario.setIduser (rs.getInt("iduser"));
        usuario.setTipo (rs.getInt("Tipo"));
        usuario.setEmail (rs.getString("Email"));
        usuario.setPassword (rs.getString("Password_2"));
       
        System.out.println(" got " + usuario.getEmail());
        usuarios.add(usuario);
     }
     return usuarios;
  } // pesquisarPorCPF
   
  public UsuarioDO buscarID(int id, Transacao tr) throws Exception {
     Connection con = tr.obterConexao();
     String sql = "select * from mediconsulta.Usuario where iduser=?";
     PreparedStatement ps = con.prepareStatement(sql);
     ps.setInt(1,id);
     
     ResultSet rs = ps.executeQuery();
     rs.next();
     UsuarioDO usuario = new UsuarioDO();
     usuario.setIduser (rs.getInt("iduser"));
     usuario.setTipo (rs.getInt("Tipo"));
     usuario.setEmail (rs.getString("Email"));
     usuario.setPassword (rs.getString("Password_2"));

     return usuario;
  } // buscarID
  
  public Vector getEmails(Transacao tr, int tipo) throws Exception{
      Connection con = tr.obterConexao();
     
     String sql = "select * from mediconsulta.Usuario where Tipo=?";
     PreparedStatement ps = con.prepareStatement(sql);
     ps.setInt(1, tipo);
     
     ResultSet rs = ps.executeQuery();
     System.out.println("Query executada");
     Vector emails = new Vector();
     while (rs.next()) {
         String s = rs.getString("Email");
         emails.add(s);
     }
     return emails;
  }
  
} // UsuarioData
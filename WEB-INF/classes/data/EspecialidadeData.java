package data;

import java.sql.*;
import java.util.*;
import utils.Transacao;

public class EspecialidadeData {

  public void incluir(EspecialidadeDO especialidade, Transacao tr) throws Exception {
     Connection con = tr.obterConexao();
     
     String sql = "insert into mediconsulta.Especialidade (Nome) values (?)";
     PreparedStatement ps = con.prepareStatement(sql);
     ps.setString(1, especialidade.getNome());
     
     int result = ps.executeUpdate();
  }

  public void excluir(EspecialidadeDO especialidade, Transacao tr) throws Exception {
     excluir(especialidade.getId(), tr);
  } // excluir

  public void excluir (int idobj, Transacao tr) throws Exception {
     Connection con = tr.obterConexao();
      
     String sql = "delete * from mediconsulta.Especialidade where idEspecialidade=?";
     PreparedStatement ps = con.prepareStatement(sql);
     ps.setInt(1, idobj);
     
     int result = ps.executeUpdate();
  } // excluir 

  public void atualizar(EspecialidadeDO especialidade, Transacao tr) throws Exception {
     Connection con = tr.obterConexao();
     
     String sql = "update mediconsulta.Especialidade set Nome=? where idEspecialidade=?";
     PreparedStatement ps = con.prepareStatement(sql);
     ps.setString(1, especialidade.getNome());
     ps.setInt(2, especialidade.getId());
     
     int result = ps.executeUpdate();
  } // atualizar

  public EspecialidadeDO buscar(int idobj, Transacao tr) throws Exception {
     Connection con = tr.obterConexao();
     
     String sql = "select * from mediconsulta.Especialidade where  idEspecialidade=?";
     PreparedStatement ps = con.prepareStatement(sql);
     ps.setInt(1, idobj);
     
     ResultSet rs = ps.executeQuery();
     rs.next();
     EspecialidadeDO plano = new EspecialidadeDO();
     plano.setId (rs.getInt("idEspecialidade"));
     plano.setNome (rs.getString("nome"));

     return plano;
  } // buscar

  public Vector pesquisarPorNome(String nome, Transacao tr) throws Exception {
     Connection con = tr.obterConexao();
     
     String sql = "select * from mediconsulta.Especialidade where Nome like ?";
     PreparedStatement ps = con.prepareStatement(sql);
     ps.setString(1, nome);
     
     ResultSet rs = ps.executeQuery();
     System.out.println("Query executada");
     Vector especialidades = new Vector();
     while (rs.next()) {
        EspecialidadeDO especialidade = new EspecialidadeDO();
        especialidade.setId (rs.getInt("idEspecialidade"));
        especialidade.setNome (rs.getString("nome"));
       
        System.out.println(" got " + especialidade.getNome());
        especialidades.add(especialidade);
     }
     return especialidades;
  } // pesquisarPorNome
  
  public Vector pesquisarPorMedico(int medicoId, Transacao tr) throws Exception {
     Connection con = tr.obterConexao();
     
     String sql = "select * from mediconsulta.Especialidade where idEspecialidade in "+
                  "(select Especialidade_idEspecialidade from mediconsulta.Medico_has_Especialidade where "+
                  "Medico_idMedico=?)";
     PreparedStatement ps = con.prepareStatement(sql);
     ps.setInt(1, medicoId);
     
     ResultSet rs = ps.executeQuery();
     System.out.println("Query executada");
     Vector especialidades = new Vector();
     while (rs.next()) {
        EspecialidadeDO especialidade = new EspecialidadeDO();
        especialidade.setId (rs.getInt("idEspecialidade"));
        especialidade.setNome (rs.getString("nome"));

        //System.out.println(" got " + plano.getNome());
        especialidades.add(especialidade);
     }
     return especialidades;
  } // pesquisarPorMedico
  
  public Vector ListarTodos(Transacao tr) throws Exception {
     Connection con = tr.obterConexao();
     
     String sql = "select * from mediconsulta.Especialidade";
     PreparedStatement ps = con.prepareStatement(sql);
     ResultSet rs = ps.executeQuery();
     System.out.println("Query executada");
     Vector especialidades = new Vector();
     while (rs.next()) {
        EspecialidadeDO especialidade = new EspecialidadeDO();
        especialidade.setId (rs.getInt("idEspecialidade"));
        especialidade.setNome (rs.getString("nome"));
       
        System.out.println(" got " + especialidade.getNome());
        especialidades.add(especialidade);
     }
     return especialidades;
  } // ListarTodos

} // EspecialidadeData

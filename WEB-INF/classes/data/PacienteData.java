package data;

import java.sql.*;
import java.util.*;
import utils.Transacao;

public class PacienteData {

  public void incluir(PacienteDO paciente, Transacao tr) throws Exception {
     Connection con = tr.obterConexao();
     java.sql.Date sqlDate = new java.sql.Date(paciente.getNascimento().getTime());
     
     String sql = "insert into mediconsulta.Paciente (Usuario_iduser, Nome, CPF, Sexo, DataDeNascimento, Telefone) values (?,?,?,?,?,?)";
     PreparedStatement ps = con.prepareStatement(sql);
     ps.setInt(1, paciente.getUserId());
     ps.setString(2, paciente.getNome());
     ps.setString(3, paciente.getCpf());
     ps.setString(4, paciente.getSexo());
     ps.setDate(5, sqlDate);
     ps.setString(6, paciente.getTelefone());
     
     int result = ps.executeUpdate();
  }

  public void excluir(PacienteDO paciente, Transacao tr) throws Exception {
     excluir(paciente.getId(), tr);
  } // excluir

  public void excluir (int idobj, Transacao tr) throws Exception {
     Connection con = tr.obterConexao();
      
     String sql = "delete from mediconsulta.Paciente where idPaciente=?";
     PreparedStatement ps = con.prepareStatement(sql);
     ps.setInt(1, idobj);
     
     int result = ps.executeUpdate();
  } // excluir 

  public void atualizar(PacienteDO paciente, Transacao tr) throws Exception {
     Connection con = tr.obterConexao();
     java.sql.Date sqlDate = new java.sql.Date(paciente.getNascimento().getTime());
     
     String sql = "update mediconsulta.Paciente set Nome=?, CPF=?, Sexo=?, DataDeNascimento=?, Telefone=? where idPaciente=?";
     PreparedStatement ps = con.prepareStatement(sql);
     ps.setString(1, paciente.getNome());
     ps.setString(2, paciente.getCpf());
     ps.setString(3, paciente.getSexo());
     ps.setDate(4, sqlDate);
     ps.setString(5, paciente.getTelefone());
     ps.setInt(6, paciente.getId());
     
     int result = ps.executeUpdate();
  } // atualizar

  public PacienteDO buscar(int idobj, Transacao tr) throws Exception {
     Connection con = tr.obterConexao();
     
     String sql = "select * from mediconsulta.Paciente where idPaciente=?";
     PreparedStatement ps = con.prepareStatement(sql);
     ps.setInt(1, idobj);
     
     ResultSet rs = ps.executeQuery();
     rs.next();
     PacienteDO paciente = new PacienteDO();
     paciente.setId (rs.getInt("idPaciente"));
     paciente.setUserId (rs.getInt("Usuario_iduser"));
     paciente.setNome (rs.getString("Nome"));
     paciente.setCpf (rs.getString("CPF"));
     paciente.setSexo (rs.getString("Sexo"));
     paciente.setNascimento (rs.getDate("DataDeNascimento"));
     paciente.setTelefone(rs.getString("Telefone"));
     
     return paciente;
  } // buscar
  
    public PacienteDO buscarPorUserID(int iduser, Transacao tr) throws Exception {
     Connection con = tr.obterConexao();
     
     String sql = "select * from mediconsulta.Paciente where Usuario_iduser=?";
     PreparedStatement ps = con.prepareStatement(sql);
     ps.setInt(1, iduser);
     
     ResultSet rs = ps.executeQuery();
     rs.next();
     PacienteDO paciente = new PacienteDO();
     paciente.setId (rs.getInt("idPaciente"));
     paciente.setUserId (rs.getInt("Usuario_iduser"));
     paciente.setNome (rs.getString("Nome"));
     paciente.setCpf (rs.getString("CPF"));
     paciente.setSexo (rs.getString("Sexo"));
     paciente.setNascimento (rs.getDate("DataDeNascimento"));
     paciente.setTelefone(rs.getString("Telefone"));
     
     return paciente;
  } // buscar

  public Vector pesquisarPorNome(String nome, Transacao tr) throws Exception {
     Connection con = tr.obterConexao();
     
     String sql = "select * from mediconsulta.Paciente where Nome like ?";
     PreparedStatement ps = con.prepareStatement(sql);
     ps.setString(1, "%" + nome + "%");
     
     ResultSet rs = ps.executeQuery();
     System.out.println("Query executada");
     Vector pacientes = new Vector();
     while (rs.next()) {
        PacienteDO paciente = new PacienteDO();
        paciente.setId (rs.getInt("idPaciente"));
        paciente.setUserId (rs.getInt("Usuario_iduser"));
        paciente.setNome (rs.getString("Nome"));
        paciente.setCpf (rs.getString("CPF"));
        paciente.setSexo (rs.getString("Sexo"));
        paciente.setNascimento (rs.getDate("DataDeNascimento"));
        paciente.setTelefone(rs.getString("Telefone"));
       
        System.out.println(" got " + paciente.getNome());
        pacientes.add(paciente);
     }
     return pacientes;
  } // pesquisarPorNome
  
 public Vector pesquisarPorCPF(String CPF, Transacao tr) throws Exception {
     Connection con = tr.obterConexao();
     
     String sql = "select * from mediconsulta.Paciente where CPF like ?";
     PreparedStatement ps = con.prepareStatement(sql);
     ps.setString(1, CPF);
     
     ResultSet rs = ps.executeQuery();
     System.out.println("Query executada");
     Vector pacientes = new Vector();
     while (rs.next()) {
        PacienteDO paciente = new PacienteDO();
        paciente.setId (rs.getInt("idPaciente"));
        paciente.setUserId (rs.getInt("Usuario_iduser"));
        paciente.setNome (rs.getString("Nome"));
        paciente.setCpf (rs.getString("CPF"));
        paciente.setSexo (rs.getString("Sexo"));
        paciente.setNascimento (rs.getDate("DataDeNascimento"));
        paciente.setTelefone(rs.getString("Telefone"));
       
        System.out.println(" got " + paciente.getNome());
        pacientes.add(paciente);
     }
     return pacientes;
  } // pesquisarPorCPF
} // PacienteData
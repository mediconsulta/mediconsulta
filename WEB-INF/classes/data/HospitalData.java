package data;

import java.sql.*;
import java.util.*;
import utils.Transacao;

public class HospitalData {

  public void incluir(HospitalDO hospital, Transacao tr) throws Exception {
     Connection con = tr.obterConexao();
     String sql = "INSERT INTO `Hospital`(`Nome`, `Endereco`, `Numero`, `Complemento`, `CEP`, `Bairro`, `Cidade`, `Estado`, `ProntoSocorro`) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)";
	
     System.out.println(sql);
     PreparedStatement ps = con.prepareStatement(sql);
	 ps.setString(1, hospital.getNome());
	 ps.setString(2, hospital.getEndereco());
	 ps.setInt(3, hospital.getNumero());
	 ps.setString(4, hospital.getComplemento());
	 ps.setString(5, hospital.getCep());
	 ps.setString(6, hospital.getBairro());
	 ps.setString(7, hospital.getCidade());
	 ps.setString(8, hospital.getEstado());
	 ps.setString(9, hospital.getProntoSocorro());
     int result = ps.executeUpdate();
  }

  public void excluir(HospitalDO hospital, Transacao tr) throws Exception {
     excluir(hospital.getIdHospital(), tr);
  } // excluir

  public void excluir (int idobj, Transacao tr) throws Exception {
  } // excluir 

  public void atualizar(HospitalDO hospital, Transacao tr) throws Exception {
     Connection con = tr.obterConexao();
     String sql = "UPDATE `Hospital` SET `Nome`=?,`Endereco`=?,`Numero`=?,`Complemento`=?,`CEP`=?,`Bairro`=?,`Cidade`=?,`Estado`=?,`ProntoSocorro`=? WHERE `idHospital`=?";
	 
     PreparedStatement ps = con.prepareStatement(sql);
	 ps.setString(1, hospital.getNome());
	 ps.setString(2, hospital.getEndereco());
	 ps.setInt(3, hospital.getNumero());
	 ps.setString(4, hospital.getComplemento());
	 ps.setString(5, hospital.getCep());
	 ps.setString(6, hospital.getBairro());
	 ps.setString(7, hospital.getCidade());
	 ps.setString(8, hospital.getEstado());
	 ps.setString(9, hospital.getProntoSocorro());
	 ps.setInt(10, hospital.getIdHospital());
     int result = ps.executeUpdate();
  } // atualizar

  public HospitalDO buscar(int idobj, Transacao tr) throws Exception {
     Connection con = tr.obterConexao();
     String sql = "select * from Hospital where idHospital=?";
     PreparedStatement ps = con.prepareStatement(sql);
     ps.setInt(1, idobj);
     ResultSet rs = ps.executeQuery();
     rs.next();
     HospitalDO hospital = new HospitalDO();
     hospital.setIdHospital (rs.getInt("idHospital"));
     hospital.setNome (rs.getString("nome"));
     return hospital;
  } // buscar

  public Vector pesquisarPorNome(String nome, Transacao tr) throws Exception {
     Connection con = tr.obterConexao();
     String sql = "select * from mediconsulta.Hospital where nome like ?";
     PreparedStatement ps = con.prepareStatement(sql);
     ps.setString(1, "%" + nome + "%");
     ResultSet rs = ps.executeQuery();
     System.out.println("query executada");
     Vector hospitais = new Vector();
     while (rs.next()) {
        HospitalDO l = new HospitalDO();
        l.setIdHospital (rs.getInt("idHospital"));
        l.setNome (rs.getString("nome"));
        l.setEndereco (rs.getString("endereco"));
        l.setNumero (rs.getInt("numero"));
        l.setComplemento (rs.getString("complemento"));
        //soh para nao aparecer "null" caso nao houver complemento
            if (l.getComplemento()==null) l.setComplemento(""); 
        l.setCep (rs.getString("cep"));
        l.setBairro (rs.getString("bairro"));
        l.setCidade (rs.getString("cidade"));
        l.setEstado (rs.getString("estado"));
        l.setProntoSocorro (rs.getString("ProntoSocorro"));
        System.out.println(" got " + l.getNome());
        hospitais.add(l);
     }
     return hospitais;
  } // pesquisarPorNome
  
  
  public Vector pesquisarPorDados(String nome, String endereco, String cep, String cidade, String estado, String prontoSocorro, Transacao tr) throws Exception {
     Connection con = tr.obterConexao();
     String sql = "SELECT * FROM mediconsulta.Hospital WHERE Nome LIKE ? AND Endereco LIKE ? AND CEP LIKE ? AND Cidade LIKE ? AND Estado LIKE ? AND ProntoSocorro LIKE ?";
     PreparedStatement ps = con.prepareStatement(sql);
     ps.setString(1, "%" + nome + "%");
     ps.setString(2, "%" + endereco + "%");
     ps.setString(3, "%" + cep + "%");
     ps.setString(4, "%" + cidade + "%");
     ps.setString(5, "%" + estado + "%");
     ps.setString(6, "%" + prontoSocorro + "%");
     ResultSet rs = ps.executeQuery();
     System.out.println("query executada");
     Vector hospitais = new Vector();
     while (rs.next()) {
        HospitalDO l = new HospitalDO();
        l.setIdHospital (rs.getInt("idHospital"));
        l.setNome (rs.getString("nome"));
        l.setEndereco (rs.getString("endereco"));
        l.setNumero (rs.getInt("numero"));
        l.setComplemento (rs.getString("complemento"));
        //soh para nao aparecer "null" caso nao houver complemento
            if (l.getComplemento() == null) l.setComplemento(""); 
        l.setCep (rs.getString("cep"));
        l.setBairro (rs.getString("bairro"));
        l.setCidade (rs.getString("cidade"));
        l.setEstado (rs.getString("estado"));
        l.setProntoSocorro (rs.getString("ProntoSocorro"));
        System.out.println(" got " + l.getNome());
        hospitais.add(l);
     }
     return hospitais;
  } // pesquisarPorNome
  
  
  public Vector pesquisarPorMedico(int medicoId, Transacao tr) throws Exception {
     Connection con = tr.obterConexao();
     
     String sql = "select * from mediconsulta.Hospital where idHospital in "+
                  "(select Hospital_idHospital from mediconsulta.Medico_has_Hospital where "+
                  "Medico_idMedico=?)";
     PreparedStatement ps = con.prepareStatement(sql);
     ps.setInt(1, medicoId);
     
     ResultSet rs = ps.executeQuery();
     System.out.println("Query executada");
     Vector hospitais = new Vector();
     while (rs.next()) {
        HospitalDO hospital = new HospitalDO();
        hospital.setIdHospital (rs.getInt("idHospital"));
        hospital.setNome (rs.getString("Nome"));

        //System.out.println(" got " + plano.getNome());
        hospitais.add(hospital);
     }
     return hospitais;
  } // pesquisarPorMedico
  
  
  public Vector ListarTodos(Transacao tr) throws Exception {
     Connection con = tr.obterConexao();
     
     String sql = "select * from mediconsulta.Hospital";
     PreparedStatement ps = con.prepareStatement(sql);
     ResultSet rs = ps.executeQuery();
     System.out.println("Query executada");
     Vector hospitais = new Vector();
     while (rs.next()) {
        HospitalDO hospital = new HospitalDO();
        hospital.setIdHospital (rs.getInt("idHospital"));
        hospital.setNome (rs.getString("nome"));
       
        System.out.println(" got " + hospital.getNome());
        hospitais.add(hospital);
     }
     return hospitais;
  } // ListarTodos

} // hospitalData

package data;

import java.sql.*;
import java.util.*;
import utils.Transacao;

public class Tipo_de_exameData {

  public void incluir(Tipo_de_exameDO tipo, Transacao tr) throws Exception {
     Connection con = tr.obterConexao();
     String sql = "insert into `Tipo_de_exame` (idTipo_de_exame, nome) values (?, ?)";
     PreparedStatement ps = con.prepareStatement(sql);
     ps.setInt(1, tipo.getId());
     ps.setString(2, tipo.getNome());
     int result = ps.executeUpdate();
  }

  public void excluir(Tipo_de_exameDO tipo, Transacao tr) throws Exception {
     excluir(tipo.getId(), tr);
  } // excluir

  public void excluir (int idobj, Transacao tr) throws Exception {
  } // excluir 

  public void atualizar(Tipo_de_exameDO tipo, Transacao tr) throws Exception {
     Connection con = tr.obterConexao();
     String sql = "update `Tipo_de_exame` set nome=?, where id=?";
     PreparedStatement ps = con.prepareStatement(sql);
     ps.setString(1, tipo.getNome());
	 ps.setInt(2, tipo.getId());
     int result = ps.executeUpdate();
  } // atualizar

  public Tipo_de_exameDO buscar(int idobj, Transacao tr) throws Exception {
     Connection con = tr.obterConexao();
     String sql = "select * from `Tipo_de_exame` where id=?";
     PreparedStatement ps = con.prepareStatement(sql);
     ps.setInt(1, idobj);
     ResultSet rs = ps.executeQuery();
     rs.next();
     Tipo_de_exameDO tipo = new Tipo_de_exameDO();
     tipo.setId (rs.getInt("id"));
     tipo.setNome (rs.getString("nome"));
     return tipo;
  } // buscar

  public Vector pesquisarPorNome(String nome, Transacao tr) throws Exception {
     Connection con = tr.obterConexao();
     String sql = "select * from `Tipo_de_exame` where nome like ?";
     PreparedStatement ps = con.prepareStatement(sql);
     ps.setString(1, nome);
     ResultSet rs = ps.executeQuery();
     System.out.println("query executada");
     Vector tipos = new Vector();
     while (rs.next()) {
        Tipo_de_exameDO t = new Tipo_de_exameDO();
        t.setId (rs.getInt("id"));
        t.setNome (rs.getString("nome"));
        System.out.println(" got " + t.getNome());
        tipos.add(t);
     }
     return tipos;
  } // pesquisarPorNome

} // Tipo_de_exameData
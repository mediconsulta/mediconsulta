/* 
    Document   : LaboratorioData
    Created on : 02/11
    Author     : Ivan
*/
package data;

import java.sql.*;
import java.util.*;
import utils.Transacao;

public class LaboratorioData {

  public void incluir(LaboratorioDO laboratorio, Transacao tr) throws Exception {
     Connection con = tr.obterConexao();
     String sql = "INSERT INTO `Laboratorio`(`idLaboratorio`, `Tipo_de_exame_idTipo_de_exame`, `Nome`, `Endereco`, `Numero`, `Complemento`, `CEP`, `Bairro`, `Cidade`, `Estado`, `Telefone`, `Email`, `Website`) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
	PreparedStatement ps = con.prepareStatement(sql);
        ps.setInt(1, laboratorio.getId());
        ps.setInt(2, laboratorio.getTipo_de_exame());
	ps.setString(3, laboratorio.getNome());
	ps.setString(4, laboratorio.getEndereco());
	ps.setInt(5, laboratorio.getNumero());
	ps.setString(6, laboratorio.getComplemento());
	ps.setString(7, laboratorio.getCep());
	ps.setString(8, laboratorio.getBairro());
	ps.setString(9, laboratorio.getCidade());
	ps.setString(10, laboratorio.getEstado());
	ps.setString(11, laboratorio.getTelefone());
	ps.setString(12, laboratorio.getEmail());
	ps.setString(13, laboratorio.getWebsite());
     int result = ps.executeUpdate();
  }

  public void excluir(LaboratorioDO laboratorio, Transacao tr) throws Exception {
     excluir(laboratorio.getId(), tr);
  } // excluir

  public void excluir (int idobj, Transacao tr) throws Exception {
  } // excluir 

  public void atualizar(LaboratorioDO laboratorio, Transacao tr) throws Exception {
     Connection con = tr.obterConexao();
     String sql = "UPDATE `Laboratorio` SET `Tipo_de_exame_idTipo_de_exame`=?,`Nome`=?,`Endereco`=?,`Numero`=?,`Complemento`=?,`CEP`=?,`Bairro`=?,`Cidade`=?,`Estado`=?,`Telefone`=?,`Email`=?,`Website`=? WHERE id=?";
	 
     PreparedStatement ps = con.prepareStatement(sql);
     ps.setInt(1, laboratorio.getTipo_de_exame());
	 ps.setString(2, laboratorio.getNome());
	 ps.setString(3, laboratorio.getEndereco());
	 ps.setInt(4, laboratorio.getNumero());
	 ps.setString(5, laboratorio.getComplemento());
	 ps.setString(6, laboratorio.getCep());
	 ps.setString(7, laboratorio.getBairro());
	 ps.setString(8, laboratorio.getCidade());
	 ps.setString(9, laboratorio.getEstado());
	 ps.setString(10, laboratorio.getTelefone());
	 ps.setString(11, laboratorio.getEmail());
	 ps.setString(12, laboratorio.getWebsite());
	 ps.setInt(13, laboratorio.getId());
     int result = ps.executeUpdate();
  } // atualizar

  public LaboratorioDO buscar(int idobj, Transacao tr) throws Exception {
     Connection con = tr.obterConexao();
     String sql = "select * from Laboratorio where  id=?";
     PreparedStatement ps = con.prepareStatement(sql);
     ps.setInt(1, idobj);
     ResultSet rs = ps.executeQuery();
     rs.next();
     LaboratorioDO laboratorio = new LaboratorioDO();
     laboratorio.setId (rs.getInt("id"));
     laboratorio.setNome (rs.getString("nome"));
     laboratorio.setTelefone(rs.getString("telefone"));
     return laboratorio;
  } // buscar

  public Vector pesquisarPorNome(String nome, String endereco, String cep, String cidade, String estado, String tipo, Transacao tr) throws Exception {
     Connection con = tr.obterConexao();
     String sql = "select * from Laboratorio where nome like ? or endereco like ? or cep like ? or cidade like ? or estado like ? or Tipo_de_exame_idTipo_de_exame like ?";
     PreparedStatement ps = con.prepareStatement(sql);
     ps.setString(1, nome);
     ps.setString(2, endereco);
     ps.setString(3, cep);
     ps.setString(4, cidade);
     ps.setString(5, estado);
     ps.setString(6, tipo);
     ResultSet rs = ps.executeQuery();
     System.out.println("query executada");
     Vector laboratorios = new Vector();
     while (rs.next()) {
        LaboratorioDO l = new LaboratorioDO();
        l.setId (rs.getInt("idLaboratorio"));
        l.setNome (rs.getString("nome"));
        l.setTelefone(rs.getString("telefone"));
        l.setEndereco(rs.getString("endereco"));
        l.setWebsite(rs.getString("website"));
        l.setEmail(rs.getString("email"));
        System.out.println(" got " + l.getNome());
        laboratorios.add(l);
     }
     return laboratorios;
  } // pesquisarPorNome

} // LaboratorioData
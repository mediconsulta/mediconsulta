package data;

import java.sql.*;
import java.util.*;
import utils.Transacao;

public class AdministradorData {

  public void incluir(AdministradorDO admin, Transacao tr) throws Exception {
     Connection con = tr.obterConexao();
     java.sql.Date sqlDate = new java.sql.Date(admin.getNascimento().getTime());
     
     String sql = "insert into mediconsulta.Administrador (Usuario_iduser, Nome, CPF, Endereco, Numero, Complemento, Bairro, Cidade, Estado, DataDeNascimento, Telefone) values (?,?,?,?,?,?,?,?,?,?,?)";
     PreparedStatement ps = con.prepareStatement(sql);
     ps.setInt(1, admin.getUserId());
     ps.setString(2, admin.getNome());
     ps.setString(3, admin.getCpf());
     ps.setString(4, admin.getEndereco());
     ps.setInt(5, admin.getNumero());
     ps.setString(6, admin.getComplemento());
     ps.setString(7, admin.getBairro());
     ps.setString(8, admin.getCidade());
     ps.setString(9, admin.getEstado());
     ps.setDate(10, sqlDate);
     ps.setString(11, admin.getTelefone());
     
     int result = ps.executeUpdate();
  }

  public void excluir(AdministradorDO admin, Transacao tr) throws Exception {
     excluir(admin.getId(), tr);
  } // excluir

  public void excluir (int idobj, Transacao tr) throws Exception {
     Connection con = tr.obterConexao();
      
     String sql = "delete * from mediconsulta.Administrador where idAdministrador=?";
     PreparedStatement ps = con.prepareStatement(sql);
     ps.setInt(1, idobj);
     
     int result = ps.executeUpdate();
  } // excluir 

  public void atualizar(AdministradorDO admin, Transacao tr) throws Exception {
     Connection con = tr.obterConexao();
     java.sql.Date sqlDate = new java.sql.Date(admin.getNascimento().getTime());
     
     String sql = "update mediconsulta.Administrador set Nome=?, CPF=?, Endereco=?, Numero=?, Complemento=?, Bairro=?, Cidade=?, Estado=?, DataDeNascimento=?, Telefone=? where idAdministrador=?";
     PreparedStatement ps = con.prepareStatement(sql);
     ps.setString(1, admin.getNome());
     ps.setString(2, admin.getCpf());
     ps.setString(3, admin.getEndereco());
     ps.setInt(4, admin.getNumero());
     ps.setString(5, admin.getComplemento());
     ps.setString(6, admin.getBairro());
     ps.setString(7, admin.getCidade());
     ps.setString(8, admin.getEstado());
     ps.setDate(9, sqlDate);
     ps.setString(10, admin.getTelefone());
     ps.setInt(11, admin.getId());
     
     int result = ps.executeUpdate();
  } // atualizar

  public AdministradorDO buscar(int idobj, Transacao tr) throws Exception {
     Connection con = tr.obterConexao();
     
     String sql = "select * from mediconsulta.Administrador where  idAdministrador=?";
     PreparedStatement ps = con.prepareStatement(sql);
     ps.setInt(1, idobj);
     
     ResultSet rs = ps.executeQuery();
     rs.next();
     AdministradorDO admin = new AdministradorDO();
     admin.setId (rs.getInt("idAdministrador"));
     admin.setUserId (rs.getInt("Usuario_iduser"));
     admin.setNome (rs.getString("Nome"));
     admin.setCpf (rs.getString("CPF"));
     admin.setEndereco (rs.getString("Endereco"));
     admin.setNumero (rs.getInt("Numero"));
     admin.setComplemento (rs.getString("Complemento"));
     admin.setBairro (rs.getString("Bairro"));
     admin.setCidade (rs.getString("Cidade"));
     admin.setEstado (rs.getString("Estado"));
     admin.setNascimento (rs.getDate("DataDeNascimento"));
     admin.setTelefone(rs.getString("Telefone"));
     
     return admin;
  } // buscar
  
  public AdministradorDO buscarPorUserID(int idobj, Transacao tr) throws Exception {
     Connection con = tr.obterConexao();
     
     String sql = "select * from mediconsulta.Administrador where Usuario_iduser = ?";
     PreparedStatement ps = con.prepareStatement(sql);
     ps.setInt(1, idobj);
     
     ResultSet rs = ps.executeQuery();
     rs.next();
     AdministradorDO admin = new AdministradorDO();
     admin.setId (rs.getInt("idAdministrador"));
     admin.setUserId (rs.getInt("Usuario_iduser"));
     admin.setNome (rs.getString("Nome"));
     admin.setCpf (rs.getString("CPF"));
     admin.setEndereco (rs.getString("Endereco"));
     admin.setNumero (rs.getInt("Numero"));
     admin.setComplemento (rs.getString("Complemento"));
     admin.setBairro (rs.getString("Bairro"));
     admin.setCidade (rs.getString("Cidade"));
     admin.setEstado (rs.getString("Estado"));
     admin.setNascimento (rs.getDate("DataDeNascimento"));
     admin.setTelefone(rs.getString("Telefone"));
     
     return admin;
  } // buscar

  public Vector pesquisarPorNome(String nome, Transacao tr) throws Exception {
     Connection con = tr.obterConexao();
     
     String sql = "select * from mediconsulta.Administrador where nome like ?";
     PreparedStatement ps = con.prepareStatement(sql);
     ps.setString(1, nome);
     
     ResultSet rs = ps.executeQuery();
     System.out.println("Query executada");
     Vector admins = new Vector();
     while (rs.next()) {
        AdministradorDO admin = new AdministradorDO();
        admin.setId (rs.getInt("idAdministrador"));
        admin.setUserId (rs.getInt("usuario_iduser_2"));
        admin.setNome (rs.getString("nome"));
        admin.setCpf (rs.getString("CPF"));
        admin.setEndereco (rs.getString("Endereco"));
        admin.setNumero (rs.getInt("Numero"));
        admin.setComplemento (rs.getString("Complemento"));
        admin.setBairro (rs.getString("Bairro"));
        admin.setCidade (rs.getString("Cidade"));
        admin.setEstado (rs.getString("Estado"));
        admin.setNascimento (rs.getDate("DataDeNascimento"));
        admin.setTelefone(rs.getString("Telefone"));
       
        System.out.println(" got " + admin.getNome());
        admins.add(admin);
     }
     return admins;
  } // pesquisarPorNome

} // AdministradorData
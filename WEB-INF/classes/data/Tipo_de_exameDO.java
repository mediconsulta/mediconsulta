package data;

public class Tipo_de_exameDO {
  private int _id;
  private String _nome;

  public int getId() {
     return _id;
  } // getId

  public void setId(int id) {
    _id = id;
  } // setId

  public String getNome() {
    return _nome;
  } // obterNome

  public void setNome(String nome) {
    _nome = nome;
  } // setNome

} // Tipo_de_exameDO
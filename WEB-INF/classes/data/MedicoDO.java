package data;

import java.util.*;

public class MedicoDO {
  private int _idMedico;
  private int _Usuario_iduser;
  private String _Nome;
  private String _CPF;
  private String _CRM;
  private String _Endereco;
  private int _Numero;
  private String _Complemento;
  private String _CEP;
  private String _Bairro;
  private String _Cidade;
  private String _Estado;
  private Date _DataDeNascimento;
  private String _Telefone;

  public int getIdMedico() {
     return _idMedico;
  } // getIdMedico

  public void setIdMedico(int idMedico) {
    _idMedico = idMedico;
  } // setIdMedico

  public int getUsuarioIdUser() {
    return _Usuario_iduser;
  } // obterIdUser2

  public void setUsuarioIdUser2(int usuario_iduser_2) {
    _Usuario_iduser = usuario_iduser_2;
  } // setIdUser2


  public String getNome() {
    return _Nome;
  } // getNome

  public void setNome(String nome) {
    _Nome = nome;
  } // setNome

  public String getCPF() {
     return _CPF;
  } // getCPF

  public void setCPF(String CPF) {
    _CPF = CPF;
  } // setCPF
  
  public String getCRM() {
     return _CRM;
  } // getCRM

  public void setCRM(String CRM) {
    _CRM = CRM;
  } // setCRM
  
  public String getEndereco() {
    return _Endereco;
  } // getEndereco

  public void setEndereco(String Endereco) {
    _Endereco = Endereco;
  } // setEndereco
  
  public int getNumero() {
     return _Numero;
  } // getNumero

  public void setNumero(int Numero) {
    _Numero = Numero;
  } // setNumero
  
  public String getComplemento() {
    return _Complemento;
  } // getComplemento

  public void setComplemento(String Complemento) {
    _Complemento = Complemento;
  } // setComplemento
  
  public String getCEP() {
     return _CEP;
  } // getCEP

  public void setCEP(String CEP) {
    _CEP = CEP;
  } // setCEP
  
  public String getBairro() {
    return _Bairro;
  } // getBairro

  public void setBairro(String Bairro) {
    _Bairro = Bairro;
  } // setBairro
  
  public String getCidade() {
    return _Cidade;
  } // getCidade

  public void setCidade(String Cidade) {
    _Cidade = Cidade;
  } // setCidade
  
  public String getEstado() {
    return _Estado;
  } // getEstado

  public void setEstado(String Estado) {
    _Estado = Estado;
  } // setEstado
  
  public Date getDataDeNascimento() {
    return _DataDeNascimento;
  } // getDataDeNascimento

  public void setDataDeNascimento(Date DataDeNascimento) {
    _DataDeNascimento = DataDeNascimento;
  } // setDataDeNascimento
  
  public String getTelefone() {
     return _Telefone;
  } // getTelefone

  public void setTelefone(String Telefone) {
    _Telefone = Telefone;
  } // setTelefone

} // MedicoDO

package data;

import java.sql.*;
import java.util.*;
import utils.Transacao;

public class ConsultaData {

  public void incluir(ConsultaDO consulta, Transacao tr) throws Exception {
     Connection con = tr.obterConexao();
     java.sql.Timestamp sqlTimestamp = new java.sql.Timestamp(consulta.getData().getTime());
     String sql = "insert into mediconsulta.Consulta (Paciente_idPaciente, Medico_idMedico, DataDaConsulta, Estado, Motivo, Feedback, NotaGeral, NotaPontualidade, Preco, Receita) values (?,?,?,?,?,?,?,?,?,?)";
     PreparedStatement ps = con.prepareStatement(sql);
     ps.setInt(1, consulta.getPacienteId());
     ps.setInt(2, consulta.getMedicoId());
     ps.setTimestamp(3, sqlTimestamp);
     ps.setInt(4, consulta.getEstado());
     ps.setString(5, consulta.getMotivo());
     ps.setString(6, consulta.getFeedback());
     ps.setInt(7, consulta.getNotaGeral());
     ps.setInt(8, consulta.getNotaPontualidade());
     ps.setFloat(9, consulta.getPreco());
     ps.setString(10, consulta.getReceita());
     int result = ps.executeUpdate();
  }

  public void excluir(ConsultaDO consulta, Transacao tr) throws Exception {
     excluir(consulta.getId(), tr);
  } // excluir

  public void excluir (int idobj, Transacao tr) throws Exception {
     Connection con = tr.obterConexao();
      
     String sql = "delete * from mediconsulta.Consulta where idConsulta=?";
     PreparedStatement ps = con.prepareStatement(sql);
     ps.setInt(1, idobj);
     
     int result = ps.executeUpdate();
  } // excluir 

  public void atualizar(ConsultaDO consulta, Transacao tr) throws Exception {
     Connection con = tr.obterConexao();
     java.sql.Timestamp sqlTimestamp = new java.sql.Timestamp(consulta.getData().getTime());
     
     String sql = "update mediconsulta.Consulta set Paciente_idPaciente=?, Medico_idMedico=?, DataDaConsulta=?, Estado=?, Motivo=?, Feedback=?, NotaGeral=?, NotaPontualidade=?, Preco=?, Receita=? where idConsulta=?";
     PreparedStatement ps = con.prepareStatement(sql);
     ps.setInt(1, consulta.getPacienteId());
     ps.setInt(2, consulta.getMedicoId());
     ps.setTimestamp(3, sqlTimestamp);
     ps.setInt(4, consulta.getEstado());
     ps.setString(5, consulta.getMotivo());
     ps.setString(6, consulta.getFeedback());
     ps.setInt(7, consulta.getNotaGeral());
     ps.setInt(8, consulta.getNotaPontualidade());
     ps.setFloat(9, consulta.getPreco());
     ps.setString(10, consulta.getReceita());
     ps.setInt(11, consulta.getId());
     
     int result = ps.executeUpdate();
  } // atualizar

  public ConsultaDO buscar(int idobj, Transacao tr) throws Exception {
     Connection con = tr.obterConexao();
     
     String sql = "select * from mediconsulta.Consulta where  idConsulta=?";
     PreparedStatement ps = con.prepareStatement(sql);
     ps.setInt(1, idobj);
     
     ResultSet rs = ps.executeQuery();
     rs.next();
     ConsultaDO consulta = new ConsultaDO();
     consulta.setId (rs.getInt("idConsulta"));
     consulta.setPacienteId (rs.getInt("Paciente_idPaciente"));
     consulta.setMedicoId (rs.getInt("Medico_idMedico"));
     consulta.setData (rs.getDate("DataDaConsulta"));
     consulta.setEstado (rs.getInt("Estado"));
     consulta.setMotivo (rs.getString("Motivo"));
     consulta.setFeedback (rs.getString("Feedback"));
     consulta.setNotaGeral (rs.getInt("NotaGeral"));
     consulta.setNotaPontualidade (rs.getInt("NotaPontualidade"));
     consulta.setPreco (rs.getFloat("Preco"));
     consulta.setReceita (rs.getString("Receita"));
     
     return consulta;
  } // buscar

  public Vector pesquisarPorMedico(int medicoId, Transacao tr) throws Exception {
     Connection con = tr.obterConexao();
     
     String sql = "select * from mediconsulta.Consulta where Medico_idMedico=?";
     PreparedStatement ps = con.prepareStatement(sql);
     ps.setInt(1, medicoId);
     
     ResultSet rs = ps.executeQuery();
     System.out.println("Query executada");
     Vector consultas = new Vector();
     while (rs.next()) {
        ConsultaDO consulta = new ConsultaDO();
        consulta.setId (rs.getInt("idConsulta"));
        consulta.setPacienteId (rs.getInt("Paciente_idPaciente"));
        consulta.setMedicoId (rs.getInt("Medico_idMedico"));
        consulta.setData (rs.getTimestamp("DataDaConsulta"));
        consulta.setEstado (rs.getInt("Estado"));
        consulta.setMotivo (rs.getString("Motivo"));
        consulta.setFeedback (rs.getString("Feedback"));
        consulta.setNotaGeral (rs.getInt("NotaGeral"));
        consulta.setNotaPontualidade (rs.getInt("NotaPontualidade"));
        consulta.setPreco (rs.getFloat("Preco"));
        consulta.setReceita (rs.getString("Receita"));
       
        //System.out.println(" got " + consulta.getNome());
        consultas.add(consulta);
     }
     return consultas;
  } // pesquisarPorMedico
  
  public Vector pesquisarPorPaciente(int pacienteId, Transacao tr) throws Exception {
     Connection con = tr.obterConexao();
     
     String sql = "select * from mediconsulta.Consulta where Paciente_idPaciente=?";
     PreparedStatement ps = con.prepareStatement(sql);
     ps.setInt(1, pacienteId);
     
     ResultSet rs = ps.executeQuery();
     System.out.println("Query executada");
     Vector consultas = new Vector();
     while (rs.next()) {
        ConsultaDO consulta = new ConsultaDO();
        consulta.setId (rs.getInt("idConsulta"));
        consulta.setPacienteId (rs.getInt("Paciente_idPaciente"));
        consulta.setMedicoId (rs.getInt("Medico_idMedico"));
        consulta.setData (rs.getTimestamp("DataDaConsulta"));
        consulta.setEstado (rs.getInt("Estado"));
        consulta.setMotivo (rs.getString("Motivo"));
        consulta.setFeedback (rs.getString("Feedback"));
        consulta.setNotaGeral (rs.getInt("NotaGeral"));
        consulta.setNotaPontualidade (rs.getInt("NotaPontualidade"));
        consulta.setPreco (rs.getFloat("Preco"));
        consulta.setReceita (rs.getString("Receita"));
       
        //System.out.println(" got " + consulta.getNome());
        consultas.add(consulta);
     }
     return consultas;
  } // pesquisarPorPaciente
  
  public Vector pesquisarPorPM(int medicoId, int pacienteId, Transacao tr) throws Exception {
     Connection con = tr.obterConexao();
     
     String sql = "select * from mediconsulta.Consulta where Medico_idMedico=? and Paciente_idPaciente=?";
     PreparedStatement ps = con.prepareStatement(sql);
     ps.setInt(1, medicoId);
     ps.setInt(2, pacienteId);
     
     ResultSet rs = ps.executeQuery();
     System.out.println("Query executada");
     Vector consultas = new Vector();
     while (rs.next()) {
        ConsultaDO consulta = new ConsultaDO();
        consulta.setId (rs.getInt("idConsulta"));
        consulta.setPacienteId (rs.getInt("Paciente_idPaciente"));
        consulta.setMedicoId (rs.getInt("Medico_idMedico"));
        consulta.setData (rs.getTimestamp("DataDaConsulta"));
        consulta.setEstado (rs.getInt("Estado"));
        consulta.setMotivo (rs.getString("Motivo"));
        consulta.setFeedback (rs.getString("Feedback"));
        consulta.setNotaGeral (rs.getInt("NotaGeral"));
        consulta.setNotaPontualidade (rs.getInt("NotaPontualidade"));
        consulta.setPreco (rs.getFloat("Preco"));
        consulta.setReceita (rs.getString("Receita"));
       
        //System.out.println(" got " + consulta.getNome());
        consultas.add(consulta);
     }
     return consultas;
  } // pesquisarPorMedicoePaciente

} // ConsultaData

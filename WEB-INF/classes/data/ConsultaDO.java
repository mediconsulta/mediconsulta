package data;

import java.util.*;

public class ConsultaDO {
  private int _id, _pacienteId, _medicoId;
  private Date _data;
  /* 1 - Marcado
     2 - Confirmado
     3 - Cancelado
  */
  private int _estado;
  private String _motivo, _feedback;
  private int _notaGeral, _notaPontualidade;
  private float _preco;
  private String _receita;
  

  public int getId() {
     return _id;
  } // getId

  public void setId(int id) {
    _id = id;
  } // setId
  
   public int getPacienteId() {
     return _pacienteId;
  } // getPacienteId

  public void setPacienteId(int pacienteId) {
    _pacienteId = pacienteId;
  } // setPacienteId

  public int getMedicoId() {
     return _medicoId;
  } // getMedicoId

  public void setMedicoId(int medicoId) {
    _medicoId = medicoId;
  } // setMedicoId
  
  public Date getData() {
      return _data;
  } // getData
  
  public void setData(Date data) {
      _data = data;
  } // setData
  
  public int getEstado() {
    return _estado;
  } // getEstado

  public void setEstado(int estado) {
    _estado = estado;
  } // setEstado
  
  public String getMotivo() {
    return _motivo;
  } // getMotivo

  public void setMotivo(String motivo) {
    _motivo = motivo;
  } // setMotivo
  
  public String getFeedback() {
    return _feedback;
  } // getFeedback

  public void setFeedback(String feedback) {
    _feedback = feedback;
  } // setFeedback
  
  public int getNotaGeral() {
    return _notaGeral;
  } // getNotaGeral
  
  public void setNotaGeral(int notaGeral) {
    _notaGeral = notaGeral;
  } // setNotaGeral
  
  public int getNotaPontualidade() {
    return _notaPontualidade;
  } // getNotaPontualidade
  
  public void setNotaPontualidade(int notaPontualidade) {
    _notaPontualidade = notaPontualidade;
  } // setNotaPontualidade
  
  public float getPreco() {
    return _preco;
  } // getPreco
  
  public void setPreco(float preco) {
    _preco = preco;
  } // setPreco
  
  public String getReceita() {
    return _receita;
  } // getReceita
  
  public void setReceita(String receita) {
    _receita = receita;
  } // setReceita
  
} // ConsultaDO
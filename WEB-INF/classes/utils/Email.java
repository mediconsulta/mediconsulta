package utils;

import java.util.Properties;
 
import javax.mail.Message;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;




public class Email {
    private Session session;
    private final String adminEmail;
    
    // Metodo construtor:
    // inicializa as variaveis da classe
    public Email() {
        // email para onde as mensagens do formulario de contato sao enviadas
        adminEmail = "mediconsultasis@gmail.com";
        
        Properties props = new Properties();
        // dados do servidor SMTP do gmail
        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.starttls.enable", "true");
        props.put("mail.smtp.host", "smtp.gmail.com");
        props.put("mail.smtp.port", "587");
        
        this.session = Session.getInstance(props,
            new javax.mail.Authenticator() {
                protected PasswordAuthentication getPasswordAuthentication() {                    
                    // define usuario e senha da conta de email utilizada para envio
                    return new PasswordAuthentication("mediconsultasis@gmail.com", "ConsultaMedSIS");
                }
            });
    }
    
    // Metodo generico de envio de mensagens
    public boolean sendMessage(String toEmail, String subject, String text) {
        try {
            Message message = new MimeMessage(this.session);
            message.setFrom(new InternetAddress("mediconsulta@gmail.com", "MediConsulta"));
            message.setRecipients(Message.RecipientType.TO, 
                                    InternetAddress.parse(toEmail));
            message.setSubject(subject);
            message.setText(text);
            Transport.send(message);
            System.out.println("E-mail enviado");
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("E-mail error");
            return false;
        }
        return true;
    }
    
    // Metodo para envio de mensagens para o administrador (com email na 
    // variavel adminEmail) a partir do formulario de contato do site
    public boolean sendMessageFromVisitor(String name, String email, 
                        String phone, String subject, String message) {
        String msgSubject = "[MediConsulta/Contato] " + subject;
        String msgMessage = "Mensagem recebida pelo formulário de contato";
        msgMessage += "\n\nNome: " + name;
        msgMessage += "\nE-mail: " + email;
        msgMessage += "\nTelefone: " + phone;
        msgMessage += "\nAssunto: " + subject;
        msgMessage += "\n\nMensagem:\n";
        msgMessage += message;
        
        return this.sendMessage(this.adminEmail, msgSubject, msgMessage);
    }

} // Transacao
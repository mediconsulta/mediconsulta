package transacoes;

import utils.*;
import data.*;
import java.util.*;

public class Tipo_de_exame {

  public boolean incluir (Tipo_de_exameDO tipo) throws Exception{

     // validacao das regras de negocio
     if ( (isEmpty(tipo.getNome())) ) {
       return false;
     }

     // efetuando a transacao
     Transacao tr = new Transacao();
     try {

       tr.begin();
         Tipo_de_exameData tdata = new Tipo_de_exameData();
         tdata.incluir(tipo, tr);
       tr.commit();
       return true;
       
     } catch(Exception e) {
         tr.rollback();
         System.out.println("erro ao incluir " + tipo.getNome());
         e.printStackTrace();
     }
     return false;
  } // incluir

  public boolean atualizar(Tipo_de_exameDO tipo) throws Exception {
     Transacao tr = new Transacao();
	 try{
	   // inserir validacoes de regras de negocio
	   tr.begin();
  	     Tipo_de_exameData tdata = new Tipo_de_exameData();
	     tdata.atualizar(tipo, tr);
	   tr.commit();
	   return true;
	 } catch (Exception e) {
	   tr.rollback();
	   System.out.println("erro ao atualizar" + tipo.getNome());
	   e.printStackTrace();
	 }
	 return false;
  } // atualizar

  public Tipo_de_exameDO buscar(int idobj) throws Exception{
     Transacao tr = new Transacao();
	 try{
	   tr.beginReadOnly();
  	     Tipo_de_exameData tdata = new Tipo_de_exameData();
	     Tipo_de_exameDO t = tdata.buscar(idobj, tr);
	   tr.commit();
	   return t;
	 } catch (Exception e) {
	   tr.rollback();
	   System.out.println("erro ao buscar" + idobj);
	   e.printStackTrace();
	 }
	 return null;
  } // buscar

  public Vector pesquisar(String nome) {
     if ( isEmpty(nome) )
        return null;

     Transacao tr = new Transacao();
     try {
	     tr.beginReadOnly();
           Tipo_de_exameData tdata = new Tipo_de_exameData();
           Vector v = tdata.pesquisarPorNome(nome, tr);
		 tr.commit();
		 return v;
     } catch(Exception e) {
         System.out.println("erro ao pesquisar " + nome);
         e.printStackTrace();
     }
     return null;
  } // pesquisar

  private boolean isEmpty(String s) {
     if (null == s)
       return true;
     if (s.length() == 0)
       return true;
     return false;
  }

  public static void main(String[] args) {
      Tipo_de_exame t = new Tipo_de_exame();
      Tipo_de_exameDO tipo = new Tipo_de_exameDO();
      try {
	    tipo = t.buscar(6);
		System.out.println(tipo.getNome());
      } catch(Exception e) {
          e.printStackTrace();
      }
  } // main
} // Tipo_de_exame
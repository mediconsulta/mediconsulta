package transacoes;

import utils.*;
import data.*;
import java.util.*;

public class Consulta {

  public boolean incluir (ConsultaDO consulta) throws Exception{

     // validacao das regras de negocio
     if ( (consulta.getPacienteId() <= 0) || (consulta.getMedicoId() <= 0) || (consulta.getData()==null) || (consulta.getEstado() <= 0) || (consulta.getPreco() < 0.0)) {
         return false;       
     }

     // efetuando a transacao
     Transacao tr = new Transacao();
     try {

       tr.begin();
         ConsultaData cdata = new ConsultaData();
         cdata.incluir(consulta, tr);
       tr.commit();
       return true;
       
     } catch(Exception e) {
         tr.rollback();
         System.out.println("erro ao incluir " + consulta.getId());
         e.printStackTrace();
     }
     return false;
  } // incluir
  
  public boolean atualizar(ConsultaDO consulta) throws Exception {
     Transacao tr = new Transacao();
	 try{
	   // inserir validacoes de regras de negocio
	   tr.begin();
  	     ConsultaData adata = new ConsultaData();
	     adata.atualizar(consulta, tr);
	   tr.commit();
	   return true;
	 } catch (Exception e) {
	   tr.rollback();
	   System.out.println("erro ao atualizar" + consulta.getId());
	   e.printStackTrace();
	 }
	 return false;
  } // atualizar

  public ConsultaDO buscar(int idobj) throws Exception{
     Transacao tr = new Transacao();
	 try{
            tr.beginReadOnly();
            ConsultaData cdata = new ConsultaData();
            ConsultaDO l = cdata.buscar(idobj, tr);
            tr.commit();
            return l;
	 } catch (Exception e) {
            tr.rollback();
            System.out.println("erro ao buscar" + idobj);
            e.printStackTrace();
	 }
	 return null;
  } // buscar
  
  public Vector pesquisar(int id, int tipo) {
     if ( id<=0 )
        return null;

     Transacao tr = new Transacao();
     try {
        tr.beginReadOnly();
        ConsultaData cdata = new ConsultaData();
        Vector v, aux = new Vector();
        
        if (tipo == 1) {    // Paciente
            v = cdata.pesquisarPorPaciente(id, tr);
            tr.commit();        
        }
        else if (tipo == 2) {
            v = cdata.pesquisarPorMedico(id, tr);
            tr.commit();
        }
        else {
            return null;
        }
        
        Date curDate = new Date();
        boolean flag = true;
        for (int i = 0; i<v.size(); i++) {
            ConsultaDO consulta = (ConsultaDO)v.get(i);

            if (consulta.getEstado() != 1) // Ja foi confirmado/cancelado
                flag = false;
            if (curDate.compareTo(consulta.getData()) > 0) // Ja passou
                flag = false;
            
            if (flag == true) 
                aux.add(consulta);
            
            flag = true;
        }
        
        v = aux;
        return v;
     } catch(Exception e) {
        System.out.println("erro ao pesquisar " + id);
        e.printStackTrace();
     }
     return null;
  } // pesquisar
  
  public Vector pesquisar2(int id, int tipo) {
     if ( id<=0 )
        return null;

     Transacao tr = new Transacao();
     try {
        tr.beginReadOnly();
        ConsultaData cdata = new ConsultaData();
        Vector v, aux = new Vector();
        
        if (tipo == 1) {    // Paciente
            v = cdata.pesquisarPorPaciente(id, tr);
            tr.commit();        
        }
        else if (tipo == 2) {
            v = cdata.pesquisarPorMedico(id, tr);
            tr.commit();
        }
        else {
            return null;
        }
        
        Date curDate = new Date();
        boolean flag = true;
        for (int i = 0; i<v.size(); i++) {
            ConsultaDO consulta = (ConsultaDO)v.get(i);

            if (consulta.getEstado() == 3) // Consulta cancelada
                flag = false;
            if (curDate.compareTo(consulta.getData()) > 0) // Ja passou
                flag = false;
            
            if (flag == true) 
                aux.add(consulta);
            
            flag = true;
        }
        
        v = aux;
        return v;
     } catch(Exception e) {
        System.out.println("erro ao pesquisar " + id);
        e.printStackTrace();
     }
     return null;
  } // pesquisar
  
   public Vector pesquisar3(int id, int tipo) {
     if ( id<=0 )
        return null;

     Transacao tr = new Transacao();
     try {
        tr.beginReadOnly();
        ConsultaData cdata = new ConsultaData();
        Vector v, aux = new Vector();
        
        if (tipo == 1) {    // Paciente
            v = cdata.pesquisarPorPaciente(id, tr);
            tr.commit();        
        }
        else if (tipo == 2) {
            v = cdata.pesquisarPorMedico(id, tr);
            tr.commit();
        }
        else {
            return null;
        }
        
        return v;
     } catch(Exception e) {
        System.out.println("erro ao pesquisar " + id);
        e.printStackTrace();
     }
     return null;
  } // pesquisar
  
   public Vector pesquisar4(int id, int tipo) {
     if ( id<=0 )
        return null;

     Transacao tr = new Transacao();
     try {
        tr.beginReadOnly();
        ConsultaData cdata = new ConsultaData();
        Vector v, aux = new Vector();
        
        if (tipo == 1) {    // Paciente
            v = cdata.pesquisarPorPaciente(id, tr);
            tr.commit();        
        }
        else if (tipo == 2) {
            v = cdata.pesquisarPorMedico(id, tr);
            tr.commit();
        }
        else {
            return null;
        }
        
        Date curDate = new Date();
        boolean flag = true;
        for (int i = 0; i<v.size(); i++) {
            ConsultaDO consulta = (ConsultaDO)v.get(i);

            if (consulta.getEstado() != 2) // Consulta cancelada
                flag = false;
            if (curDate.compareTo(consulta.getData()) < 0) // Nao passou
                flag = false;
            
            if (flag == true) 
                aux.add(consulta);
            
            flag = true;
        }
        
        v = aux;
        return v;
     } catch(Exception e) {
        System.out.println("erro ao pesquisar " + id);
        e.printStackTrace();
     }
     return null;
  } // pesquisar
   
  public Vector checkPM(int medicoId, int pacienteId) {
     if ( medicoId<=0 || pacienteId<=0 )
        return null;

     Transacao tr = new Transacao();
     try {
        tr.beginReadOnly();
        ConsultaData cdata = new ConsultaData();
        Vector v, aux = new Vector();
        
        v = cdata.pesquisarPorPM(medicoId, pacienteId, tr);
        tr.commit();
        System.out.println(v.size());
        return v;
     } catch(Exception e) {
        System.out.println("erro ao pesquisar " + medicoId);
        e.printStackTrace();
     }
     return null;
  } // pesquisar
}

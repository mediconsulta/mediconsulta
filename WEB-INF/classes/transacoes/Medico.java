/*Autor: Lucas Aveiro*/
package transacoes;

import utils.*;
import data.*;
import java.sql.Timestamp;
import java.util.*;

public class Medico {

  public boolean incluir (MedicoDO medic) throws Exception{

     // validacao das regras de negocio
     if ( (medic.getUsuarioIdUser()<0) || (isEmpty(medic.getNome())) || (isEmpty(medic.getCPF())) || (isEmpty(medic.getCRM())) || (isEmpty(medic.getEndereco())) || (medic.getNumero()<=0) || (isEmpty(medic.getCEP())) || (isEmpty(medic.getCidade())) || (isEmpty(medic.getEndereco())) || (isEmpty(medic.getDataDeNascimento())) || (isEmpty(medic.getTelefone())) ) {
         return false;
     }

     // efetuando a transacao
     Transacao tr = new Transacao();
     try {

       tr.begin();
         MedicoData mdata = new MedicoData();
         mdata.incluir(medic, tr);
       tr.commit();
       
       return true;
       
     } catch(Exception e) {
         tr.rollback();
         System.out.println("erro ao incluir " + medic.getNome());
         e.printStackTrace();
     }
     return false;
  } // incluir
  
  public boolean remover (MedicoDO medico) throws Exception{

         // validacao das regras de negocio
         if ( (medico.getIdMedico()<0)|| (isEmpty(medico.getNome())) || ( isEmpty(medico.getCPF())) || (isEmpty(medico.getCRM())) || isEmpty(medico.getTelefone()) || medico.getDataDeNascimento()==null) {
            return false;
         }

         // efetuando a transacao
         Transacao tr = new Transacao();
         try {
           tr.begin();
             MedicoData pdata = new MedicoData();
             pdata.excluir(medico, tr);
           tr.commit();
           return true;

         } catch(Exception e) {
             tr.rollback();
             System.out.println("erro ao excluir " + medico.getNome());
             e.printStackTrace();
         }
         return false;
      } // Remover

  public boolean atualizar(MedicoDO medic) throws Exception {
     Transacao tr = new Transacao();
	 try{
	   // inserir validacoes de regras de negocio
	   tr.begin();
  	     MedicoData mdata = new MedicoData();
	     mdata.atualizar(medic, tr);
	   tr.commit();
	   return true;
	 } catch (Exception e) {
	   tr.rollback();
	   System.out.println("erro ao atualizar" + medic.getNome());
	   e.printStackTrace();
	 }
	 return false;
  } // atualizar

  public MedicoDO buscar(int idobj) throws Exception{
     Transacao tr = new Transacao();
	 try{
	   tr.beginReadOnly();
  	     MedicoData mdata = new MedicoData();
	     MedicoDO m = mdata.buscar(idobj, tr);
	   tr.commit();
	   return m;
	 } catch (Exception e) {
	   tr.rollback();
	   System.out.println("erro ao buscar" + idobj);
	   e.printStackTrace();
	 }
	 return null;
  } // buscar
  
    public MedicoDO buscarPorUserID(int iduser) throws Exception{
     Transacao tr = new Transacao();
	 try{
	   tr.beginReadOnly();
  	     MedicoData mdata = new MedicoData();
	     MedicoDO m = mdata.buscarPorUserID(iduser, tr);
	   tr.commit();
	   return m;
	 } catch (Exception e) {
	   tr.rollback();
	   System.out.println("erro ao buscar" + iduser);
	   e.printStackTrace();
	 }
	 return null;
  } // buscar

  public Vector pesquisarN(String nome) {
     if ( isEmpty(nome) )
        return null;

     Transacao tr = new Transacao();
     try {
	     tr.beginReadOnly();
           MedicoData mdata = new MedicoData();
           Vector v = mdata.pesquisarPorNome(nome, tr);
		 tr.commit();
		 return v;
     } catch(Exception e) {
         System.out.println("erro ao pesquisar " + nome);
         e.printStackTrace();
     }
     return null;
  } // pesquisar nome
  
  public Vector pesquisarR(String rua) {
     if ( isEmpty(rua) )
        return null;

     Transacao tr = new Transacao();
     try {
	     tr.beginReadOnly();
           MedicoData mdata = new MedicoData();
           Vector v = mdata.pesquisarPorRua(rua, tr);
		 tr.commit();
		 return v;
     } catch(Exception e) {
         System.out.println("erro ao pesquisar " + rua);
         e.printStackTrace();
     }
     return null;
  } // pesquisar rua
  
  public Vector pesquisarC(String CEP) {
     if ( isEmpty(CEP) )
        return null;

     Transacao tr = new Transacao();
     try {
	     tr.beginReadOnly();
           MedicoData mdata = new MedicoData();
           Vector v = mdata.pesquisarPorCEP(CEP, tr);
		 tr.commit();
		 return v;
     } catch(Exception e) {
         System.out.println("erro ao pesquisar " + CEP);
         e.printStackTrace();
     }
     return null;
  } // pesquisar CEP
  
  public Vector pesquisarCRM(String CRM) {
     if ( isEmpty(CRM) )
        return null;

     Transacao tr = new Transacao();
     try {
	     tr.beginReadOnly();
           MedicoData mdata = new MedicoData();
           Vector v = mdata.pesquisarPorCRM(CRM, tr);
		 tr.commit();
		 return v;
     } catch(Exception e) {
         System.out.println("erro ao pesquisar " + CRM);
         e.printStackTrace();
     }
     return null;
  } // pesquisar CRM
  
  public Vector pesquisarCPF(String CPF) {
     if ( isEmpty(CPF) )
        return null;

     Transacao tr = new Transacao();
     try {
	     tr.beginReadOnly();
           MedicoData mdata = new MedicoData();
           Vector v = mdata.pesquisarPorCPF(CPF, tr);
		 tr.commit();
		 return v;
     } catch(Exception e) {
         System.out.println("erro ao pesquisar " + CPF);
         e.printStackTrace();
     }
     return null;
  } // pesquisar CPF
  
  public Vector pesquisarCi(String Cidade) {
     if ( isEmpty(Cidade) )
        return null;

     Transacao tr = new Transacao();
     try {
	     tr.beginReadOnly();
           MedicoData mdata = new MedicoData();
           Vector v = mdata.pesquisarPorCidade(Cidade, tr);
		 tr.commit();
		 return v;
     } catch(Exception e) {
         System.out.println("erro ao pesquisar " + Cidade);
         e.printStackTrace();
     }
     return null;
  } // pesquisar Cidade

  public Vector pesquisarE(String Estado) {
     if ( isEmpty(Estado) )
        return null;

     Transacao tr = new Transacao();
     try {
	     tr.beginReadOnly();
           MedicoData mdata = new MedicoData();
           Vector v = mdata.pesquisarPorEstado(Estado, tr);
		 tr.commit();
		 return v;
     } catch(Exception e) {
         System.out.println("erro ao pesquisar " + Estado);
         e.printStackTrace();
     }
     return null;
  } // pesquisar Estado
  
  public Vector pesquisarP(int planoId) {
     if ( planoId<=0 )
        return null;

     Transacao tr = new Transacao();
     try {
         tr.beginReadOnly();
         MedicoData mdata = new MedicoData();
         Vector v = mdata.pesquisarPorPlano(planoId, tr);
         tr.commit();
         return v;
     } catch(Exception e) {
         System.out.println("erro ao pesquisar " + planoId);
         e.printStackTrace();
     }
     return null;
  } // pesquisar Plano

    public Vector pesquisarHospital(int hospitalId) {
     if ( hospitalId<=0 )
        return null;

     Transacao tr = new Transacao();
     try {
         tr.beginReadOnly();
         MedicoData mdata = new MedicoData();
         Vector v = mdata.pesquisarPorPlano(hospitalId, tr);
         tr.commit();
         return v;
     } catch(Exception e) {
         System.out.println("erro ao pesquisar " + hospitalId);
         e.printStackTrace();
     }
     return null;
  } // pesquisar Hospital

  
  public Vector pesquisarEspecialidade(int especialidadeId) {
     if ( especialidadeId <=0 )
        return null;

     Transacao tr = new Transacao();
     try {
         tr.beginReadOnly();
         MedicoData mdata = new MedicoData();
         Vector v = mdata.pesquisarPorEspecialidade(especialidadeId, tr);
         tr.commit();
         return v;
     } catch(Exception e) {
         System.out.println("erro ao pesquisar " + especialidadeId);
         e.printStackTrace();
     }
     return null;
  } // pesquisar Especialidade
  
  public Vector pesquisarMedicEspec(int medicoId) {
     if ( medicoId <=0 )
        return null;

     Transacao tr = new Transacao();
     try {
         tr.beginReadOnly();
         MedicoData mdata = new MedicoData();
         Vector v = mdata.pesquisarEspecialidades(medicoId, tr);
         tr.commit();
         return v;
     } catch(Exception e) {
         System.out.println("erro ao pesquisar " + medicoId);
         e.printStackTrace();
     }
     return null;
  } // pesquisar Especialidades
  
  public Vector pesquisarDisponibilidade(Timestamp data) {

     Transacao tr = new Transacao();
     try {
         tr.beginReadOnly();
         MedicoData mdata = new MedicoData();
         Vector v = mdata.pesquisarPorDisponibilidade(data, tr);
         tr.commit();
         return v;
     } catch(Exception e) {
         System.out.println("erro ao pesquisar " + data);
         e.printStackTrace();
     }
     return null;
  } // pesquisar Especialidade
  
  private boolean isEmpty(String s) {
     if (null == s)
       return true;
     if (s.length() == 0)
       return true;
     return false;
  }
  
  private boolean isEmpty(Date d) {
     if (null == d){
       return true;
     }
     return false;
  }
  
  public boolean incluirE (MedicoDO medic, EspecialidadeDO espec) throws Exception{

     // efetuando a transacao
     Transacao tr = new Transacao();
     if (espec == null){return true;}
     try {

       tr.begin();
         MedicoData mdata = new MedicoData();
         mdata.incluirEspec(medic, espec, tr);
       tr.commit();
       return true;
       
     } catch(Exception e) {
         tr.rollback();
         System.out.println("erro ao incluir " + espec.getNome());
         e.printStackTrace();
     }
     return false;
  } // incluir especialidade
  
  public boolean incluirH (MedicoDO medic, HospitalDO hospital) throws Exception{

     // efetuando a transacao
     Transacao tr = new Transacao();
     if (hospital == null){return true;}
     try {

       tr.begin();
         MedicoData mdata = new MedicoData();
         mdata.incluirHospital(medic, hospital, tr);
       tr.commit();
       return true;
       
     } catch(Exception e) {
         tr.rollback();
         System.out.println("erro ao incluir " + hospital.getNome());
         e.printStackTrace();
     }
     return false;
  } // incluir hospital

  public static void main(String[] args) {
      Medico m = new Medico();
      MedicoDO medic = new MedicoDO();
      try {
	    medic = m.buscar(6);
		System.out.println(medic.getNome());
      } catch(Exception e) {
          e.printStackTrace();
      }
  } // main
} // Medico

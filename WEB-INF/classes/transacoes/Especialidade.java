package transacoes;

import utils.*;
import data.*;
import java.util.*;

public class Especialidade {

  public boolean incluir (EspecialidadeDO especialidade) throws Exception{

     // validacao das regras de negocio
     if ( (isEmpty(especialidade.getNome())) ) {
       return false;
     }

     // efetuando a transacao
     Transacao tr = new Transacao();
     try {

       tr.begin();
         EspecialidadeData adata = new EspecialidadeData();
         adata.incluir(especialidade, tr);
       tr.commit();
       return true;
       
     } catch(Exception e) {
         tr.rollback();
         System.out.println("erro ao incluir " + especialidade.getNome());
         e.printStackTrace();
     }
     return false;
  } // incluir

  public boolean atualizar(EspecialidadeDO especialidade) throws Exception {
     Transacao tr = new Transacao();
	 try{
	   // inserir validacoes de regras de negocio
	   tr.begin();
  	     EspecialidadeData adata = new EspecialidadeData();
	     adata.atualizar(especialidade, tr);
	   tr.commit();
	   return true;
	 } catch (Exception e) {
	   tr.rollback();
	   System.out.println("erro ao atualizar" + especialidade.getNome());
	   e.printStackTrace();
	 }
	 return false;
  } // atualizar

  public EspecialidadeDO buscar(int idobj) throws Exception{
     Transacao tr = new Transacao();
     if (idobj == -1){return null;}
	 try{
            tr.beginReadOnly();
            EspecialidadeData adata = new EspecialidadeData();
            EspecialidadeDO l = adata.buscar(idobj, tr);
            tr.commit();
            return l;
	 } catch (Exception e) {
            tr.rollback();
            System.out.println("erro ao buscar" + idobj);
            e.printStackTrace();
	 }
	 return null;
  } // buscar

  public Vector pesquisarNome(String nome) {
     if ( isEmpty(nome) )
        return null;

     Transacao tr = new Transacao();
     try {
         tr.beginReadOnly();
         EspecialidadeData adata = new EspecialidadeData();
         Vector v = adata.pesquisarPorNome(nome, tr);
         tr.commit();
         return v;
     } catch(Exception e) {
         System.out.println("erro ao pesquisar " + nome);
         e.printStackTrace();
     }
     return null;
  } // pesquisar
  
  public Vector pesquisarPorMedico(int medicoId) {
     if ( medicoId<=0 )
        return null;

     Transacao tr = new Transacao();
     try {
         tr.beginReadOnly();
         EspecialidadeData adata = new EspecialidadeData();
         Vector v = adata.pesquisarPorMedico(medicoId, tr);
         tr.commit();
         return v;
     } catch(Exception e) {
         System.out.println("erro ao pesquisar " + medicoId);
         e.printStackTrace();
     }
     return null;
  } // pesquisar
  
  public Vector ListarTodos() {

     Transacao tr = new Transacao();
     try {
         tr.beginReadOnly();
         EspecialidadeData adata = new EspecialidadeData();
         Vector v = adata.ListarTodos(tr);
         tr.commit();
         return v;
     } catch(Exception e) {
         System.out.println("erro ao listar");
         e.printStackTrace();
     }
     return null;
  } // Listar

  private boolean isEmpty(String s) {
     if (null == s)
       return true;
     if (s.length() == 0)
       return true;
     return false;
  }

} // Especialidade
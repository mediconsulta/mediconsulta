/* 
    Document   : Laboratorio
    Created on : 02/11
    Author     : Ivan
*/
package transacoes;

import utils.*;
import data.*;
import java.util.*;

public class Laboratorio {

  public boolean incluir (LaboratorioDO laboratorio) throws Exception{

     // validacao das regras de negocio
     if ( (isEmpty(laboratorio.getTelefone())) || (isEmpty(laboratorio.getEstado())) || (isEmpty(laboratorio.getCidade())) || (isEmpty(laboratorio.getCep())) || ((laboratorio.getNumero())<=0) || (isEmpty(laboratorio.getEndereco())) || (isEmpty(laboratorio.getNome())) ) {
       return false;
     }

     // efetuando a transacao
     Transacao tr = new Transacao();
     try {

       tr.begin();
         LaboratorioData ldata = new LaboratorioData();
         ldata.incluir(laboratorio, tr);
       tr.commit();
       return true;
       
     } catch(Exception e) {
         tr.rollback();
         System.out.println("erro ao incluir " + laboratorio.getNome());
         e.printStackTrace();
     }
     return false;
  } // incluir

  public boolean atualizar(LaboratorioDO laboratorio) throws Exception {
     Transacao tr = new Transacao();
	 try{
	   // inserir validacoes de regras de negocio
	   tr.begin();
  	     LaboratorioData ldata = new LaboratorioData();
	     ldata.atualizar(laboratorio, tr);
	   tr.commit();
	   return true;
	 } catch (Exception e) {
	   tr.rollback();
	   System.out.println("erro ao atualizar" + laboratorio.getNome());
	   e.printStackTrace();
	 }
	 return false;
  } // atualizar

  public LaboratorioDO buscar(int idobj) throws Exception{
     Transacao tr = new Transacao();
	 try{
	   tr.beginReadOnly();
  	     LaboratorioData ldata = new LaboratorioData();
	     LaboratorioDO l = ldata.buscar(idobj, tr);
	   tr.commit();
	   return l;
	 } catch (Exception e) {
	   tr.rollback();
	   System.out.println("erro ao buscar" + idobj);
	   e.printStackTrace();
	 }
	 return null;
  } // buscar

  public Vector pesquisar(String nome, String endereco, String cep, String cidade, String estado, String tipo) {
     if ( isEmpty(nome)&&isEmpty(endereco)&&isEmpty(cep)&&isEmpty(cidade)&&isEmpty(estado)&&isEmpty(tipo) )
        return null;

     Transacao tr = new Transacao();
     try {
	     tr.beginReadOnly();
           LaboratorioData ldata = new LaboratorioData();
           Vector v = ldata.pesquisarPorNome(nome, endereco, cep, cidade, estado, tipo, tr);
		 tr.commit();
		 return v;
     } catch(Exception e) {
         System.out.println("erro ao pesquisar " + nome);
         e.printStackTrace();
     }
     return null;
  } // pesquisar

  private boolean isEmpty(String s) {
     if (null == s)
       return true;
     if (s.length() == 0)
       return true;
     return false;
  }

  public static void main(String[] args) {
      Laboratorio l = new Laboratorio();
      LaboratorioDO laboratorio = new LaboratorioDO();
      try {
	    laboratorio = l.buscar(6);
		System.out.println(laboratorio.getNome());
      } catch(Exception e) {
          e.printStackTrace();
      }
  } // main
} // Laboratorio
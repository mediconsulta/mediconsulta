package transacoes;

import utils.*;
import data.*;
import java.util.*;

public class PlanoDeSaude {

  public boolean incluir (PlanoDeSaudeDO plano) throws Exception{

     // validacao das regras de negocio
     if ( (isEmpty(plano.getNome())) ) {
       return false;
     }

     // efetuando a transacao
     Transacao tr = new Transacao();
     try {

       tr.begin();
         PlanoDeSaudeData adata = new PlanoDeSaudeData();
         adata.incluir(plano, tr);
       tr.commit();
       return true;
       
     } catch(Exception e) {
         tr.rollback();
         System.out.println("erro ao incluir " + plano.getNome());
         e.printStackTrace();
     }
     return false;
  } // incluir

  public boolean atualizar(PlanoDeSaudeDO plano) throws Exception {
     Transacao tr = new Transacao();
	 try{
	   // inserir validacoes de regras de negocio
	   tr.begin();
  	     PlanoDeSaudeData adata = new PlanoDeSaudeData();
	     adata.atualizar(plano, tr);
	   tr.commit();
	   return true;
	 } catch (Exception e) {
	   tr.rollback();
	   System.out.println("erro ao atualizar" + plano.getNome());
	   e.printStackTrace();
	 }
	 return false;
  } // atualizar

  public PlanoDeSaudeDO buscar(int idobj) throws Exception{
     Transacao tr = new Transacao();
     if (idobj == -1){return null;}
	 try{
            tr.beginReadOnly();
            PlanoDeSaudeData adata = new PlanoDeSaudeData();
            PlanoDeSaudeDO l = adata.buscar(idobj, tr);
            tr.commit();
            return l;
	 } catch (Exception e) {
            tr.rollback();
            System.out.println("erro ao buscar" + idobj);
            e.printStackTrace();
	 }
	 return null;
  } // buscar

  public Vector pesquisarNome(String nome) {
     if ( isEmpty(nome) )
        return null;

     Transacao tr = new Transacao();
     try {
         tr.beginReadOnly();
         PlanoDeSaudeData adata = new PlanoDeSaudeData();
         Vector v = adata.pesquisarPorNome(nome, tr);
         tr.commit();
         return v;
     } catch(Exception e) {
         System.out.println("erro ao pesquisar " + nome);
         e.printStackTrace();
     }
     return null;
  } // pesquisar
  
  public Vector pesquisarPorMedico(int medicoId) {
     if ( medicoId<=0 )
        return null;

     Transacao tr = new Transacao();
     try {
         tr.beginReadOnly();
         PlanoDeSaudeData adata = new PlanoDeSaudeData();
         Vector v = adata.pesquisarPorMedico(medicoId, tr);
         tr.commit();
         return v;
     } catch(Exception e) {
         System.out.println("erro ao pesquisar " + medicoId);
         e.printStackTrace();
     }
     return null;
  } // pesquisar
  
  public Vector ListarTodos() {

     Transacao tr = new Transacao();
     try {
         tr.beginReadOnly();
         PlanoDeSaudeData adata = new PlanoDeSaudeData();
         Vector v = adata.ListarTodos(tr);
         tr.commit();
         return v;
     } catch(Exception e) {
         System.out.println("erro ao listar");
         e.printStackTrace();
     }
     return null;
  } // Listar
  
  public boolean incluirM (MedicoDO medic, PlanoDeSaudeDO plano) throws Exception{

     // efetuando a transacao
     Transacao tr = new Transacao();
     if (plano == null){return true;}
     try {

       tr.begin();
         PlanoDeSaudeData pdata = new PlanoDeSaudeData();
         pdata.incluirMedico(medic, plano, tr);
       tr.commit();
       return true;
       
     } catch(Exception e) {
         tr.rollback();
         System.out.println("erro ao incluir " + medic.getNome());
         e.printStackTrace();
     }
     return false;
  } // incluir medico

  private boolean isEmpty(String s) {
     if (null == s)
       return true;
     if (s.length() == 0)
       return true;
     return false;
  }

} // Laboratorio
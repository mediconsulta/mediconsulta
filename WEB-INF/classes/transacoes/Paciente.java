package transacoes;

import utils.*;
import data.*;
import java.util.*;

public class Paciente {
    
    public boolean incluir (PacienteDO paciente) throws Exception{

        // validacao das regras de negocio
        if ( (paciente.getUserId()<=0) || (isEmpty(paciente.getNome())) || ( isEmpty(paciente.getCpf())) || (isEmpty(paciente.getSexo())) || isEmpty(paciente.getTelefone()) || paciente.getNascimento()==null) {
          return false;
        }

        // efetuando a transacao
        Transacao tr = new Transacao();
        try {

          tr.begin();
            PacienteData pdata = new PacienteData();
            pdata.incluir(paciente, tr);
          tr.commit();
          return true;

        } catch(Exception e) {
            tr.rollback();
            System.out.println("erro ao incluir " + paciente.getNome());
            e.printStackTrace();
        }
        return false;
     } // incluir
    
    public boolean remover (PacienteDO paciente) throws Exception{

         // validacao das regras de negocio
         if ( (paciente.getId()<=0)||(paciente.getUserId()<=0) || (isEmpty(paciente.getNome())) || ( isEmpty(paciente.getCpf())) || (isEmpty(paciente.getSexo())) || isEmpty(paciente.getTelefone()) || paciente.getNascimento()==null) {
            return false;
         }

         // efetuando a transacao
         Transacao tr = new Transacao();
         try {
           tr.begin();
             PacienteData pdata = new PacienteData();
             pdata.excluir(paciente, tr);
           tr.commit();
           return true;

         } catch(Exception e) {
             tr.rollback();
             System.out.println("erro ao excluir " + paciente.getNome());
             e.printStackTrace();
         }
         return false;
      } // Remover

    public boolean atualizar(PacienteDO paciente) throws Exception {
       Transacao tr = new Transacao();
           try{
             // inserir validacoes de regras de negocio
             tr.begin();
               PacienteData pdata = new PacienteData();
               pdata.atualizar(paciente, tr);
             tr.commit();
             return true;
           } catch (Exception e) {
             tr.rollback();
             System.out.println("erro ao atualizar" + paciente.getNome());
             e.printStackTrace();
           }
           return false;
    } // atualizar

    public PacienteDO buscar(int idobj) throws Exception{
       Transacao tr = new Transacao();
           try{
             tr.beginReadOnly();
               PacienteData pdata = new PacienteData();
               PacienteDO p = pdata.buscar(idobj, tr);
             tr.commit();
             return p;
           } catch (Exception e) {
             tr.rollback();
             System.out.println("erro ao buscar" + idobj);
             e.printStackTrace();
           }
           return null;
    } // buscar
    
    public PacienteDO buscarPorUserID(int idobj) throws Exception{
       Transacao tr = new Transacao();
           try{
             tr.beginReadOnly();
               PacienteData pdata = new PacienteData();
               PacienteDO p = pdata.buscarPorUserID(idobj, tr);
             tr.commit();
             return p;
           } catch (Exception e) {
             tr.rollback();
             System.out.println("erro ao buscar" + idobj);
             e.printStackTrace();
           }
           return null;
    } // buscar
    

    public Vector pesquisar(String nome) {
       if ( isEmpty(nome) )
          return null;

       Transacao tr = new Transacao();
       try {
             tr.beginReadOnly();
             PacienteData pdata = new PacienteData();
             Vector v = pdata.pesquisarPorNome(nome, tr);
             tr.commit();
             return v;
       } catch(Exception e) {
           System.out.println("erro ao pesquisar " + nome);
           e.printStackTrace();
       }
       return null;
    } // pesquisar
    
    public Vector pesquisarCPF(String CPF) {
       if ( isEmpty(CPF) )
          return null;

       Transacao tr = new Transacao();
       try {
             tr.beginReadOnly();
             PacienteData pdata = new PacienteData();
             Vector v = pdata.pesquisarPorCPF(CPF, tr);
             tr.commit();
             return v;
       } catch(Exception e) {
           System.out.println("erro ao pesquisar " + CPF);
           e.printStackTrace();
       }
       return null;
    } // pesquisar

    private boolean isEmpty(String s) {
       if (null == s)
         return true;
       if (s.length() == 0)
         return true;
       return false;
    }

    public static void main(String[] args) {
        Paciente p = new Paciente();
        PacienteDO paciente = new PacienteDO();
        try {
              paciente = p.buscar(6);
                  System.out.println(paciente.getNome());
        } catch(Exception e) {
            e.printStackTrace();
        }
    } // main
} // Paciente


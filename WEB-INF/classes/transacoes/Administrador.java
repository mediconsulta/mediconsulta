package transacoes;

import utils.*;
import data.*;
import java.util.*;

public class Administrador {

  public boolean incluir (AdministradorDO administrador) throws Exception{

     // validacao das regras de negocio
     if ( (administrador.getUserId() <= 0) || (isEmpty(administrador.getNome())) || (isEmpty(administrador.getCpf())) || (isEmpty(administrador.getEndereco())) || (administrador.getNumero()<=0) || (isEmpty(administrador.getCidade())) || (isEmpty(administrador.getEstado())) || (administrador.getNascimento()==null) || (isEmpty(administrador.getTelefone()))) {
         return false;
     }

     // efetuando a transacao
     Transacao tr = new Transacao();
     try {

       tr.begin();
         AdministradorData adata = new AdministradorData();
         adata.incluir(administrador, tr);
       tr.commit();
       return true;
       
     } catch(Exception e) {
         tr.rollback();
         System.out.println("erro ao incluir " + administrador.getNome());
         e.printStackTrace();
     }
     return false;
  } // incluir

  public boolean atualizar(AdministradorDO administrador) throws Exception {
     Transacao tr = new Transacao();
	 try{
	   // inserir validacoes de regras de negocio
	   tr.begin();
  	     AdministradorData adata = new AdministradorData();
	     adata.atualizar(administrador, tr);
	   tr.commit();
	   return true;
	 } catch (Exception e) {
	   tr.rollback();
	   System.out.println("erro ao atualizar" + administrador.getNome());
	   e.printStackTrace();
	 }
	 return false;
  } // atualizar

  public AdministradorDO buscar(int idobj) throws Exception{
     Transacao tr = new Transacao();
	 try{
            tr.beginReadOnly();
            AdministradorData adata = new AdministradorData();
            AdministradorDO l = adata.buscar(idobj, tr);
            tr.commit();
            return l;
	 } catch (Exception e) {
            tr.rollback();
            System.out.println("erro ao buscar" + idobj);
            e.printStackTrace();
	 }
	 return null;
  } // buscar
  
    public AdministradorDO buscarPorUserID(int idobj) throws Exception{
     Transacao tr = new Transacao();
	 try{
            tr.beginReadOnly();
            AdministradorData adata = new AdministradorData();
            AdministradorDO l = adata.buscarPorUserID(idobj, tr);
            tr.commit();
            return l;
	 } catch (Exception e) {
            tr.rollback();
            System.out.println("erro ao buscar" + idobj);
            e.printStackTrace();
	 }
	 return null;
  } // buscar

  public Vector pesquisar(String nome) {
     if ( isEmpty(nome) )
        return null;

     Transacao tr = new Transacao();
     try {
	     tr.beginReadOnly();
           AdministradorData adata = new AdministradorData();
           Vector v = adata.pesquisarPorNome(nome, tr);
		 tr.commit();
		 return v;
     } catch(Exception e) {
         System.out.println("erro ao pesquisar " + nome);
         e.printStackTrace();
     }
     return null;
  } // pesquisar

  private boolean isEmpty(String s) {
     if (null == s)
       return true;
     if (s.length() == 0)
       return true;
     return false;
  }

  public static void main(String[] args) {
      Administrador l = new Administrador();
      AdministradorDO administrador = new AdministradorDO();
      try {
	    administrador = l.buscar(6);
            System.out.println(administrador.getNome());
      } catch(Exception e) {
            e.printStackTrace();
      }
  } // main
} // Laboratorio
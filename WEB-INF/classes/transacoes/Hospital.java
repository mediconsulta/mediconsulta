package transacoes;

import utils.*;
import data.*;
import java.util.*;


public class Hospital {
    
  public boolean incluir (HospitalDO hospital) throws Exception {

     // validacao das regras de negocio
      if (hospital.getNome() != null && hospital.getNome().length() == 0)
          return false;
      
      if (hospital.getEndereco() != null && hospital.getEndereco().length() == 0)
          return false;
      
      if (hospital.getCep() != null && hospital.getCep().length() == 0)
          return false;
      
      if (hospital.getCidade() != null && hospital.getCidade().length() == 0)
          return false;      
      
      if (hospital.getEstado() != null && hospital.getEstado().length() != 2)
          return false;
      
     // efetuando a transacao
     Transacao tr = new Transacao();
     try {
       tr.begin();
         HospitalData hdata = new HospitalData();         
         hdata.incluir(hospital, tr);
       tr.commit();
       return true;
       
     } catch(Exception e) {
         tr.rollback();
         System.out.println("erro ao incluir " + hospital.getNome());
         e.printStackTrace();
     }
     return false;
  } // incluir

  public boolean atualizar(HospitalDO hospital) throws Exception {
     Transacao tr = new Transacao();
	 try{
	   // inserir validacoes de regras de negocio
	   tr.begin();
  	     HospitalData hdata = new HospitalData();
	     hdata.atualizar(hospital, tr);
	   tr.commit();
	   return true;
	 } catch (Exception e) {
	   tr.rollback();
	   System.out.println("erro ao atualizar" + hospital.getNome());
	   e.printStackTrace();
	 }
	 return false;
  } // atualizar

  public HospitalDO buscar(int idobj) throws Exception{
     Transacao tr = new Transacao();
     if (idobj == -1){return null;}
	 try{
	   tr.beginReadOnly();
  	     HospitalData hdata = new HospitalData();
	     HospitalDO h = hdata.buscar(idobj, tr);
	   tr.commit();
	   return h;
	 } catch (Exception e) {
	   tr.rollback();
	   System.out.println("erro ao buscar" + idobj);
	   e.printStackTrace();
	 }
	 return null;
  } // buscar

  public Vector pesquisar(String nome) {
     if ( isEmpty(nome) )
        return null;

     Transacao tr = new Transacao();
     try {
	     tr.beginReadOnly();
           HospitalData hdata = new HospitalData();
           Vector v = hdata.pesquisarPorNome(nome, tr);
		 tr.commit();
		 return v;
     } catch(Exception e) {
         System.out.println("erro ao pesquisar " + nome);
         e.printStackTrace();
     }
     return null;
  } // pesquisar
  
  // Pesquisa por dados do hospital
  public Vector pesquisar(String nome, String endereco, String cep, String cidade, String estado, String prontoSocorro) {
      if (isEmpty(nome) && isEmpty(endereco) && isEmpty(cep) && isEmpty(cidade) && isEmpty(estado))
          return null;
      
     Transacao tr = new Transacao();
     try {
	tr.beginReadOnly();
        HospitalData hdata = new HospitalData();
        String psOption;
        if (prontoSocorro.equals("only"))
            psOption = "s";
        else
            psOption = "";
        Vector v = hdata.pesquisarPorDados(nome, endereco, cep, cidade, estado, psOption, tr);
	tr.commit();
	return v;
     } catch(Exception e) {
         System.out.println("erro ao pesquisar " + nome);
         e.printStackTrace();
     }
     return null;
  } // pesquisar

  public Vector pesquisarPorMedico(int medicoId) {
     if ( medicoId<=0 )
        return null;

     Transacao tr = new Transacao();
     try {
         tr.beginReadOnly();
         HospitalData adata = new HospitalData();
         Vector v = adata.pesquisarPorMedico(medicoId, tr);
         tr.commit();
         return v;
     } catch(Exception e) {
         System.out.println("erro ao pesquisar " + medicoId);
         e.printStackTrace();
     }
     return null;
  } // pesquisar
  
  private boolean isEmpty(String s) {
     if (null == s)
       return true;
     if (s.length() == 0)
       return true;
     return false;
  }
  
  public Vector ListarTodos() {

     Transacao tr = new Transacao();
     try {
         tr.beginReadOnly();
         HospitalData adata = new HospitalData();
         Vector v = adata.ListarTodos(tr);
         tr.commit();
         return v;
     } catch(Exception e) {
         System.out.println("erro ao listar");
         e.printStackTrace();
     }
     return null;
  } // Listar

  public static void main(String[] args) {
      Hospital h = new Hospital();
      HospitalDO hospital = new HospitalDO();
      try {
	    hospital = h.buscar(6);
		System.out.println(hospital.getNome());
      } catch(Exception e) {
          e.printStackTrace();
      }
  } // main
} // Contato
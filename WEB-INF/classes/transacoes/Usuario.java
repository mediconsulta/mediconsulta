package transacoes;

import utils.*;
import data.*;
import java.util.*;

/**
 *
 * @author aluno_t1
 */
public class Usuario {
    
    public boolean incluir (UsuarioDO user) throws Exception{

     // validacao das regras de negocio
     //if ( (isEmpty(user.getEmail())) || (isEmpty(user.getPassword()))) {
     //    return false;
     //}

     // efetuando a transacao
     Transacao tr = new Transacao();
     try {

       tr.begin();
         UsuarioData adata = new UsuarioData();
         adata.incluir(user, tr);
       tr.commit();
       return true;
       
     } catch(Exception e) {
         tr.rollback();
         System.out.println("erro ao incluir " + user.getEmail());
         e.printStackTrace();
     }
     return false;
  } // incluir
    
    public boolean remover (UsuarioDO usuario) throws Exception{

         // validacao das regras de negocio
         if (usuario.getEmail()==null || usuario.getIduser()<=0) {
            return false;
         }

         // efetuando a transacao
         Transacao tr = new Transacao();
         try {
           tr.begin();
             UsuarioData udata = new UsuarioData();
             udata.excluir(usuario, tr);
           tr.commit();
           return true;

         } catch(Exception e) {
             tr.rollback();
             System.out.println("erro ao excluir " + usuario.getEmail());
             e.printStackTrace();
         }
         return false;
      } // Remover
    
    public boolean checkLogin (String username, String pass) throws Exception{
     // efetuando a transacao
     Transacao tr = new Transacao();
     try {
       boolean v;
       tr.begin();
         UsuarioData adata = new UsuarioData();
       v= adata.checkLogin(username,pass,tr);
       tr.commit();
       return v;
       
     } catch(Exception e) {
         tr.rollback();
         System.out.println("erro ao incluir " + "teste");
         e.printStackTrace();
     }
     return false;
  } // checkLogin
    
    public UsuarioDO pesquisar(String username, String pass) {
        //if ( isEmpty(username) || isEmpty(pass) )
           //return null;

        Transacao tr = new Transacao();
        try {
                tr.beginReadOnly();
              UsuarioData udata = new UsuarioData();
              
              UsuarioDO user = udata.buscarUser(username, pass, tr);
              tr.commit();
              return user;
        } catch(Exception e) {
            System.out.println("erro ao pesquisar " + " ");
            e.printStackTrace();
        }
        return null;
     } // pesquisar
    
    public UsuarioDO buscarEmail(String email) {
        //if ( isEmpty(username) || isEmpty(pass) )
           //return null;

        Transacao tr = new Transacao();
        try {
              tr.beginReadOnly();
              UsuarioData udata = new UsuarioData();
              
              UsuarioDO user = udata.buscarEmail(email, tr);
              tr.commit();
              return user;
        } catch(Exception e) {
            System.out.println("erro ao pesquisar " + email);
            e.printStackTrace();
        }
        return null;
     } // buscar Email
    
    public UsuarioDO buscarID(int id) {
        //if ( isEmpty(username) || isEmpty(pass) )
           //return null;

        Transacao tr = new Transacao();
        try {
              tr.beginReadOnly();
              UsuarioData udata = new UsuarioData();
              
              UsuarioDO user = udata.buscarID(id, tr);
              tr.commit();
              return user;
        } catch(Exception e) {
            System.out.println("erro ao pesquisar " + id);
            e.printStackTrace();
        }
        return null;
     } // pesquisar
    
    public Vector pesquisarEmail(String email) {
       if ( email==null || email.equals("") )
          return null;

       Transacao tr = new Transacao();
       try {
             tr.beginReadOnly();
             UsuarioData udata = new UsuarioData();
             Vector v = udata.pesquisarPorEmail(email, tr);
             tr.commit();
             return v;
       } catch(Exception e) {
           System.out.println("erro ao pesquisar " + email);
           e.printStackTrace();
       }
       return null;
    } // pesquisarEmail
    
    public boolean atualizar(UsuarioDO usuario) throws Exception {
     Transacao tr = new Transacao();
	 try{
	   // inserir validacoes de regras de negocio
	   tr.begin();
  	     UsuarioData adata = new UsuarioData();
	     adata.atualizar(usuario, tr);
	   tr.commit();
	   return true;
	 } catch (Exception e) {
	   tr.rollback();
	   System.out.println("erro ao atualizar" + usuario.getEmail());
	   e.printStackTrace();
	 }
	 return false;
  } // atualizar
    
    public Vector getEmails (int tipo){
       Transacao tr = new Transacao();
       try {
             tr.beginReadOnly();
             UsuarioData udata = new UsuarioData();
             Vector v = udata.getEmails(tr, tipo);
             tr.commit();
             return v;
       } catch(Exception e) {
           System.out.println("erro ao pesquisar emails medicos");
           e.printStackTrace();
       }
       return null;
    }
}
